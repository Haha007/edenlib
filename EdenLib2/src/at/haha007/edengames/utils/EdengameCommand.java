package at.haha007.edengames.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Messages;

public class EdengameCommand implements CommandExecutor, TabCompleter {
	private EdengameManager manager = new EdengameManager();
	private String gameReplacement = "\\{GAME\\}";

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission("eden.edengames.manager.command")) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			return true;
		}
		if (args.length == 0) {
			sender.sendMessage(ChatColor.GOLD + "/edengame reload");
			sender.sendMessage(ChatColor.GOLD + "/edengame reload <game>");
			return true;
		}

		switch (args[0].toLowerCase()) {

		case "reload":
			Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, () -> {
				if (args.length == 1) {
					manager.stopAll();
					manager = new EdengameManager();
					sender.sendMessage(Messages.getMessages().getMessage("games.manager.reload.all"));
				} else {
					boolean found = manager.reload(args[1]);
					if (found) {
						sender.sendMessage(Messages.getMessages().getMessage("games.manager.reload.one")
								.replaceAll(gameReplacement, args[1]));
					} else {
						sender.sendMessage(Messages.getMessages().getMessage("games.manager.reload.gamenotfound")
								.replaceAll(gameReplacement, args[1]));
					}
				}
			});
			break;

		default:
			sender.sendMessage(ChatColor.GOLD + "/edengame reload");
			sender.sendMessage(ChatColor.GOLD + "/edengame reload <game>");
			break;

		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		if (args.length == 1) {
			list.add("reload");
		} else if (args.length == 2) {
			for (String key : manager.getKeys()) {
				if (key.startsWith(args[1].toLowerCase()))
					list.add(key);
			}
		}
		return list;
	}

	public EdengameManager getManager() {
		return manager;
	}

}
