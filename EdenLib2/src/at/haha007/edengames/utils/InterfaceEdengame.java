package at.haha007.edengames.utils;

import java.util.HashMap;
import java.util.List;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public interface InterfaceEdengame {

	public boolean containsPlayer(Player player);

	public boolean isMultiplayer();

	public void unregister();

	public void removePlayer(Player player);

	public HashMap<String, CommandExecutor> getCommandExecutors();

	public HashMap<String, TabCompleter> getCommandCompleters();

	public List<Listener> getListeners();
}
