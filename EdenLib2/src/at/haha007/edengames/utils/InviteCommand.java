package at.haha007.edengames.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Messages;
import net.minecraft.server.v1_13_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_13_R2.PacketPlayOutChat;

public class InviteCommand implements CommandExecutor, TabCompleter {

	private final File file = new File(EdenLib.instance.getDataFolder(), "invite.yml");
	private YamlConfiguration cfg;
	private HashMap<String, Location> invites = new HashMap<>();
	private final String inviteReplacement = "\\{INVITE\\}";
	private final String playerReplacement = "\\{PLAYER\\}";

	public InviteCommand() {
		try {
			if (!file.exists())
				file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		cfg = YamlConfiguration.loadConfiguration(file);

		for (String key : cfg.getKeys(false)) {
			Location loc = getLocation(cfg.getConfigurationSection(key));
			if (loc != null)
				invites.put(key, loc);
		}
	}

	private Location getLocation(ConfigurationSection cfg) {
		World world = Bukkit.getWorld(cfg.getString("world"));
		if (world == null)
			return null;
		return new Location(world, cfg.getDouble("x"), cfg.getDouble("y"), cfg.getDouble("z"),
				(float) cfg.getDouble("yaw"), (float) cfg.getDouble("pitch"));
	}

	private void setLocation(ConfigurationSection cfg, Location loc) {
		cfg.set("x", loc.getX());
		cfg.set("y", loc.getY());
		cfg.set("z", loc.getZ());
		cfg.set("yaw", loc.getYaw());
		cfg.set("pitch", loc.getPitch());
		cfg.set("world", loc.getWorld().getName());
	}

	private void save() {
		for (Entry<String, Location> entry : invites.entrySet()) {
			setLocation(cfg.createSection(entry.getKey()), entry.getValue());
		}
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String delInvite(String key) {
		cfg.set("key", null);
		Bukkit.getScheduler().runTaskLaterAsynchronously(EdenLib.instance, () -> {
			save();
		}, 1);
		return invites.remove(key) == null
				? Messages.getMessages().getMessage("commands.invite.notexists").replaceAll(inviteReplacement, key)
				: Messages.getMessages().getMessage("commands.invite.delinvite").replaceAll(inviteReplacement, key);
	}

	private void acceptInvite(String key, Player player) {
		Location loc = invites.get(key);
		if (loc == null) {
			player.sendMessage(
					Messages.getMessages().getMessage("commands.invite.notexists").replaceAll(inviteReplacement, key));
			return;
		}
		player.teleport(loc);
	}

	private void broadcastInvite(String key, Player sender) {
		if (!invites.containsKey(key)) {
			sender.sendMessage(
					Messages.getMessages().getMessage("commands.invite.notexists").replaceAll(inviteReplacement, key));
			return;
		}

		broadcastClickMessage(Messages.getMessages().getMessage("commands.invite.broadcast")
				.replaceAll(inviteReplacement, key).replaceAll(playerReplacement, sender.getDisplayName()),
				"/acceptinvite " + key);

	}

	private void broadcastClickMessage(String message, String command) {
		PacketPlayOutChat packet = new PacketPlayOutChat(
				ChatSerializer.a("{\"text\":\"" + "" + "\",\"extra\":[{\"text\":\"" + message
						+ "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"" + command + "\"}}]}"));

		for (Player player : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
	}

	private String setInvite(String key, Location loc) {
		if (invites.containsKey(key)) {
			invites.remove(key);
		}
		invites.put(key, loc);
		Bukkit.getScheduler().runTaskLaterAsynchronously(EdenLib.instance, () -> {
			save();
		}, 1);
		return Messages.getMessages().getMessage("commands.invite.setinvite");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length != 1)
			return false;

		if (!(sender instanceof Player)) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.notplayer"));
			return true;
		}
		args[0] = args[0].toLowerCase();

		switch (label.toLowerCase()) {
		case "edeninvite":
		case "invite":
			if (sender.hasPermission("eden.edenutils.invite.broadcast")) {
				broadcastInvite(args[0], (Player) sender);
			} else {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			}
			break;
		case "delinvite":
			if (sender.hasPermission("eden.edenutils.invite.delete")) {
				sender.sendMessage(delInvite(args[0]));
			} else {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			}
			break;
		case "setinvite":
			if (sender.hasPermission("eden.edenutils.invite.set")) {
				sender.sendMessage(setInvite(args[0], ((Player) sender).getLocation()));
			} else {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			}
			break;
		case "acceptinvite":
			if (sender.hasPermission("eden.edenutils.invite.accept")) {
				acceptInvite(args[0], (Player) sender);
			} else {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			}
			break;

		default:
			break;
		}

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		for (String key : invites.keySet()) {
			if (key.startsWith(args[args.length - 1]))
				list.add(key);
		}
		return list;
	}

}
