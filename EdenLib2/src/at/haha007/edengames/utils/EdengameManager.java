package at.haha007.edengames.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class EdengameManager {

	private HashMap<String, Class<? extends InterfaceEdengame>> classes;

	// edengames
	private List<InterfaceEdengame> games;

	public EdengameManager() {
		classes = new HashMap<>();
		games = new ArrayList<InterfaceEdengame>();
	}

	public void registerGame(String key, Class<? extends InterfaceEdengame> clazz) {
		key = key.toLowerCase();
		if (classes.containsValue(clazz) || classes.containsKey(key)) {
			System.err.println("[EdenLib] Trying to register a game twice?");
			System.err.println("[EdenLib] Key: " + key);
			System.err.println("[EdenLib] Class: " + clazz.getName());
		}
		classes.put(key, clazz);
		try {
			games.add(clazz.getConstructor().newInstance());
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}

	public void stopAll() {
		for (InterfaceEdengame game : games) {
			game.unregister();
		}
		games.clear();
	}

	public boolean reload(String key) {
		key = key.toLowerCase();
		Class<? extends InterfaceEdengame> clazz = classes.get(key);
		for (int i = 0; i < games.size(); i++) {
			InterfaceEdengame game = games.get(i);
			if (game.getClass() == clazz) {
				game.unregister();
				games.remove(i);

				try {
					games.add(i, clazz.getConstructor().newInstance());
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
				return true;
			}
		}
		return false;
	}

	public Set<String> getKeys() {
		return classes.keySet();
	}
}
