package at.haha007.edengames.jnr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import at.haha007.edenlib.utils.Utils;

public class JumpAndRun {
	private ArrayList<JumpAndRunCheckpoint> checkpoints = new ArrayList<>();
	boolean showNextCheckpoint;
	private String name;
	private World world;
	private final String toolName = ChatColor.GREEN + "Jump and Run TOOL ";

	public JumpAndRun(String name, Location firstCheckpoint) {
		world = firstCheckpoint.getWorld();
		this.name = name;
		checkpoints.add(new JumpAndRunCheckpoint(
				new Vector(firstCheckpoint.getBlockX(), firstCheckpoint.getBlockY(), firstCheckpoint.getBlockZ()),
				firstCheckpoint.getYaw(), firstCheckpoint.getPitch()));
		showNextCheckpoint = true;
		if (this.checkpoints == null)
			this.checkpoints = new ArrayList<>();
	}

	public JumpAndRun(File file) {
		if (!file.exists()) {
			this.name = file.getName();
			checkpoints = new ArrayList<>();
			save(file);
			return;
		}

		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		String fileName = file.getName();
		name = fileName.substring(0, fileName.length() - 4);
		int index = 0;
		while (true) {
			ConfigurationSection section = cfg.getConfigurationSection("checkpoints." + index++);
			if (section == null)
				break;
			checkpoints.add(new JumpAndRunCheckpoint(section));
		}
		showNextCheckpoint = cfg.getBoolean("shownext", true);
		world = Bukkit.getWorld(cfg.getString("world"));

	}

	public void save(File file) {
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		YamlConfiguration cfg = new YamlConfiguration();
		for (int i = 0; i < checkpoints.size(); i++) {
			ConfigurationSection section = cfg.createSection("checkpoints." + i);
			checkpoints.get(i).save(section);
		}
		cfg.set("shownext", showNextCheckpoint);
		if (world != null)
			cfg.set("world", world.getName());
		else
			cfg.set("world", Bukkit.getWorlds().get(0).getName());
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<JumpAndRunCheckpoint> getCheckpoints() {
		return checkpoints;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public World getWorld() {
		return world;
	}

	public Inventory openInfoInventory(Player player) {

		Inventory inv = Bukkit.createInventory(null, (checkpoints.size() / 9 + 1) * 9,
				ChatColor.GREEN + "Jump and Run INFO " + getName());
		if (checkpoints.isEmpty())
			return inv;

		Vector playerPosition = player.getLocation().toVector();
		int closestIndex = 0;
		double closestDistance = Double.MAX_VALUE;

		for (int i = 0; i < checkpoints.size(); i++) {
			JumpAndRunCheckpoint checkpoint = checkpoints.get(i);
			Vector loc = checkpoint.getPosition();
			ItemStack item = new ItemStack(Material.GRASS_BLOCK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD.toString() + i);
			ArrayList<String> lore = new ArrayList<>();
			List<String> commands = checkpoint.getCommands();
			lore.add(ChatColor.GOLD.toString() + loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ());
			for (int j = 0; j < commands.size(); j++) {
				lore.add(ChatColor.GREEN.toString() + j + ": " + ChatColor.GOLD + commands.get(j));
			}
			meta.setLore(lore);
			item.setItemMeta(meta);
			inv.addItem(item);

			double distance = loc.distanceSquared(playerPosition);
			if (distance < closestDistance) {
				closestDistance = distance;
				closestIndex = i;
			}
		}

		inv.setItem(closestIndex, Utils.addGlow(inv.getItem(closestIndex)));

		player.openInventory(inv);
		return inv;
	}

	void clickInfoInventory(Player player, int index, boolean right) {
		if (right) {
			// dump info into chat
			try {
				JumpAndRunCheckpoint cp = checkpoints.get(index);
				Vector pos = cp.getPosition();

				player.sendMessage(ChatColor.GREEN + "========================");
				player.sendMessage(ChatColor.GREEN + "JnR name: " + ChatColor.GOLD + getName());
				player.sendMessage(ChatColor.GREEN + "Show next Checkpoint: " + ChatColor.GOLD + showNextCheckpoint);
				player.sendMessage(ChatColor.GREEN + "World: " + ChatColor.GOLD + world.getName());
				player.sendMessage(ChatColor.GREEN + "Position: " + ChatColor.GOLD + pos.getBlockX() + ", "
						+ pos.getBlockY() + ", " + pos.getBlockZ());
				player.sendMessage(ChatColor.GREEN + "Commands: ");
				for (String string : cp.getCommands()) {
					player.sendMessage(ChatColor.GOLD + string);
				}
				player.sendMessage(ChatColor.GREEN + "========================");
			} catch (IndexOutOfBoundsException e) {
				return;
			}
		} else {
			// teleport player
			Location loc = getCheckpointLocation(index);
			if (loc == null)
				return;
			player.teleport(loc);
		}
	}

	Location getCheckpointLocation(int index) {
		try {
			return getCheckpointLocation(checkpoints.get(index));
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	Location getCheckpointLocation(JumpAndRunCheckpoint checkpoint) {
		return checkpoint.toLocation(world);
	}

	public String getToolName() {
		return toolName + getName();
	}

	public ItemStack getTool() {
		ItemStack item = new ItemStack(Material.STICK);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(getToolName());
		item.setItemMeta(meta);
		return item;
	}
}
