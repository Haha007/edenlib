package at.haha007.edengames.jnr;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.util.Vector;

public class JumpAndRunCheckpoint {

	private Vector position;
	private float pitch;
	private float yaw;
	private List<String> commands;

	JumpAndRunCheckpoint(Vector position, float yaw, float pitch) {
		this.position = new Vector(position.getBlockX(), position.getBlockY(), position.getBlockZ());
		this.yaw = yaw;
		this.pitch = pitch;
		commands = new ArrayList<>();
	}

	JumpAndRunCheckpoint(ConfigurationSection cfg) {
		commands = cfg.getStringList("commands");

		position = new Vector(cfg.getDouble("position.x"), cfg.getDouble("position.y"), cfg.getDouble("position.z"));
		yaw = (float) cfg.getDouble("position.yaw");
		pitch = (float) cfg.getDouble("position.pitch");
	}

	public void save(ConfigurationSection cfg) {
		cfg.set("position.x", position.getX());
		cfg.set("position.y", position.getY());
		cfg.set("position.z", position.getZ());
		cfg.set("position.yaw", yaw);
		cfg.set("position.pitch", pitch);
		cfg.set("commands", commands);
	}

	public Location toLocation(World world) {
		return new Location(world, position.getX() + 0.5, position.getY(), position.getZ() + 0.5, yaw, pitch);
	}

	public List<String> getCommands() {
		return commands;
	}

	public void executeCommands(String player) {
		for (String command : commands) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replaceAll("%player%", player));
		}
	}

	public Vector getPosition() {
		return position;
	}

	public void setPosition(Vector position, float yaw, float pitch) {
		this.position = new Vector(position.getBlockX(), position.getBlockY(), position.getBlockZ());
		this.yaw = yaw;
		this.pitch = pitch;
	}
}
