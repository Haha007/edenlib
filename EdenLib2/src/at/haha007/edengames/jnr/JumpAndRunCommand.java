package at.haha007.edengames.jnr;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.EdenArea;
import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.Utils;

public class JumpAndRunCommand implements CommandExecutor, TabCompleter {

	JumpAndRunGame game;
	private final String jnrReplacement = "\\{JNR\\}";
	private final String numberReplacement = "\\{NUMBER\\}";
	private final String commandReplacement = "\\{COMMAND\\}";

	public JumpAndRunCommand(JumpAndRunGame game) {
		this.game = game;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("eden.edengames.jnr.command"))
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
		if (args.length == 0) {
			sendCommandInfo(sender);
			return true;
		}

		for (int i = 0; i < args.length; i++) {
			args[i] = args[i].toLowerCase();
		}

		if (sender instanceof ConsoleCommandSender) {
			switch (args[0]) {
			case "reload":
				reload();
				break;
			case "list":
				listJumpAndRuns(sender);
				break;
			default:
				break;
			}
		} else if (sender instanceof BlockCommandSender) {
			if (args[0].equals("start")) {
				try {
					Block block = ((BlockCommandSender) sender).getBlock();

					JumpAndRun jnr = game.getJumpAndRun(args[1]);

					double dx = Double.parseDouble(args[2]);
					double dy = Double.parseDouble(args[3]);
					double dz = Double.parseDouble(args[4]);

					double x = Double.parseDouble(args[5]);
					double y = Double.parseDouble(args[6]);
					double z = Double.parseDouble(args[7]);

					Vector pos1 = new Vector(block.getX() + dx, block.getY() + dy, block.getZ() + dz);
					Vector pos2 = new Vector(x, y, z).add(pos1);
					World world = block.getWorld();
					EdenArea area = new EdenArea(pos1, pos2, world);
					start(area, jnr);
				} catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
					return false;
				}
			} else {
				return false;
			}
		} else if (sender instanceof Player) {

			Player player = (Player) sender;
			switch (args[0]) {

			case "create":
				createJnR(player, args);
				break;

			case "delete":
				deleteJnR(player, args);
				break;

			case "reload":
				reload();
				break;

			case "list":
				listJumpAndRuns(sender);
				break;

			case "setlobby":
				game.setLobby(player.getLocation());
				player.sendMessage(Messages.getMessages().getMessage("games.jnr.setlobby"));
				break;

			case "info":
				showJnrInfo(player, args);
				break;

			case "addcheckpoint":
				addCheckpoint(player, args);
				break;

			case "movecheckpoint":
				moveCheckpoint(player, args);
				break;

			case "removecheckpoint":
				removeCheckpoint(player, args);
				break;

			case "addcommand":
				addCommand(player, args);
				break;

			case "removecommand":
				removeCommand(player, args);
				break;

			case "reset":
				reset(player, args);
				break;

			case "shownextcheckpoint":
				showNext(player, args);
				break;

			case "tool":
				getTool(player, args);
				break;
			default:
				sendCommandInfo(sender);
				break;
			}
		}

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		if (!(sender instanceof Player))
			return list;
		if (!sender.hasPermission("eden.edengames.jnr.command"))
			return list;

		// für leichteres handling tolowercase
		for (int i = 0; i < args.length; i++) {
			args[i] = args[i].toLowerCase();
		}

		if (args.length == 1) {
			String[] strings = new String[] { "create", "delete", "addcheckpoint", "removecheckpoint", "addcommand",
					"removecommand", "setlobby", "list", "info", "reload", "reset", "movecheckpoint",
					"shownextcheckpoint", "tool" };
			for (String string : strings) {
				if (string.startsWith(args[0])) {
					list.add(string);
				}
			}
			return list;
		}

		if (args.length == 2) {
			switch (args[0]) {
			case "delete":
			case "addcheckpoint":
			case "removecheckpoint":
			case "addcommand":
			case "removecommand":
			case "info":
			case "movecheckpoint":
			case "shownextcheckpoint":
			case "tool":
				for (JumpAndRun jnr : game.getJnrList()) {
					if (jnr.getName().startsWith(args[1]))
						list.add(jnr.getName());
				}
				break;
			case "reset":
				for (Player player : Bukkit.getOnlinePlayers()) {
					if (player.getName().startsWith(args[1]))
						list.add(player.getName());
				}
				break;
			default:
				break;
			}
			return list;
		}

		return list;
	}

	private void start(EdenArea area, JumpAndRun jnr) {
		for (Player player : area.getWorld().getPlayers()) {
			if (area.isPointInArea(player.getLocation())) {
				try {
					game.start(player, jnr);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void reload() {
		Bukkit.getScheduler().runTask(EdenLib.instance, () -> {
			EdenLib.instance.getEdengameCommand().getManager().reload("jnr");
		});
	}

	private void sendCommandInfo(CommandSender sender) {
		sender.sendMessage(ChatColor.GOLD + "/jnr create <jnr>");
		sender.sendMessage(ChatColor.GOLD + "/jnr delete <jnr>");
		sender.sendMessage(ChatColor.GOLD + "/jnr shownextcheckpoint <jnr> [true/false]");
		sender.sendMessage(ChatColor.GOLD + "/jnr addcheckpoint <jnr> <index>");
		sender.sendMessage(ChatColor.GOLD + "/jnr movecheckpoint <jnr> <index>");
		sender.sendMessage(ChatColor.GOLD + "/jnr removecheckpoint <jnr> <index>");
		sender.sendMessage(ChatColor.GOLD + "/jnr addcommand <jnr> <checkpoint> <index> <command>");
		sender.sendMessage(ChatColor.GOLD + "/jnr removecommand <jnr> <checkpoint> <index>");
		sender.sendMessage(ChatColor.GOLD + "/jnr setlobby");
		sender.sendMessage(ChatColor.GOLD + "/jnr list");
		sender.sendMessage(ChatColor.GOLD + "/jnr info <jnr>");
		sender.sendMessage(ChatColor.GOLD + "/jnr reload");
		sender.sendMessage(ChatColor.GOLD + "/jnr reset <player> <jnr>");
		sender.sendMessage(ChatColor.GOLD + "/jnr tool <jnr>");
		sender.sendMessage(ChatColor.GOLD + "You can only start Jump and Runs with commandblocks.");
		sender.sendMessage(ChatColor.GOLD + "/jnr start <jnr> <dx> <dy> <dz> <x> <y> <z>");
	}

	private void listJumpAndRuns(CommandSender sender) {
		sender.sendMessage(ChatColor.DARK_GREEN + "Jump and Runs:");
		sender.sendMessage(ChatColor.DARK_GREEN + "====================");
		for (JumpAndRun jnr : game.getJnrList()) {
			sender.sendMessage(ChatColor.GREEN + jnr.getName());
		}
		sender.sendMessage(ChatColor.DARK_GREEN + "====================");
	}

	private void createJnR(Player player, String[] args) {
		if (args.length != 2) {
			player.sendMessage(ChatColor.GOLD + "/jnr create <jnr>");
			return;
		}
		if (game.getJumpAndRun(args[1]) != null) {
			player.sendMessage(
					Messages.getMessages().getMessage("games.jnr.notexists").replaceAll(jnrReplacement, args[1]));
			return;
		}
		JumpAndRun jnr = new JumpAndRun(args[1], player.getLocation());
		game.addJumpAndRun(jnr);
		game.getManager().saveJumpAndRun(jnr);
		player.sendMessage(
				Messages.getMessages().getMessage("games.jnr.create").replaceAll(jnrReplacement, jnr.getName()));
	}

	private void deleteJnR(Player player, String[] args) {
		if (args.length != 2) {
			player.sendMessage(ChatColor.GOLD + "/jnr delete <jnr>");
			return;
		}
		JumpAndRun jnr = game.getJumpAndRun(args[1]);
		if (jnr == null) {
			String message = Messages.getMessages().getMessage("games.jnr.notexists").replaceAll(jnrReplacement,
					args[1]);
			player.sendMessage(message);
			return;
		}
		game.getManager().deleteJnr(jnr);

		String message = Messages.getMessages().getMessage("games.jnr.delete").replaceAll(jnrReplacement, args[1]);
		player.sendMessage(message);
	}

	private void getTool(Player player, String[] args) {
		if (args.length != 2) {
			player.sendMessage(ChatColor.GOLD + "/jnr tool <jnr>");
			return;
		}
		JumpAndRun jnr = game.getJumpAndRun(args[1]);
		if (jnr == null) {
			player.sendMessage(
					Messages.getMessages().getMessage("games.jnr.notexists").replaceAll(jnrReplacement, args[1]));
			return;
		}
		player.getInventory().setItemInMainHand(jnr.getTool());
	}

	private void showNext(Player player, String[] args) {
		if (args.length != 3) {
			player.sendMessage(ChatColor.GOLD + "/jnr shownextcheckpoint <jnr> [true/false]");
			return;
		}
		JumpAndRun jnr = game.getJumpAndRun(args[1]);
		if (jnr == null) {
			player.sendMessage(ChatColor.RED + "Dieses Jump and Run existiert nicht.");
			return;
		}
		boolean showNext = Boolean.parseBoolean(args[2]);
		jnr.showNextCheckpoint = showNext;
		game.getManager().saveJumpAndRun(jnr);

		if (showNext) {
			String message = Messages.getMessages().getMessage("games.jnr.shownextcheckpoint.enable")
					.replaceAll(jnrReplacement, jnr.getName());
			player.sendMessage(message);
		} else {
			String message = Messages.getMessages().getMessage("games.jnr.shownextcheckpoint.disable")
					.replaceAll(jnrReplacement, jnr.getName());
			player.sendMessage(message);
		}
	}

	private void showJnrInfo(Player player, String[] args) {
		if (args.length != 2) {
			sendCommandInfo(player);
			return;
		}
		JumpAndRun jnr = game.getJumpAndRun(args[1]);
		if (jnr == null) {
			String message = Messages.getMessages().getMessage("games.jnr.jnrnotexists").replaceAll(jnrReplacement,
					args[1]);
			player.sendMessage(message);
			return;
		}
		jnr.openInfoInventory(player);
	}

	private void removeCheckpoint(Player player, String[] args) {
		if (args.length < 2 || args.length > 3) {
			player.sendMessage(ChatColor.GOLD + "/jnr removecheckpoint <jnr> <index>");
			return;
		}
		JumpAndRun jnr = game.getJumpAndRun(args[1]);

		if (jnr == null) {
			String message = Messages.getMessages().getMessage("games.jnr.jnrnotexists").replaceAll(jnrReplacement,
					args[1]);
			player.sendMessage(message);
			return;
		}

		List<JumpAndRunCheckpoint> checkpoints = jnr.getCheckpoints();

		if (checkpoints.isEmpty()) {
			String message = Messages.getMessages().getMessage("games.jnr.hasnocheckpoints").replaceAll(jnrReplacement,
					jnr.getName());
			player.sendMessage(message);
			return;
		}

		int checkpointIndex = checkpoints.size() - 1;
		if (args.length == 3) {
			try {
				int i = Integer.parseInt(args[2]);
				if (i < 0 || i > checkpointIndex) {
					String message = Messages.getMessages().getMessage("games.jnr.checkpoint.notexists")
							.replaceAll(jnrReplacement, args[1]).replaceAll(numberReplacement, "" + i);
					player.sendMessage(message);
					return;
				}
				checkpointIndex = i;
			} catch (NumberFormatException e) {
				String message = Messages.getMessages().getMessage("commands.general.notinteger")
						.replaceAll(numberReplacement, args[2]).replaceAll(jnrReplacement, jnr.getName());
				player.sendMessage(message);
				return;
			}
		}

		checkpoints.remove(checkpointIndex);
		game.getManager().saveJumpAndRun(jnr);
		String message = Messages.getMessages().getMessage("games.jnr.checkpoint.delete")
				.replaceAll(numberReplacement, "" + checkpointIndex).replaceAll(jnrReplacement, jnr.getName());
		player.sendMessage(message);
	}

	private void addCheckpoint(Player player, String[] args) {
		if (args.length < 2 || args.length > 3) {
			player.sendMessage(ChatColor.GOLD + "/jnr addcheckpoint <jnr> <index>");
			return;
		}
		JumpAndRun jnr = game.getJumpAndRun(args[1]);
		if (jnr == null) {
			String message = Messages.getMessages().getMessage("games.jnr.jnrnotexists").replaceAll(jnrReplacement,
					args[1]);
			player.sendMessage(message);
			return;
		}

		List<JumpAndRunCheckpoint> checkpoints = jnr.getCheckpoints();

		int checkpointIndex = checkpoints.size();
		if (args.length == 3) {
			try {
				int i = Integer.parseInt(args[2]);
				if (i < 0 || i > checkpointIndex) {
					String message = Messages.getMessages().getMessage("commands.general.outofbounds")
							.replaceAll(numberReplacement, "" + i).replaceAll(jnrReplacement, jnr.getName());
					player.sendMessage(message);
					return;
				}
				checkpointIndex = i;
			} catch (NumberFormatException e) {
				String message = Messages.getMessages().getMessage("commands.general.notinteger")
						.replaceAll(numberReplacement, args[2]).replaceAll(jnrReplacement, jnr.getName());
				player.sendMessage(message);
				return;
			}
		}
		Location loc = player.getLocation();

		checkpoints.add(checkpointIndex, new JumpAndRunCheckpoint(
				new Vector(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()), loc.getYaw(), loc.getPitch()));
		game.getManager().saveJumpAndRun(jnr);
		String message = Messages.getMessages().getMessage("games.jnr.checkpoint.create")
				.replaceAll(numberReplacement, "" + checkpointIndex).replaceAll(jnrReplacement, jnr.getName());
		player.sendMessage(message);

	}

	private void moveCheckpoint(Player player, String[] args) {
		if (args.length < 2 || args.length > 3) {
			player.sendMessage(ChatColor.GOLD + "/jnr movecheckpoint <jnr> <index>");
			return;
		}
		JumpAndRun jnr = game.getJumpAndRun(args[1]);
		if (jnr == null) {
			player.sendMessage(ChatColor.RED + "Dieses Jump and Run existiert nicht.");
			return;
		}

		List<JumpAndRunCheckpoint> checkpoints = jnr.getCheckpoints();

		int checkpointIndex = checkpoints.size() - 1;
		if (args.length == 3) {
			try {
				int i = Integer.parseInt(args[2]);
				if (i < 0 || i > checkpointIndex) {
					String message = Messages.getMessages().getMessage("games.jnr.checkpoint.notexists")
							.replaceAll(numberReplacement, "" + i).replaceAll(jnrReplacement, jnr.getName());
					player.sendMessage(message);
					return;
				}
				checkpointIndex = i;
			} catch (NumberFormatException e) {
				String message = Messages.getMessages().getMessage("commands.general.notinteger")
						.replaceAll(numberReplacement, args[2]).replaceAll(jnrReplacement, jnr.getName());
				player.sendMessage(message);
				return;
			}
		}

		Location loc = player.getLocation();

		JumpAndRunCheckpoint cp = checkpoints.get(checkpointIndex);
		cp.setPosition(new Vector(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()), loc.getYaw(), loc.getPitch());
		game.getManager().saveJumpAndRun(jnr);
		String message = Messages.getMessages().getMessage("games.jnr.checkpoint.move")
				.replaceAll(numberReplacement, "" + checkpointIndex).replaceAll(jnrReplacement, jnr.getName());
		player.sendMessage(message);

	}

	private void removeCommand(Player player, String[] args) {
		if (args.length < 3 || args.length > 4) {
			player.sendMessage(ChatColor.GOLD + "/jnr removecommand <jnr> <checkpoint> <index>");
			return;
		}
		JumpAndRun jnr = game.getJumpAndRun(args[1]);
		if (jnr == null) {
			String message = Messages.getMessages().getMessage("games.jnr.notexists").replaceAll(jnrReplacement,
					args[1]);
			player.sendMessage(message);
			return;
		}

		List<JumpAndRunCheckpoint> checkpoints = jnr.getCheckpoints();

		int checkpointIndex;
		try {
			int i = Integer.parseInt(args[2]);
			if (i < 0 || i > checkpoints.size() - 1) {
				String message = Messages.getMessages().getMessage("games.jnr.checkpoint.notexists")
						.replaceAll(numberReplacement, "" + i).replaceAll(jnrReplacement, jnr.getName());
				player.sendMessage(message);
				return;
			}
			checkpointIndex = i;
		} catch (NumberFormatException e) {
			String message = Messages.getMessages().getMessage("commands.general.notinteger")
					.replaceAll(numberReplacement, "" + args[2]);
			player.sendMessage(message);
			return;
		}
		JumpAndRunCheckpoint checkpoint = checkpoints.get(checkpointIndex);

		List<String> commands = checkpoint.getCommands();

		int commandIndex = commands.size() - 1;
		if (args.length == 4) {
			try {
				int i = Integer.parseInt(args[3]);
				if (i < 0 || i > commandIndex) {
					String message = Messages.getMessages().getMessage("games.jnr.checkpoint.command.outofbounds")
							.replaceAll(numberReplacement, "" + checkpointIndex)
							.replaceAll(jnrReplacement, jnr.getName()).replaceAll("\\{INDEX\\}", "" + i);
					player.sendMessage(message);
					return;
				}
				commandIndex = i;
			} catch (NumberFormatException e) {
				String message = Messages.getMessages().getMessage("commands.general.notinteger")
						.replaceAll(numberReplacement, args[3]);
				player.sendMessage(message);
				return;
			}
		}

		String command = commands.get(commandIndex);
		commands.remove(commandIndex);
		game.getManager().saveJumpAndRun(jnr);

		String message = Messages.getMessages().getMessage("games.jnr.checkpoint.command.remove")
				.replaceAll(numberReplacement, "" + checkpointIndex).replaceAll(jnrReplacement, jnr.getName())
				.replaceAll(commandReplacement, command);
		player.sendMessage(message);

	}

	private void addCommand(Player player, String[] args) {
		if (args.length < 5) {
			player.sendMessage(ChatColor.GOLD + "/jnr addcommand <jnr> <checkpoint> <index> <command>");
			return;
		}
		JumpAndRun jnr = game.getJumpAndRun(args[1]);
		if (jnr == null) {
			String message = Messages.getMessages().getMessage("games.jnr.notexists").replaceAll(jnrReplacement,
					args[1]);
			player.sendMessage(message);
			return;
		}

		List<JumpAndRunCheckpoint> checkpoints = jnr.getCheckpoints();

		int checkpointIndex;
		try {
			int i = Integer.parseInt(args[2]);
			if (i < 0 || i > checkpoints.size() - 1) {
				String message = Messages.getMessages().getMessage("games.jnr.checkpoint.notexists")
						.replaceAll(numberReplacement, "" + i).replaceAll(jnrReplacement, jnr.getName());
				player.sendMessage(message);
				return;
			}
			checkpointIndex = i;
		} catch (NumberFormatException e) {
			String message = Messages.getMessages().getMessage("commands.general.notinteger")
					.replaceAll(numberReplacement, args[2]);
			player.sendMessage(message);
			return;
		}
		JumpAndRunCheckpoint checkpoint = checkpoints.get(checkpointIndex);

		List<String> commands = checkpoint.getCommands();

		int commandIndex = commands.size();
		try {
			int i = Integer.parseInt(args[3]);
			if (i < 0 || i > commandIndex) {
				String message = Messages.getMessages().getMessage("games.jnr.checkpoint.command.outofbounds")
						.replaceAll(numberReplacement, "" + checkpointIndex).replaceAll(jnrReplacement, jnr.getName())
						.replaceAll("\\{INDEX\\}", "" + i);
				player.sendMessage(message);
				return;
			}
			commandIndex = i;
		} catch (NumberFormatException e) {
			String message = Messages.getMessages().getMessage("commands.general.notinteger")
					.replaceAll(numberReplacement, args[3]);
			player.sendMessage(message);
			return;
		}

		String command = Utils.combineStrings(4, args.length, args);

		commands.add(commandIndex, command);
		game.getManager().saveJumpAndRun(jnr);

		String message = Messages.getMessages().getMessage("games.jnr.checkpoint.command.add")
				.replaceAll(numberReplacement, "" + checkpointIndex).replaceAll(jnrReplacement, jnr.getName())
				.replaceAll(commandReplacement, command);
		player.sendMessage(message);

	}

	private void reset(Player player, String[] args) {
		if (args.length < 2 || args.length > 3) {
			player.sendMessage(ChatColor.GOLD + "/jnr reset <player> <jnr>");
			return;
		}

		if (args.length == 3)
			game.getManager().resetPlayer(args[1], args[2]);
		else
			game.getManager().resetPlayer(args[1]);

		String message = Messages.getMessages().getMessage("games.jnr.reset").replaceAll("\\{PLAYER\\}", args[1]);
		player.sendMessage(message);

	}
}
