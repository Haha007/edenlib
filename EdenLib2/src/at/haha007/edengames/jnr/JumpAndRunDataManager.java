package at.haha007.edengames.jnr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.edenplayer.EdenPlayerManager;

public class JumpAndRunDataManager {
	private File folder = new File(EdenLib.instance.getDataFolder(), "jumpAndRun");
	private JumpAndRunGame game;
	private EdenPlayerManager playerManager = EdenLib.instance.getEdenPlayerManager();

	JumpAndRunDataManager(JumpAndRunGame game) {
		if (!folder.exists())
			folder.mkdirs();
		this.game = game;
	}

	List<JumpAndRun> loadJumpAndRuns() {
		List<JumpAndRun> list = new ArrayList<>();
		for (File file : folder.listFiles()) {
			list.add(new JumpAndRun(file));
		}
		return list;
	}

	int getActiveCheckpoint(String player, JumpAndRun jnr) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		return cfg.getInt("games.jnr." + jnr.getName() + ".active", 0);
	}

	int getMaximalCheckpoint(String player, JumpAndRun jnr) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		return cfg.getInt("games.jnr." + jnr.getName() + ".max", 0);
	}

	void setActiveCheckpoint(String player, JumpAndRun jnr, int checkpoint) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		cfg.set("games.jnr." + jnr.getName() + ".active", checkpoint);
	}

	void setMaximalCheckpoint(String player, JumpAndRun jnr, int checkpoint) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		cfg.set("games.jnr." + jnr.getName() + ".max", checkpoint);
	}

	void resetPlayer(String player) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		cfg.set("games.jnr", null);
	}

	void resetPlayer(String player, String jnr) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		cfg.set("games.jnr." + jnr, null);
	}

	void saveJumpAndRun(JumpAndRun jnr) {
		try {
			File file = getFile(jnr);
			if (!file.exists())
				file.createNewFile();
			jnr.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public File getFolder() {
		return folder;
	}

	public File getFile(JumpAndRun jnr) {
		File file = new File(folder, jnr.getName() + ".yml");
		return file;
	}

	public void deleteJnr(JumpAndRun jnr) {
		File file = getFile(jnr);
		game.getJnrList().remove(jnr);

		if (file.exists()) {
			file.delete();
		}
	}
}
