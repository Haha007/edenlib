package at.haha007.edengames.jnr;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import at.haha007.edenlib.utils.Utils;
import net.minecraft.server.v1_13_R2.DataWatcher;
import net.minecraft.server.v1_13_R2.DataWatcherObject;
import net.minecraft.server.v1_13_R2.DataWatcherRegistry;
import net.minecraft.server.v1_13_R2.EntityTypes;
import net.minecraft.server.v1_13_R2.IRegistry;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_13_R2.PacketPlayOutSpawnEntityLiving;

public class JumpAndRunListener implements Listener {

	private String cpSelectorInvName = ChatColor.DARK_GREEN + "Jump and Run Checkpoints";
	private JumpAndRunGame game;
	private HashMap<Player, List<Integer>> guardians = new HashMap<>();
	private Random random = new Random();

	public JumpAndRunListener(JumpAndRunGame jumpAndRunGame) {
		game = jumpAndRunGame;
	}

	@EventHandler
	void onSwapItem(PlayerItemHeldEvent event) {
		ItemStack i1 = event.getPlayer().getInventory().getItem(event.getNewSlot());
		ItemStack i2 = event.getPlayer().getInventory().getItem(event.getPreviousSlot());

		String to = i1 == null ? "" : i1.getItemMeta().getDisplayName();
		String from = i2 == null ? "" : i2.getItemMeta().getDisplayName();
		if (from.startsWith(ChatColor.GREEN + "Jump and Run TOOL ")) {
			hidePath(event.getPlayer());
		}
		if (to.startsWith(ChatColor.GREEN + "Jump and Run TOOL ")) {
			showPath(event.getPlayer(),
					game.getJumpAndRun(to.replaceFirst(ChatColor.GREEN + "Jump and Run TOOL ", "")));
		}

	}

	private void showPath(Player player, JumpAndRun jnr) {
		if (jnr == null)
			return;
		List<Location> locs = new ArrayList<>();

		for (int i = 0; i < jnr.getCheckpoints().size(); i++) {
			locs.add(jnr.getCheckpointLocation(i));
		}

		List<Integer> entityIDs = new ArrayList<>();

		for (int i = 0; i < locs.size(); i++) {
			Location location = locs.get(i);
			int entityID = random.nextInt();
			entityIDs.add(entityID);
			PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving();
			DataWatcher dw = new DataWatcher(null);

			if (i > 0)
				dw.register(new DataWatcherObject<>(13, DataWatcherRegistry.b), entityIDs.get(i - 1));
			dw.register(new DataWatcherObject<>(0, DataWatcherRegistry.a), (byte) 0x20);

			set("a", packet, entityID);
			set("b", packet, UUID.randomUUID());
			set("c", packet, IRegistry.ENTITY_TYPE.a(EntityTypes.GUARDIAN));

			set("d", packet, location.getX());
			set("e", packet, location.getY());
			set("f", packet, location.getZ());

			set("j", packet, (byte) 0);
			set("k", packet, (byte) 0);
			set("l", packet, (byte) 0);

			set("g", packet, 0);
			set("h", packet, 0);
			set("i", packet, 0);

			set("m", packet, dw);

			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		guardians.put(player, entityIDs);
	}

	private void hidePath(Player player) {
		List<Integer> entityIDs = guardians.get(player);
		if (entityIDs == null)
			return;

		for (Integer entityID : entityIDs) {
			PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(entityID);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		guardians.remove(player);
	}

	@EventHandler
	void onPlayerQuit(PlayerQuitEvent event) {
		game.leaveGame(event.getPlayer());
	}

	@EventHandler
	void onPlayerTeleport(PlayerTeleportEvent event) {
		Player player = event.getPlayer();

		if (!game.containsPlayer(player))
			return;

		if (event.getFrom().getWorld() != event.getTo().getWorld()) {
			return;
		}

		JumpAndRun jnr = game.getJumpAndRun(player);
		Location cp = jnr.getCheckpointLocation(game.getManager().getActiveCheckpoint(player.getName(), jnr));

		if (cp.getWorld() != event.getTo().getWorld()) {
			game.removePlayer(event.getPlayer());
			return;
		}

		if (event.getTo().distanceSquared(cp) < 4)
			return;

		game.removePlayer(event.getPlayer());
		event.getPlayer().getInventory().clear();
	}

	@EventHandler
	void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction() == Action.PHYSICAL) {
			onPhysical(event);
		} else if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			onClick(event);
		}

	}

	@EventHandler
	void onInventoryClick(InventoryClickEvent event) {
		String title = event.getView().getTitle();
		if (title.startsWith(ChatColor.GREEN + "Jump and Run INFO ")) {
			JumpAndRun jnr = game.getJumpAndRun(title.substring(20));
			if (jnr != null && event.getClickedInventory() == event.getView().getTopInventory()) {
				jnr.clickInfoInventory((Player) event.getWhoClicked(), event.getSlot(),
						event.getClick() == ClickType.RIGHT);
				event.setCancelled(true);
				event.getWhoClicked().closeInventory();
			}
		}

		Player player = (Player) event.getWhoClicked();
		if (!game.containsPlayer(player))
			return;

		event.setCancelled(true);

		if (event.getView().getBottomInventory() == event.getClickedInventory()) {
			switch (event.getSlot()) {
			case 0:
				respawnPlayer(player);
				break;

			case 1:
				leaveGame(player);
				break;

			case 8:
				openCheckpointMenu(player);
				break;

			default:
				break;
			}
		} else if (event.getView().getTitle().equals(cpSelectorInvName)
				&& event.getClickedInventory() == event.getView().getTopInventory()) {
			ItemStack item = event.getCurrentItem();
			if (item == null || item.getType() == Material.AIR) {
				return;
			}

			int checkpointIndex = Integer
					.parseInt(item.getItemMeta().getDisplayName().replaceFirst(ChatColor.GOLD + "Checkpoint ", ""));
			checkpointIndex = event.getSlot();
			JumpAndRun jnr = game.getJumpAndRun(player);
			int max = game.getManager().getMaximalCheckpoint(player.getName(), jnr);
			if (max < checkpointIndex) {
				return;
			}
			game.getManager().setActiveCheckpoint(player.getName(), jnr, checkpointIndex);
			game.respawn(player);
			player.closeInventory();
		}
	}

	// Checkpoint Listener
	private void onPhysical(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		JumpAndRun jnr = game.getJumpAndRun(player);
		if (jnr == null)
			return;
		event.setCancelled(true);

		JumpAndRunDataManager manager = game.getManager();

		int checkpointIndex = manager.getActiveCheckpoint(player.getName(), jnr) + 1;
		ArrayList<JumpAndRunCheckpoint> checkpoints = jnr.getCheckpoints();

		if (checkpoints.size() <= checkpointIndex)
			return;

		Block block = event.getClickedBlock();
		Location loc = jnr.getCheckpointLocation(checkpointIndex);

		if (block.getX() != loc.getBlockX() || block.getY() != loc.getBlockY() || block.getZ() != loc.getBlockZ())
			return;

		manager.setActiveCheckpoint(player.getName(), jnr, checkpointIndex);

		player.sendActionBar(ChatColor.GOLD + "Checkpoint   " + checkpointIndex + " / " + (checkpoints.size() - 1));

		if (jnr.showNextCheckpoint)
			game.showNextCheckpoint(player);

		if (manager.getMaximalCheckpoint(player.getName(), jnr) >= checkpointIndex)
			return;

		game.getManager().setMaximalCheckpoint(player.getName(), jnr, checkpointIndex);
		JumpAndRunCheckpoint checkpoint = checkpoints.get(checkpointIndex);
		checkpoint.executeCommands(player.getName());

	}

	// Respawn / Leave / Checkpoint Inventory
	private void onClick(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (!game.containsPlayer(player))
			return;

		event.setCancelled(true);

		switch (player.getInventory().getHeldItemSlot()) {
		case 0:
			respawnPlayer(player);
			break;

		case 1:
			leaveGame(player);
			break;

		case 8:
			openCheckpointMenu(player);
			break;

		default:
			break;
		}
	}

	private void openCheckpointMenu(Player player) {
		JumpAndRun jnr = game.getJumpAndRun(player);
		Inventory inv = Bukkit.createInventory(null, 9 * (jnr.getCheckpoints().size() / 9 + 1), cpSelectorInvName);
		int cp = game.getManager().getMaximalCheckpoint(player.getName(), jnr);

		ArrayList<JumpAndRunCheckpoint> checkpoints = jnr.getCheckpoints();
		for (int i = 0; i < checkpoints.size(); i++) {
			Material material = cp >= i ? Material.GREEN_STAINED_GLASS_PANE : Material.RED_STAINED_GLASS_PANE;
			inv.setItem(i, Utils.getItem(material, ChatColor.GOLD + "Checkpoint " + i));
		}

		player.openInventory(inv);
	}

	private void leaveGame(Player player) {
		game.leaveGame(player);
	}

	void respawnPlayer(Player player) {
		game.respawn(player);
	}

	private void set(String field_, Object obj, Object value) {
		try {
			Field field = obj.getClass().getDeclaredField(field_);
			boolean accassible = field.isAccessible();
			field.setAccessible(true);
			field.set(obj, value);
			field.setAccessible(accassible);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
