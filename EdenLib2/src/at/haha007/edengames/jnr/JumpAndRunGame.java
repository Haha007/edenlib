package at.haha007.edengames.jnr;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.PlayerInventory;

import at.haha007.edengames.utils.InterfaceEdengame;
import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.commandexecutos.CE_EdenLib;
import at.haha007.edenlib.utils.Settings;
import at.haha007.edenlib.utils.Utils;
import net.minecraft.server.v1_13_R2.Blocks;
import net.minecraft.server.v1_13_R2.DataWatcher;
import net.minecraft.server.v1_13_R2.DataWatcherObject;
import net.minecraft.server.v1_13_R2.DataWatcherRegistry;
import net.minecraft.server.v1_13_R2.MobEffect;
import net.minecraft.server.v1_13_R2.MobEffects;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntityEffect;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_13_R2.PacketPlayOutSpawnEntity;

public class JumpAndRunGame implements InterfaceEdengame {

	private JumpAndRunDataManager manager;
	private List<JumpAndRun> jnrList;
	private HashMap<Player, JumpAndRun> active = new HashMap<>();
	private JumpAndRunListener listener = new JumpAndRunListener(this);
	private JumpAndRunCommand command = new JumpAndRunCommand(this);
	private UUID uuid = UUID.randomUUID();
	private int entityID = new Random().nextInt(Integer.MAX_VALUE);

	//

	public JumpAndRunGame() {
		manager = new JumpAndRunDataManager(this);
		jnrList = manager.loadJumpAndRuns();
		Bukkit.getServer().getPluginManager().registerEvents(listener, EdenLib.instance);
		EdenLib.instance.getCommand("edenjumpandrun").setExecutor(command);
		EdenLib.instance.getCommand("edenjumpandrun").setTabCompleter(command);
	}

	public List<JumpAndRun> getJnrList() {
		return jnrList;
	}

	JumpAndRun getJumpAndRun(Player player) {
		return active.get(player);
	}

	public JumpAndRunDataManager getManager() {
		return manager;
	}

	public Location getLobby() {
		return Settings.jumpAndRunLobby;
	}

	public void setLobby(Location loc) {
		CE_EdenLib ceEdenlib = EdenLib.instance.getCE_EdenLib();
		File file = ceEdenlib.getFile();
		YamlConfiguration cfg = ceEdenlib.getConfig();

		cfg.set("jumpandrunspawn.x", loc.getX());
		cfg.set("jumpandrunspawn.y", loc.getY());
		cfg.set("jumpandrunspawn.z", loc.getZ());
		cfg.set("jumpandrunspawn.pitch", loc.getPitch());
		cfg.set("jumpandrunspawn.yaw", loc.getYaw());
		cfg.set("jumpandrunspawn.world", loc.getWorld().getName());

		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

		ceEdenlib.reload();
	}

	public void addJumpAndRun(JumpAndRun jnr) {
		jnrList.add(jnr);
		saveJumpAndRun(jnr);
	}

	void saveJumpAndRun(JumpAndRun jnr) {
		manager.saveJumpAndRun(jnr);
	}

	JumpAndRun getJumpAndRun(String name) {
		for (JumpAndRun jumpAndRun : jnrList) {
			if (jumpAndRun.getName().toLowerCase().equals(name))
				return jumpAndRun;
		}

		return null;
	}

	void start(Player player, JumpAndRun jnr) {
		if (jnr == null)
			return;
		if (jnr.getCheckpoints().size() == 0)
			return;
		if (!player.hasPermission("eden.edengames.jnr.start." + jnr.getName().toLowerCase()))
			return;
		active.put(player, jnr);
		respawn(player);
	}

	// InterfaceEdengame Methoden

	@Override
	public void unregister() {
		HandlerList.unregisterAll(listener);
		EdenLib.instance.getCommand("edenjumpandrun").setExecutor(null);
		EdenLib.instance.getCommand("edenjumpandrun").setTabCompleter(null);
		for (Player player : active.keySet()) {
			leaveGame(player);
		}
	}

	@Override
	public boolean containsPlayer(Player player) {
		return active.containsKey(player);
	}

	@Override
	public boolean isMultiplayer() {
		return true;
	}

	@Override
	public void removePlayer(Player player) {
		active.remove(player);
	}

	@Override
	public HashMap<String, CommandExecutor> getCommandExecutors() {
		HashMap<String, CommandExecutor> commands = new HashMap<>();
		commands.put("edenjumpandrun", command);
		return commands;
	}

	@Override
	public HashMap<String, TabCompleter> getCommandCompleters() {
		HashMap<String, TabCompleter> tabCompleter = new HashMap<>();
		tabCompleter.put("edenjumpandrun", command);
		return tabCompleter;
	}

	@Override
	public List<Listener> getListeners() {
		return Arrays.asList(new Listener[] { listener });
	}

	public void respawn(Player player) {

		JumpAndRun jnr = getJumpAndRun(player);
		if (jnr == null)
			return;
		int index = getManager().getActiveCheckpoint(player.getName(), jnr);
		Location location = jnr.getCheckpointLocation(index);
		if (location == null) {
			leaveGame(player);
			return;
		}
		player.teleport(location);
		player.setGameMode(GameMode.ADVENTURE);
		player.setHealth(20);
		player.setFoodLevel(20);
		player.setFireTicks(0);
		PlayerInventory inv = player.getInventory();

		inv.clear();
		inv.setItem(0, Utils.getItem(Material.ROSE_RED, ChatColor.GOLD + "Respawn"));
		inv.setItem(1, Utils.getItem(Material.DANDELION_YELLOW, ChatColor.GOLD + "Jump and Run verlassen"));
		inv.setItem(8, Utils.getItem(Material.CYAN_DYE, ChatColor.GOLD + "Checkpoint wählen"));

		if (jnr.showNextCheckpoint)
			showNextCheckpoint(player);
	}

	public void leaveGame(Player player) {
		if (!active.containsKey(player))
			return;

		player.getInventory().clear();
		player.teleport(getLobby());
		active.remove(player);

		((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(entityID));
	}

	public void showNextCheckpoint(Player player) {
		JumpAndRun jnr = getJumpAndRun(player);

		if (jnr == null)
			return;

		int checkpoint = manager.getActiveCheckpoint(player.getName(), jnr) + 1;

		if (jnr.getCheckpoints().size() <= checkpoint)
			return;

		sendFakeFallingBlock(jnr.getCheckpointLocation(checkpoint), player);
	}

	private void sendFakeFallingBlock(Location loc, Player player) {
		PacketPlayOutSpawnEntity packetSpawn = new PacketPlayOutSpawnEntity();

		// spawn falling block
		set("a", packetSpawn, entityID);
		set("b", packetSpawn, uuid);
		set("c", packetSpawn, loc.getX());
		set("d", packetSpawn, loc.getY());
		set("e", packetSpawn, loc.getZ());
		set("f", packetSpawn, 0);
		set("g", packetSpawn, 0);
		set("h", packetSpawn, 0);
		set("i", packetSpawn, 0);
		set("j", packetSpawn, 0);
		set("k", packetSpawn, 70);
		// id + (data << 12)
		set("l", packetSpawn, net.minecraft.server.v1_13_R2.Block.REGISTRY_ID
				.getId(Blocks.LIGHT_WEIGHTED_PRESSURE_PLATE.getBlockData()));
		// 5604

		// glowing
		MobEffect mobEffect = new MobEffect(MobEffects.GLOWING, Integer.MAX_VALUE, 1);
		PacketPlayOutEntityEffect packetEffect = new PacketPlayOutEntityEffect(entityID, mobEffect);

		// nogravity
		DataWatcher dw = new DataWatcher(null);
		dw.register(new DataWatcherObject<>(5, DataWatcherRegistry.i), true);
		dw.register(new DataWatcherObject<>(0, DataWatcherRegistry.a), (byte) 0x40);
		PacketPlayOutEntityMetadata packetMeta = new PacketPlayOutEntityMetadata();
		set("a", packetMeta, entityID);
		set("b", packetMeta, dw.c());

		// send the packets
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetSpawn);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetEffect);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetMeta);
	}

	private void set(String field_, Object obj, Object value) {
		try {
			Field field = obj.getClass().getDeclaredField(field_);
			boolean accassible = field.isAccessible();
			field.setAccessible(true);
			field.set(obj, value);
			field.setAccessible(accassible);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
