package at.haha007.edenutils.commandexecutors;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.Utils;

public class CE_ItemTool implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Messages.getMessages().getMessage("commands.general.notplayer"));
            return true;
        }
        Player player = (Player) sender;
        if (args.length < 2) {
            sendInfo(player);
            return true;
        }

        switch (args[0].toLowerCase()) {
            case "rename": {
                if (!player.hasPermission("eden.edenutils.itemtool.rename")) {
                    sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
                    return true;
                }

                ItemStack item = player.getInventory().getItemInMainHand();
                if (item == null || item.getType() == Material.AIR) {
                    player.sendMessage(Messages.getMessages().getMessage("commands.general.noitem"));
                    return true;
                }

                String name = Utils.combineStrings(1, args.length, args);
                if (player.hasPermission("eden.edenutils.itemtool.color"))
                    name = ChatColor.translateAlternateColorCodes('&', name);

                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName(name);
                item.setItemMeta(meta);

                player.getInventory().setItemInMainHand(item);
            }
            break;
            case "setenchantable": {
                if (!player.hasPermission("eden.edenutils.itemtool.enchantable")) {
                    sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
                    return true;
                }

                ItemStack item = player.getInventory().getItemInMainHand();
                if (item == null || item.getType() == Material.AIR) {
                    player.sendMessage(Messages.getMessages().getMessage("commands.general.noitem"));
                    return true;
                }

                boolean enchantable;
                switch (args[1].toLowerCase()) {
                    case "true":
                        enchantable = true;
                        break;
                    case "false":
                        enchantable = false;
                        break;

                    default:
                        sendInfo(player);
                        return true;
                }

                item = Utils.setEnchantable(item, enchantable);

                player.getInventory().setItemInMainHand(item);
            }
            break;
            case "addlore": {
                if (!player.hasPermission("eden.edenutils.itemtool.lore")) {
                    sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
                    return true;
                }

                ItemStack item = player.getInventory().getItemInMainHand();
                if (item == null || item.getType() == Material.AIR) {
                    sender.sendMessage(Messages.getMessages().getMessage("commands.general.noitem"));
                    return true;
                }

                String string = Utils.combineStrings(1, args.length, args);
                if (player.hasPermission("eden.edenutils.itemtool.color"))
                    string = ChatColor.translateAlternateColorCodes('&', string);

                ItemMeta meta = item.getItemMeta();
                List<String> lore = meta.getLore();
                if (lore == null)
                    lore = new ArrayList<>();
                lore.add(string);
                meta.setLore(lore);
                item.setItemMeta(meta);

                player.getInventory().setItemInMainHand(item);
            }
            break;
            case "setlore": {
                if (!player.hasPermission("eden.edenutils.itemtool.lore")) {
                    sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
                    return true;
                }

                int index;
                try {
                    index = Integer.parseInt(args[1]);
                } catch (NumberFormatException e) {
                    sender.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
                            .replaceAll("\\{NUMBER\\}", args[1]));
                    return true;
                }

                ItemStack item = player.getInventory().getItemInMainHand();
                if (item == null || item.getType() == Material.AIR) {
                    sender.sendMessage(Messages.getMessages().getMessage("commands.general.noitem"));
                    return true;
                }

                String string = Utils.combineStrings(2, args.length, args);
                if (player.hasPermission("eden.edenutils.itemtool.color"))
                    string = ChatColor.translateAlternateColorCodes('&', string);

                ItemMeta meta = item.getItemMeta();
                List<String> lore = meta.getLore();
                if (lore == null)
                    lore = new ArrayList<>();
                try {
                    lore.set(index, string);
                } catch (IndexOutOfBoundsException e) {
                    player.sendMessage(Messages.getMessages().getMessage("commands.itemtool.lore.linenotexists")
                            .replaceAll("\\{NUMBER\\}", Integer.toString(index)));
                    return true;
                }
                meta.setLore(lore);
                item.setItemMeta(meta);

                player.getInventory().setItemInMainHand(item);
            }
            break;
            case "removelore": {
                if (!player.hasPermission("eden.edenutils.itemtool.lore")) {
                    sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
                    return true;
                }

                ItemStack item = player.getInventory().getItemInMainHand();
                if (item == null || item.getType() == Material.AIR) {
                    sender.sendMessage(Messages.getMessages().getMessage("commands.general.noitem"));
                    return true;
                }

                ItemMeta meta = item.getItemMeta();
                List<String> lore = meta.getLore();
                if (lore == null || lore.isEmpty()) {
                    player.sendMessage(Messages.getMessages().getMessage("commands.itemtool.lore.notexists"));
                    return true;
                }

                int index;
                if (args.length == 1) {
                    index = lore.size() - 1;
                } else {
                    try {
                        index = Integer.parseInt(args[1]);
                    } catch (NumberFormatException e) {
                        sender.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
                                .replaceAll("\\{NUMBER\\}", args[1]));
                        return true;
                    }
                }

                try {
                    lore.remove(index);
                } catch (IndexOutOfBoundsException e) {
                    player.sendMessage(Messages.getMessages().getMessage("commands.itemtool.lore.linenotexists")
                            .replaceAll("\\{NUMBER\\}", Integer.toString(index)));
                    return true;
                }
                meta.setLore(lore);
                item.setItemMeta(meta);

                player.getInventory().setItemInMainHand(item);
            }
            break;

            default:
                break;
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> list = new ArrayList<>();
        if (args.length == 1) {
            String arg0 = args[0].toLowerCase();
            if ("rename".startsWith(arg0))
                list.add("rename");
            if ("addlore".startsWith(arg0))
                list.add("addlore");
            if ("setlore".startsWith(arg0))
                list.add("setlore");
            if ("removelore".startsWith(arg0))
                list.add("removelore");
            if ("setenchantable".startsWith(arg0))
                list.add("setenchantable");
        }
        return list;
    }

    private void sendInfo(Player p) {
        p.sendMessage(ChatColor.GOLD + "/itemtool rename <name>");
        p.sendMessage(ChatColor.GOLD + "/itemtool setenchantable [true/false]");
        p.sendMessage(ChatColor.GOLD + "/itemtool addlore <name>");
        p.sendMessage(ChatColor.GOLD + "/itemtool setlore <line> <name>");
        p.sendMessage(ChatColor.GOLD + "/itemtool removelore <line>");
    }
}