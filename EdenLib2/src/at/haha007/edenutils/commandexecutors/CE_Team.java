package at.haha007.edenutils.commandexecutors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.Utils;

public class CE_Team implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("eden.edenbugfixes.team.team")) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.GOLD + "/team message");
			return true;
		}

		String name = ChatColor.DARK_RED + sender.getName() + ChatColor.WHITE;
		if (sender instanceof Player)
			name = ((Player) sender).getDisplayName();

		String message = Utils.combineStrings(0, args.length - 1, args);
		Bukkit.broadcast(ChatColor.DARK_PURPLE + "[" + ChatColor.LIGHT_PURPLE + "TeamChat" + ChatColor.DARK_PURPLE
				+ "] " + name + " " + message, "eden.edenutils.team.team");
		return true;
	}

}
