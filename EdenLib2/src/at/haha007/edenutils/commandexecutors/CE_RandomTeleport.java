package at.haha007.edenutils.commandexecutors;

import java.util.HashSet;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import at.haha007.edenlib.utils.Messages;

public class CE_RandomTeleport implements CommandExecutor {

	private static HashSet<Material> blocked = new HashSet<>();
	private Random random = new Random();

	static {
		blocked.add(Material.LAVA);
		blocked.add(Material.WATER);
		blocked.add(Material.COBWEB);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("eden.edenutils.randomtp.rtp")) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			return true;
		}
		Location location = null;
		if (sender instanceof BlockCommandSender)
			location = ((BlockCommandSender) sender).getBlock().getLocation();
		if (sender instanceof Player)
			location = ((Player) sender).getLocation();
		if (location == null) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.notplayerorblock"));
			return true;
		}
		if (args.length != 2) {
			sender.sendMessage(ChatColor.RED + "/rtp <radius> <radius>");
			return true;
		}
		double r1, r2;
		try {
			r1 = Double.parseDouble(args[0]);
			r2 = Double.parseDouble(args[1]);
		} catch (NumberFormatException e) {
			sender.sendMessage(ChatColor.RED + "/rtp <radius> <radius>");
			return true;
		}
		if (r1 <= 0) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.numbertoosmall")
					.replaceAll("\\{NUMBER\\}", args[0]).replaceAll("\\{MINIMUM\\}", "0"));
			return true;
		}
		if (r2 <= 0) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.numbertoosmall")
					.replaceAll("\\{NUMBER\\}", args[1]).replaceAll("\\{MINIMUM\\}", "0"));
			return true;
		}
		r1 *= r1;
		HashSet<Player> inRange = new HashSet<>();
		for (Player player : location.getWorld().getPlayers()) {
			if (player.getLocation().distanceSquared(location) < r1)
				inRange.add(player);
		}
		for (Player player : inRange) {
			Location to = getRandomLocaton(location, r2);
			if (to != null)
				player.teleport(to);
		}
		return true;
	}

	private Location getRandomLocaton(Location location, double radius) {
		for (int i = 0; i < 10; i++) {
			double r = Math.sqrt(radius * radius / 2 * random.nextDouble());
			double a = 2 * Math.PI * random.nextDouble();

			double z = Math.sin(a) * r;
			double x = Math.cos(a) * r;

			Location loc = location.clone();
			Material mat = null;
			loc.add(x, 0, z);
			for (int j = 255; j > 0; j--) {
				loc.setY(j);
				mat = loc.getBlock().getType();
				if (mat != Material.AIR) {
					loc.setY(j + 1);
					break;
				}
			}
			if (!blocked.contains(mat))
				return loc.getBlock().getLocation().add(0.5, 0.5, 0.5);
		}
		return null;
	}
}
