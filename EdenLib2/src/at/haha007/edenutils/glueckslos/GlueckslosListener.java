package at.haha007.edenutils.glueckslos;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Utils;
import net.ess3.api.MaxMoneyException;

public class GlueckslosListener implements Listener, CommandExecutor {
	// types: money, item
	// reward message
	// inventory with all items and weights

	private List<GlueckslosEntry> entries;
	private int size = 0;

	public GlueckslosListener() {
		load();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		// gueckslos give [player] amount
		return false;
	}

	private void load() {
		ConfigurationSection cfg = EdenLib.instance.getCE_EdenLib().getConfig().getConfigurationSection("glueckslos");
	}

	private void save() {
		YamlConfiguration cfg = EdenLib.instance.getCE_EdenLib().getConfig();
	}

	private ItemStack getRandom() {

		return null;
	}

	ItemStack getGlueckslos() {
		ItemStack item = Utils.getItem(Material.PAPER, ChatColor.GOLD + "Gl\u00FCckslos",
				ChatColor.AQUA + "Gewinne seltene Preise!");
		item = Utils.setEnchantable(item, false);
		return item;
	}

	private class GlueckslosEntry {
		private Object entry;
		private int weight;

		public GlueckslosEntry(ItemStack item) {
			entry = item;
		}

		public GlueckslosEntry(double money) {
			entry = money;
		}

		private void give(Player player) {
			switch (getType()) {
			case ITEM:
				Utils.giveItem(player, (ItemStack) entry);
				break;
			case MONEY:
				try {
					EdenLib.essentials.getUser(player).giveMoney(BigDecimal.valueOf((double) entry));
				} catch (MaxMoneyException e) {
				}
				break;

			default:
				break;
			}
		}

		public EntryType getType() {
			if (entry instanceof Double)
				return EntryType.MONEY;
			if (entry instanceof ItemStack)
				return EntryType.ITEM;
			return null;
		}
	}

	private enum EntryType {
		MONEY, ITEM;
	}
}
