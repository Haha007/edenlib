package at.haha007.edenutils.quests;

import java.util.Hashtable;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;

public class QuestCollectItems implements InterfaceQuest {

	private Hashtable<Material, Integer> items = new Hashtable<>();
	private double reward;

	@Override
	public void addEntry(String type, int amount) {
		Material material = Material.getMaterial(type.toUpperCase());

		if (material == null) {
			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.RED + "[EdenLib] [Quest] Material " + type + " does not exist!");
			return;
		}

		items.put(material, amount);

	}

	@Override
	public InterfaceQuest copy() {
		QuestCollectItems quest = new QuestCollectItems();
		quest.items.putAll(items);
		quest.reward = reward;
		return quest;
	}

	@Override
	public Hashtable<String, Integer> getEntries() {
		Hashtable<String, Integer> entries = new Hashtable<>();
		for (Entry<Material, Integer> entry : items.entrySet()) {
			entries.put(entry.getKey().toString(), entry.getValue());
		}
		return entries;
	}

	public Hashtable<Material, Integer> getItems() {
		return items;
	}

	@Override
	public double getReward() {
		return reward;
	}

	@Override
	public void setReward(double reward) {
		this.reward = reward;
	}
}
