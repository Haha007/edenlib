package at.haha007.edenutils.quests;

import java.util.Hashtable;

public interface InterfaceQuest {

	public void addEntry(String type, int amount);

	public Hashtable<String, Integer> getEntries();

	public void setReward(double reward);

	public double getReward();

	public InterfaceQuest copy();

}
