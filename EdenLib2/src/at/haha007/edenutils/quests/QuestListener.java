package at.haha007.edenutils.quests;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.earth2me.essentials.api.Economy;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;

import at.haha007.edenlib.utils.Messages;

public class QuestListener implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	void onEntityDamage(EntityDamageByEntityEvent event) {

		if (!(event.getDamager() instanceof Player))
			return;
		Player player = (Player) event.getDamager();

		if (!player.hasPermission("eden.edenutils.quest.work"))
			return;

		QuestManager manager = QuestManager.getInstance();

		InterfaceQuest generalQuest = manager.getActiveQuest(player);
		if (generalQuest == null)
			return;
		if (!(generalQuest instanceof QuestKillMobs))
			return;

		QuestKillMobs quest = (QuestKillMobs) generalQuest;
		Hashtable<EntityType, Integer> entities = quest.getEntities();
		EntityType type = event.getEntity().getType();

		int amount = entities.get(type);
		if (amount <= 0)
			return;

		amount--;
		entities.put(type, amount);
		player.sendActionBar(ChatColor.GOLD + type.toString() + ": " + amount);

		for (Entry<EntityType, Integer> entry : entities.entrySet()) {
			if (entry.getValue() > 0) {
				manager.setActiveQuest(player, quest);
				return;
			}
		}
		player.sendMessage(Messages.getMessages().getMessage("quest.done"));
		manager.setActiveQuest(player, null);
		try {
			Economy.add(player.getName(), BigDecimal.valueOf(quest.getReward()));
		} catch (NoLoanPermittedException | ArithmeticException | UserDoesNotExistException e) {
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();

		if (!player.hasPermission("eden.edenutils.quest.work"))
			return;

		QuestManager manager = QuestManager.getInstance();
		InterfaceQuest generalQuest = manager.getActiveQuest(player);
		if (!(generalQuest instanceof QuestBreakBlocks))
			return;

		QuestBreakBlocks quest = (QuestBreakBlocks) generalQuest;
		Hashtable<Material, Integer> blocks = quest.getBlocks();
		Material type = event.getBlock().getType();

		int amount = blocks.get(type);
		if (amount <= 0)
			return;

		amount--;
		blocks.put(type, amount);
		player.sendActionBar(ChatColor.GOLD + type.toString() + ": " + amount);

		for (Entry<Material, Integer> entry : blocks.entrySet()) {
			if (entry.getValue() > 0) {
				manager.setActiveQuest(player, quest);
				return;
			}
		}

		player.sendMessage(Messages.getMessages().getMessage("quest.done"));
		manager.setActiveQuest(player, null);
		manager.setActiveQuest(player, null);
		try {
			Economy.add(player.getName(), BigDecimal.valueOf(quest.getReward()));
		} catch (NoLoanPermittedException | ArithmeticException | UserDoesNotExistException e) {
		}
	}
}
