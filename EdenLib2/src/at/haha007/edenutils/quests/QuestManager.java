package at.haha007.edenutils.quests;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.edenplayer.EdenPlayerManager;

public class QuestManager {
	private static QuestManager instance;

	private File file;
	private ArrayList<InterfaceQuest> quests = new ArrayList<>();
	private EdenPlayerManager epManager = EdenLib.instance.getEdenPlayerManager();
	private Random random = new Random();
	private QuestCash cash;

	public static QuestManager getInstance() {
		return instance == null ? instance = new QuestManager() : instance;
	}

	private QuestManager() {
		file = new File(EdenLib.instance.getDataFolder(), "quests.yml");
		loadQuests();
		cash = new QuestCash();
		Bukkit.getScheduler().scheduleSyncRepeatingTask(EdenLib.instance, new Runnable() {
			@Override
			public void run() {
				cash.tick();
			}
		}, 0, 100);
	}

	private void loadQuests() {
		if (!file.exists()) {
			try {
				file.createNewFile();
				YamlConfiguration cfg = YamlConfiguration
						.loadConfiguration(new InputStreamReader(EdenLib.instance.getResource("assets/quests.yml")));
				cfg.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		for (String key : cfg.getKeys(false)) {
			InterfaceQuest quest = getQuest(cfg.getConfigurationSection(key));
			if (quest != null) {
				quests.add(quest);
			}
		}
	}

	private InterfaceQuest getQuest(ConfigurationSection cfg) {
		EnumQuestType type = EnumQuestType.fromString(cfg.getString("type"));
		if (type == null)
			return null;

		InterfaceQuest quest = type.getQuest();
		if (quest == null)
			return null;

		ConfigurationSection entrySection = cfg.getConfigurationSection("entries");

		for (String key : entrySection.getKeys(false)) {
			quest.addEntry(key, entrySection.getInt(key));
		}

		quest.setReward(cfg.getDouble("reward"));

		return quest;
	}

	public InterfaceQuest getRandomQuest() {
		return quests.get(random.nextInt(quests.size())).copy();
	}

	public InterfaceQuest getActiveQuest(Player player) {
		return getActiveQuest(player.getName());
	}

	public InterfaceQuest getActiveQuest(String player) {
		InterfaceQuest quest = null;
		if (!cash.contains(player)) {
			ConfigurationSection cfg = epManager.getEdenPlayer(player).getConfig()
					.getConfigurationSection("quest.active");
			if (cfg == null)
				return null;
			quest = getQuest(cfg);
			cash.set(player, quest);
		} else {
			quest = cash.get(player);
		}
		return quest;
	}

	public void setActiveQuest(Player player, InterfaceQuest quest) {
		if (quest == null) {
			epManager.getEdenPlayer(player).getConfig().set("quest.active", null);
			cash.set(player.getName(), quest);
			return;
		}

		ConfigurationSection cfg = epManager.getEdenPlayer(player).getConfig().createSection("quest.active");
		cfg.set("type", EnumQuestType.fromClass(quest.getClass()).getKey());
		for (Entry<String, Integer> entry : quest.getEntries().entrySet()) {
			cfg.set("entries." + entry.getKey(), entry.getValue());
		}

		cash.set(player.getName(), quest);
	}

	private class QuestCash {
		private HashMap<String, InterfaceQuest> cash = new HashMap<>();
		private HashMap<String, Integer> time = new HashMap<>();

		void tick() {
			Set<String> remove = new HashSet<>();
			for (String key : cash.keySet()) {
				if (!time.containsKey(key)) {
					time.put(key, 0);
				}
				int timing = time.get(key);
				time.put(key, timing + 1);
				if (timing > 5) {
					remove.add(key);
				}
			}

			for (String key : remove) {
				set(key, null);
			}

			Set<String> timeRemove = new HashSet<>();
			for (String key : time.keySet()) {
				if (!cash.containsKey(key))
					timeRemove.add(key);
			}

			for (String key : timeRemove) {
				time.remove(key);
			}
		}

		boolean contains(String player) {
			time.put(player.toLowerCase(), 0);
			return cash.containsKey(player.toLowerCase());
		}

		void set(String player, InterfaceQuest quest) {
			if (quest == null) {
				cash.remove(player.toLowerCase());
				time.remove(player.toLowerCase());
				return;
			}
			cash.put(player.toLowerCase(), quest);
			time.put(player.toLowerCase(), 0);
		}

		InterfaceQuest get(String player) {
			time.put(player.toLowerCase(), 0);
			return cash.get(player.toLowerCase());
		}
	}
}
