package at.haha007.edenutils.quests;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.earth2me.essentials.api.Economy;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.edenplayer.EdenPlayer;
import at.haha007.edenlib.utils.Messages;

public class QuestCommand implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.notplayer"));
			return true;
		}
		Player player = (Player) sender;
		if (args.length == 0) {
			sendInfo(player);
			return true;
		}

		switch (args[0].toLowerCase()) {
		case "get":
			if (player.hasPermission("eden.edenutils.quest.get"))
				getQuest(player);
			break;
		case "done":
			if (player.hasPermission("eden.edenutils.quest.work"))
				finishQuest(player);
			break;
		case "cancel":
			if (player.hasPermission("eden.edenutils.quest.cancel"))
				cancelQuest(player);
			break;
		case "info":
			if (player.hasPermission("eden.edenutils.quest.info"))
				sendQuestInfo(player);
			break;

		default:
			sendInfo(player);
			break;
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> list = new ArrayList<>();
		if (args.length == 1) {
			String[] strings = new String[] { "get", "done", "cancel", "info" };
			for (String string : strings) {
				String arg = args[0].toLowerCase();
				if (string.startsWith(arg))
					list.add(string);
			}
		}
		return list;
	}

	private void cancelQuest(Player player) {
		QuestManager questManager = QuestManager.getInstance();
		if (questManager.getActiveQuest(player) == null) {
			player.sendMessage(Messages.getMessages().getMessage("quest.noactive"));
		} else {
			player.sendMessage(Messages.getMessages().getMessage("quest.cancel"));
			questManager.setActiveQuest(player, null);
		}
	}

	private void sendQuestInfo(Player player) {
		QuestManager questManager = QuestManager.getInstance();
		InterfaceQuest active = questManager.getActiveQuest(player);
		if (active == null) {
			player.sendMessage(Messages.getMessages().getMessage("quest.noactive"));
		} else {

			switch (EnumQuestType.fromClass(active.getClass())) {
			case COLLECT_ITEMS:
				player.sendMessage(Messages.getMessages().getMessage("quest.get.items"));
				break;
			case KILL_MOBS:
				player.sendMessage(Messages.getMessages().getMessage("quest.get.kill"));
				break;
			case MINE_BLOCKS:
				player.sendMessage(Messages.getMessages().getMessage("quest.get.blocks"));
				break;
			default:
				break;
			}

			Hashtable<String, Integer> entries = active.getEntries();
			for (Entry<String, Integer> entry : entries.entrySet()) {
				player.sendMessage(ChatColor.AQUA + entry.getKey() + ": " + ChatColor.GREEN + entry.getValue());
			}
		}
	}

	private void finishQuest(Player player) {
		QuestManager manager = QuestManager.getInstance();
		InterfaceQuest generalQuest = manager.getActiveQuest(player);
		if (generalQuest == null) {
			player.sendMessage(Messages.getMessages().getMessage("quest.noactive"));
			return;
		}

		if (!(generalQuest instanceof QuestCollectItems)) {
			player.sendMessage(Messages.getMessages().getMessage("quest.done.wrongtype"));
			return;
		}

		QuestCollectItems quest = (QuestCollectItems) generalQuest;
		Hashtable<Material, Integer> collect = quest.getItems();
		Hashtable<Material, Integer> playerItems = new Hashtable<Material, Integer>();

		// count items in inventory
		for (ItemStack item : player.getInventory()) {
			if (item != null) {
				ItemMeta meta = item.getItemMeta();
				if (!(meta.hasEnchants() || meta.hasDisplayName() || meta.hasLore())) {
					Material material = item.getType();
					int newAmount = (playerItems.containsKey(material) ? playerItems.get(material) : 0)
							+ item.getAmount();
					playerItems.put(material, newAmount);
				}
			}
		}

		for (Entry<Material, Integer> entry : playerItems.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}

		// check if player has enougth items
		for (Entry<Material, Integer> entry : collect.entrySet()) {
			Material material = entry.getKey();
			int amount = entry.getValue();
			if (playerItems.get(material) <= amount) {
				player.sendMessage(Messages.getMessages().getMessage("quest.done.missingitem"));
				return;
			}
		}

		// remove items from inventory
		for (ItemStack item : player.getInventory()) {
			ItemMeta meta = item.getItemMeta();
			if (!(meta.hasEnchants() || meta.hasDisplayName() || meta.hasLore())) {
				Material material = item.getType();
				int amount = item.getAmount();
				int remove = collect.get(material);
				if (amount < remove) {
					item.setAmount(0);
					collect.put(material, remove - amount);
				} else {
					item.setAmount(amount - remove);
					collect.remove(material);
					if (collect.isEmpty())
						break;
				}
			}
		}

		player.sendMessage(Messages.getMessages().getMessage("quest.done"));
		manager.setActiveQuest(player, null);
		try {
			Economy.add(player.getName(), BigDecimal.valueOf(quest.getReward()));
		} catch (NoLoanPermittedException | ArithmeticException | UserDoesNotExistException e) {
		}
	}

	private void getQuest(Player player) {
		EdenPlayer ep = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player);
		YamlConfiguration cfg = ep.getConfig();

		if (cfg.contains("quest.active")) {
			player.sendMessage(Messages.getMessages().getMessage("quest.active"));
			return;
		}

		Calendar old = Calendar.getInstance();
		Calendar today = Calendar.getInstance();
		Long timestamp = cfg.getLong("quest.date", 0);
		old.setTimeInMillis(timestamp);

		if (cfg.getInt("quest.today", 0) >= 3) {
			if (old.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)
					&& old.get(Calendar.YEAR) == today.get(Calendar.YEAR)) {
				player.sendMessage(Messages.getMessages().getMessage("quest.get.alldone"));
				return;
			}
			cfg.set("quest.date", System.currentTimeMillis());
		}

		cfg.set("quest.today", cfg.getInt("quest.today") + 1);
		QuestManager manager = QuestManager.getInstance();
		manager.setActiveQuest(player, manager.getRandomQuest());
		sendQuestInfo(player);
	}

	private void sendInfo(Player player) {
		player.sendMessage(ChatColor.GOLD + "-- Tägliche Quests --");
		player.sendMessage(ChatColor.GOLD + "/quest get " + ChatColor.GREEN + " - Erhalte eine neue Quest.");
		player.sendMessage(ChatColor.GOLD + "/quest done" + ChatColor.GREEN + " - Beende eine Quest.");
		player.sendMessage(ChatColor.GOLD + "/quest cancel" + ChatColor.GREEN + " - Breche eine Quest ab.");
		player.sendMessage(
				ChatColor.GOLD + "/quest info" + ChatColor.GREEN + " - Informationen über deine aktuelle Quest.");
	}

}
