package at.haha007.edenutils.quests;

public enum EnumQuestType {
	KILL_MOBS("KILL", QuestKillMobs.class), COLLECT_ITEMS("COLLECT", QuestCollectItems.class),
	MINE_BLOCKS("MINE", QuestBreakBlocks.class);

	private String key;
	private Class<? extends InterfaceQuest> clazz;

	private EnumQuestType(String key, Class<? extends InterfaceQuest> clazz) {
		this.key = key;
		this.clazz = clazz;
	}

	public String getKey() {
		return key;
	}

	public InterfaceQuest getQuest() {
		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			return null;
		}
	}

	public static EnumQuestType fromString(String string) {
		for (EnumQuestType type : values()) {
			if (type.getKey().equals(string)) {
				return type;
			}
		}
		return null;
	}

	public static EnumQuestType fromClass(Class<? extends InterfaceQuest> clazz) {
		for (EnumQuestType type : values()) {
			if (type.clazz == clazz)
				return type;
		}
		return null;
	}
}
