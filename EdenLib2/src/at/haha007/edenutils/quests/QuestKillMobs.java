package at.haha007.edenutils.quests;

import java.util.Hashtable;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;

public class QuestKillMobs implements InterfaceQuest {

	private Hashtable<EntityType, Integer> entities = new Hashtable<>();
	private double reward;

	@Override
	public void addEntry(String key, int amount) {
		EntityType type = EntityType.valueOf(key.toUpperCase());
		if (type == null) {
			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.RED + "[EdenLib] [Quest] Entity " + key + " does not exist!");
		} else {
			entities.put(type, amount);
		}
	}

	@Override
	public InterfaceQuest copy() {
		QuestKillMobs quest = new QuestKillMobs();
		quest.entities.putAll(entities);
		quest.reward = reward;
		return quest;
	}

	@Override
	public Hashtable<String, Integer> getEntries() {
		Hashtable<String, Integer> entries = new Hashtable<>();
		for (Entry<EntityType, Integer> entry : entities.entrySet()) {
			entries.put(entry.getKey().toString(), entry.getValue());
		}
		return entries;
	}

	public Hashtable<EntityType, Integer> getEntities() {
		return entities;
	}

	@Override
	public double getReward() {
		return reward;
	}

	@Override
	public void setReward(double reward) {
		this.reward = reward;
	}
}
