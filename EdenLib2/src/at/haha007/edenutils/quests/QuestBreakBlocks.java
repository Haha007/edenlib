package at.haha007.edenutils.quests;

import java.util.Hashtable;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;

public class QuestBreakBlocks implements InterfaceQuest {
	private Hashtable<Material, Integer> blocks = new Hashtable<Material, Integer>();
	private double reward;

	@Override
	public void addEntry(String type, int amount) {
		Material material = Material.getMaterial(type.toUpperCase());

		if (material == null) {
			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.RED + "[EdenLib] [Quest] Material " + type + " does not exist!");
			return;
		}

		if (!material.isBlock()) {
			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.RED + "[EdenLib] [Quest] Material " + type + " is not a Block!");
			return;
		}

		blocks.put(material, amount);

	}

	@Override
	public InterfaceQuest copy() {
		QuestBreakBlocks quest = new QuestBreakBlocks();
		quest.blocks.putAll(blocks);
		quest.reward = reward;
		return quest;
	}

	@Override
	public Hashtable<String, Integer> getEntries() {
		Hashtable<String, Integer> entries = new Hashtable<>();
		for (Entry<Material, Integer> entry : blocks.entrySet()) {
			entries.put(entry.getKey().toString(), entry.getValue());
		}
		return entries;
	}

	public Hashtable<Material, Integer> getBlocks() {
		return blocks;
	}

	@Override
	public double getReward() {
		return reward;
	}

	@Override
	public void setReward(double reward) {
		this.reward = reward;
	}
}
