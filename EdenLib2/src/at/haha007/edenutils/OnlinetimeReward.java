package at.haha007.edenutils;

import java.math.BigDecimal;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.earth2me.essentials.User;

import at.haha007.edenlib.EdenLib;
import net.ess3.api.MaxMoneyException;

public class OnlinetimeReward {

	private int taskID;
	private boolean isRunning;

	public OnlinetimeReward(int time, double cash) {
		taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(EdenLib.instance, new Runnable() {
			@Override
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers()) {
					User user = EdenLib.essentials.getUser(player);
					if (!user.isAfk()) {
						try {
							user.giveMoney(BigDecimal.valueOf(cash));
						} catch (MaxMoneyException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}, 0, time);
		isRunning = true;
	}

	public void unregister() {
		if (!isRunning)
			return;
		Bukkit.getScheduler().cancelTask(taskID);
		isRunning = false;
	}
}
