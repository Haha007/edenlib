package at.haha007.edenutils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.dynmap.DynmapWebChatEvent;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.Utils;

public class ChatFilter implements CommandExecutor, Listener, TabCompleter {

	private File file;
	private HashSet<String> filter = new HashSet<>();
	private final float persistance = 0.7f;
	private final int minLength = 7;

	public ChatFilter(EdenLib edenlib) {
		file = new File(edenlib.getDataFolder(), "chatfilter.txt");
		if (!file.exists())
			return;

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			String line;
			while ((line = reader.readLine()) != null) {
				filter.add(line.toLowerCase());
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void saveWords() {

		try {
			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file));
			for (String string : filter) {
				writer.write(string + "\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("eden.edenutils.chatfilter.bypass")) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			return true;
		}

		if (args.length == 0) {
			sender.sendMessage(ChatColor.RED + "/" + label + " add <wort>");
			sender.sendMessage(ChatColor.RED + "/" + label + " remove <wort>");
			sender.sendMessage(ChatColor.RED + "/" + label + " list");
			return true;
		}

		switch (args[0].toLowerCase()) {
		case "add":
			if (args.length != 2) {
				sender.sendMessage(ChatColor.RED + "/" + label + " add <wort>");
				return true;
			}
			sender.sendMessage(
					Messages.getMessages().getMessage("chatfilter.add").replaceAll("\\{MESSAGE\\}", args[1]));
			filter.add(args[1].toLowerCase());
			saveWords();
			break;
		case "remove":
			if (args.length != 2) {
				sender.sendMessage(ChatColor.RED + "/" + label + " remove <wort>");
				return true;
			}
			filter.remove(args[1].toLowerCase());
			sender.sendMessage(
					Messages.getMessages().getMessage("chatfilter.remove").replaceAll("\\{MESSAGE\\}", args[1]));
			saveWords();
			break;
		case "list":
			if (args.length != 1) {
				sender.sendMessage(ChatColor.RED + "/" + label + " list");
				return true;
			}
			String message = "";
			for (String string : filter) {
				message += string + ", ";
			}
			List<String> messages = Utils.toLines(message, 200);
			sender.sendMessage(Messages.getMessages().getMessage("chatfilter.list"));
			sender.sendMessage(ChatColor.GOLD + "=============================");
			for (String string : messages) {
				sender.sendMessage(ChatColor.GOLD + string);
			}
			sender.sendMessage(ChatColor.GOLD + "=============================");
			saveWords();
			break;
		default:
			sender.sendMessage(ChatColor.RED + "/" + label + " add <wort>");
			sender.sendMessage(ChatColor.RED + "/" + label + " remove <wort>");
			break;
		}

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		ArrayList<String> list = new ArrayList<>();
		if (!sender.hasPermission("eden.edenutils.chatfilter.bypass")) {
			return list;
		}
		if (args.length == 1) {
			if ("add".contains(args[0].toLowerCase()))
				list.add("add");
			if ("remove".contains(args[0].toLowerCase()))
				list.add("remove");
			if ("list".contains(args[0].toLowerCase()))
				list.add("list");
		}
		return list;
	}

	@EventHandler
	void onChat(AsyncPlayerChatEvent event) {
		if (event.getPlayer().hasPermission("eden.edenlib.chatfilter.bypass")) {
			return;
		}
		if (!event.isAsynchronous()) {
			return;
		}

		String message = event.getMessage().toLowerCase();
		for (String string : filter) {
			if (message.contains(string)) {
				Bukkit.broadcast(Messages.getMessages().getMessage("chatfilter.blocked.broadcast")
						.replaceAll("\\{MESSAGE\\}", event.getMessage()).replaceAll("\\{PLAYER\\}",
								event.getPlayer().getDisplayName()),
						"eden.edenutils.chatfilter.message");
				event.getPlayer().sendMessage(Messages.getMessages().getMessage("chatfilter.blocked.word"));
				event.setCancelled(true);
				return;
			}
		}

		if (checkCaps(event.getMessage())) {
			Bukkit.broadcast(Messages.getMessages().getMessage("chatfilter.blocked.broadcast")
					.replaceAll("\\{MESSAGE\\}", event.getMessage()).replaceAll("\\{PLAYER\\}",
							event.getPlayer().getDisplayName()),
					"eden.edenutils.chatfilter.message");
			event.getPlayer().sendMessage(Messages.getMessages().getMessage("chatfilter.blocked.caps"));
			event.setCancelled(true);
		}
	}

	private boolean checkCaps(String message) {
		int length = message.length();
		if (length < minLength)
			return false;

		int caps = 0;
		for (char c : message.toCharArray()) {
			if (Character.isUpperCase(c)) {
				caps++;
			}
		}

		float persistance = (float) caps / length;
		if (persistance > this.persistance)
			return true;
		return false;
	}

	@EventHandler
	void onChat(DynmapWebChatEvent event) {
		String message = event.getMessage().toLowerCase();
		if (EdenLib.essentials.getUser(event.getName()).isMuted()) {
			event.setCancelled(true);
		}
		for (String string : filter) {
			if (message.contains(string)) {
				Bukkit.broadcast(Messages.getMessages().getMessage("chatfilter.blocked.broadcast")
						.replaceAll("\\{MESSAGE\\}", event.getMessage()).replaceAll("\\{PLAYER\\}", event.getName()),
						"eden.edenutils.chatfilter.message");
				event.setCancelled(true);
				return;
			}
		}

		if (checkCaps(event.getMessage())) {
			event.setCancelled(true);
		}
	}

}
