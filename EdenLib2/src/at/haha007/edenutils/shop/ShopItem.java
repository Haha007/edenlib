package at.haha007.edenutils.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import at.haha007.edenlib.utils.Utils;

public class ShopItem {
	private String player;
	private ItemStack item;
	private double price;
	private long timeStamp;
	private UUID uuid;

	public ShopItem(ConfigurationSection cfg, UUID _uuid) {
		player = cfg.getString("player");
		item = cfg.getItemStack("item");
		price = cfg.getDouble("price");
		timeStamp = cfg.getLong("timeStamp");
		uuid = _uuid;
	}

	public ShopItem(String _player, ItemStack _item, double _price, long _timeStamp) {
		player = _player;
		item = _item;
		price = _price;
		timeStamp = _timeStamp;
		uuid = UUID.randomUUID();
	}

	public ConfigurationSection save(ConfigurationSection cfg) {
		cfg.set("player", player);
		cfg.set("item", item);
		cfg.set("price", price);
		cfg.set("timeStamp", timeStamp);
		return cfg;
	}

	public ItemStack getDisplayItem() {
		ItemStack display = new ItemStack(item);
		ItemMeta meta = display.getItemMeta();
		List<String> lore = item.getLore();
		if (lore == null)
			lore = new ArrayList<>();
		lore.add(0, ChatColor.GOLD + "Preis pro Stück: " + ChatColor.AQUA + price);
		lore.add(1, ChatColor.GOLD + "Verkäufer: " + ChatColor.AQUA + player);
		lore.add(2, ChatColor.GOLD + "eingestellt am: " + ChatColor.AQUA + Utils.formatDate(timeStamp));
		meta.setLore(lore);
		display.setItemMeta(meta);

		return Utils.setItemUUID(display, uuid);
	}

	public ItemStack getItem() {
		return item;
	}

	public void setItem(ItemStack item) {
		this.item = item;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public UUID getUuid() {
		return uuid;
	}
}
