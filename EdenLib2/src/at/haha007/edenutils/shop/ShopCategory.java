package at.haha007.edenutils.shop;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ShopCategory {
	private List<Material> materials;
	private String name;
	private ItemStack item;

	public ShopCategory(Material material, String name, List<Material> entries) {
		this.name = name;
		item = new ItemStack(material);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);

		materials = entries;
	}

	public void addMaterial(Material material) {
		materials.add(material);
	}

	public boolean containsMaterial(Material material) {
		return materials.contains(material);
	}

	public ItemStack getItem() {
		return item;
	}

	public String getName() {
		return name;
	}
}
