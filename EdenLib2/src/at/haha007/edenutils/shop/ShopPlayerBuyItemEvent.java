package at.haha007.edenutils.shop;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class ShopPlayerBuyItemEvent extends PlayerEvent implements Cancellable {

	private static final HandlerList HANDLERS_LIST = new HandlerList();
	private boolean isCanceled = false;
	private final ShopItem item;

	public ShopPlayerBuyItemEvent(Player player, ShopItem item) {
		super(player);
		this.item = item;
	}

	@Override
	public HandlerList getHandlers() {
		return HANDLERS_LIST;
	}

	public static HandlerList getHandlerList() {
		return HANDLERS_LIST;
	}

	@Override
	public boolean isCancelled() {
		return isCanceled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		isCanceled = cancel;
	}

	public ShopItem getItem() {
		return item;
	}

}
