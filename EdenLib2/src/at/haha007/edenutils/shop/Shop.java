package at.haha007.edenutils.shop;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.earth2me.essentials.api.Economy;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Utils;

public class Shop {

	private static Shop instance;

	public static Shop getShop() {
		if (instance == null)
			instance = new Shop(new File(EdenLib.instance.getDataFolder(), "shop.yml"),
					new File(EdenLib.instance.getDataFolder(), "shopConfig.yml"),
					ChatColor.GREEN + "Kategorien Aktionshaus", ChatColor.GREEN + "Auktionshaus");
		return instance;
	}

	private enum EnumAuswahl {
		ALL, OWN, EXPIRED, OTHERS;
	}

	private enum EnumTool {
		ADMIN_TOOL, EXPIRE, NO_PERM, NONE;
	}

	private List<ShopCategory> categories = new ArrayList<>();
	private ArrayList<ShopItem> contents = new ArrayList<>();
	private File file;
	private String mainTitle, categoryTitle;
	private double fee;
	private long expiring;

	private Shop(File save, File config, String mainTitle, String categoryTitle) {
		file = save;
		this.mainTitle = mainTitle;
		this.categoryTitle = categoryTitle;
		long delete;

		// create file if not exists
		if (!save.exists()) {
			try {
				save.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// create config if not exists
		if (!config.exists()) {
			try {
				config.createNewFile();
				YamlConfiguration
						.loadConfiguration(new InputStreamReader(EdenLib.instance.getResource("assets/shopConfig.yml")))
						.save(config);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// load configuration
		{
			YamlConfiguration cfg = YamlConfiguration.loadConfiguration(config);
			fee = cfg.getDouble("fee");
			if (fee < 0)
				fee = 0;
			if (fee > 1)
				fee = 1;
			try {
				expiring = Utils.parseDateDiff(cfg.getString("expiring"), true) - System.currentTimeMillis();
			} catch (Exception e) {
				expiring = 100000l;
				e.printStackTrace();
			}
			try {
				delete = Utils.parseDateDiff(cfg.getString("delete"), true) - System.currentTimeMillis();
			} catch (Exception e) {
				delete = 100000l;
				e.printStackTrace();
			}
			ConfigurationSection categorySection = cfg.getConfigurationSection("categories");
			for (String key : categorySection.getKeys(false)) {
				List<String> materialKeys = categorySection.getStringList(key);
				List<Material> materials = new ArrayList<>();
				for (String string : materialKeys) {
					materials.add(Material.matchMaterial(string));
				}
				categories.add(new ShopCategory(materials.get(0), ChatColor.GREEN + key, materials));
			}
			int autosave = cfg.getInt("autosave", 100);
			Bukkit.getScheduler().scheduleSyncRepeatingTask(EdenLib.instance, new Runnable() {
				@Override
				public void run() {
					saveAsynchronous();
				}
			}, autosave, autosave);
		}

		{
			YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			for (String key : cfg.getKeys(false)) {
				ShopItem item = new ShopItem(cfg.getConfigurationSection(key), UUID.fromString(key));
				if (System.currentTimeMillis() - item.getTimeStamp() < delete)
					contents.add(item);
			}
		}
	}

	public void openMainInventory(Player player) {
		if (!player.hasPermission("eden.edenutils.shop.open"))
			return;

		int size = ((categories.size() + 9) / 9) * 9;
		Inventory inv = Bukkit.createInventory(null, size, getMainTitle());

		for (int i = 0; i < categories.size(); i++) {
			inv.setItem(size + i - categories.size(), categories.get(i).getItem());
		}

		ItemStack item = new ItemStack(Material.CHEST);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Alle Items");
		item.setItemMeta(meta);
		inv.setItem(0, item);

		Bukkit.getScheduler().runTask(EdenLib.instance, new Runnable() {
			@Override
			public void run() {
				player.openInventory(inv);
			}
		});
	}

	public void clickMainInventory(InventoryClickEvent event) {

		Player player = (Player) event.getWhoClicked();
		if (!player.hasPermission("eden.edenutils.shop.interact"))
			return;

		if (event.getView().getTopInventory() != event.getClickedInventory())
			return;

		ItemStack item = event.getCurrentItem();
		if (item == null)
			return;

		if (item.getType() == Material.AIR)
			return;

		ItemMeta meta = item.getItemMeta();
		String name = meta.getDisplayName();

		openCategoryInventory((Player) event.getWhoClicked(), name, 1, EnumAuswahl.ALL, 1, null);
	}

	public void openCategoryInventory(Player player, String category_name, int page, EnumAuswahl auswahl, int count,
			EnumTool tool) {
		if (!player.hasPermission("eden.edenutils.shop.open"))
			return;
		if (page < 1)
			page = 1;
		if (count > 64)
			count = 1;
		if (tool == null)
			tool = player.hasPermission("eden.edenutils.shop.edit") ? EnumTool.NONE : EnumTool.NO_PERM;

		Inventory inv = Bukkit.createInventory(null, 54, getCategoryTitle());

		ShopCategory category = getShopCategory(category_name);

		// Info
		inv.setItem(0,
				Utils.getItem(Material.BOOK, ChatColor.GREEN + "- Info -",
						ChatColor.AQUA + "Klicke auf ein Item um es zu kaufen.",
						ChatColor.AQUA + "Klicke auf deine eigenen Items um sie zurückzunehmen.",
						ChatColor.AQUA + "Benutze das Papier um die Seiten zu wechseln.",
						ChatColor.AQUA + "Mache \"/shop info\" für weitere Informationen.",
						ChatColor.RED + "Mit einem Rechtsklick kannst du Items verlüngern / neu einstellen."));
		if (category != null)
			inv.setItem(0, Utils.addNbtString(inv.getItem(0), "shopCategory", category.getName()));

		// Anzahl
		ItemStack anzahl = Utils.getItem(Material.WHITE_WOOL, ChatColor.GREEN + "Anzahl: " + ChatColor.GOLD + count,
				ChatColor.AQUA + "Anzahl der Items die auf einmal gekauft werden.",
				ChatColor.AQUA + "Klicken um es zu ändern.");
		anzahl.setAmount(count);
		inv.setItem(2, anzahl);

		// Auswahl
		setAuswahl(inv, auswahl);

		// umblättern
		setPage(inv, page);

		// back
		inv.setItem(9, Utils.getItem(Material.BARRIER, ChatColor.RED + "Zurück"));

		// content
		ShopItem[] items = getItems(category, auswahl, page, tool, player.getName());
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				break;
			inv.setItem(i + 18, items[i].getDisplayItem());
		}

		// admin items
		if (tool != EnumTool.NO_PERM) {
			inv.setItem(1,
					Utils.getItem(Material.BARRIER, ChatColor.RED + "Admin Tool",
							ChatColor.AQUA + "Zeigt absolut alle Angebote.",
							ChatColor.AQUA + "Items werden beim Klick entfernt."));
			inv.setItem(10,
					Utils.getItem(Material.REDSTONE, ChatColor.RED + "Items Auslaufen lassen",
							ChatColor.AQUA + "Aktiviere dies um angeklickte Items",
							ChatColor.AQUA + "als abgelaufen zu markieren."));

			if (tool == EnumTool.ADMIN_TOOL)
				inv.setItem(1, Utils.addGlow(inv.getItem(1)));

			if (tool == EnumTool.EXPIRE)
				inv.setItem(10, Utils.addGlow(inv.getItem(10)));
		}
		Bukkit.getScheduler().runTask(EdenLib.instance, new Runnable() {
			@Override
			public void run() {
				player.openInventory(inv);
			}
		});
	}

	public void clickCategoryInventory(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		if (!player.hasPermission("eden.edenutils.shop.interact"))
			return;

		// get inventory settings
		InventoryView inv = event.getView();
		if (event.getClickedInventory() != inv.getTopInventory())
			return;

		ShopCategory category = getShopCategory(Utils.getNbtString(inv.getItem(0), "shopCategory"));
		int count = Integer.parseInt(inv.getItem(2).getItemMeta().getDisplayName()
				.replaceFirst(ChatColor.GREEN + "Anzahl: " + ChatColor.GOLD, ""));
		EnumAuswahl auswahl = getAuswahl(inv.getTopInventory());
		int page = Integer.parseInt(inv.getItem(8).getItemMeta().getLore().get(0)
				.replaceFirst(ChatColor.GREEN + "Aktuell auf Seite: " + ChatColor.DARK_RED, ""));
		EnumTool tool = getTool(inv.getTopInventory());
		String categoryName = category == null ? null : category.getName();

		// change settings ?
		if (event.getSlot() == 7) {
			openCategoryInventory(player, categoryName, page - 1, auswahl, count, tool);
			return;
		}
		if (event.getSlot() == 8) {
			openCategoryInventory(player, categoryName, page + 1, auswahl, count, tool);
			return;
		}
		if (event.getSlot() == 3) {
			openCategoryInventory(player, categoryName, page, EnumAuswahl.ALL, count, tool);
			return;
		}
		if (event.getSlot() == 4) {
			openCategoryInventory(player, categoryName, page, EnumAuswahl.OWN, count, tool);
			return;
		}
		if (event.getSlot() == 5) {
			openCategoryInventory(player, categoryName, page, EnumAuswahl.EXPIRED, count, tool);
			return;
		}
		if (event.getSlot() == 6) {
			openCategoryInventory(player, categoryName, page, EnumAuswahl.OTHERS, count, tool);
			return;
		}
		if (event.getSlot() == 9) {
			openMainInventory(player);
			return;
		}
		if (event.getSlot() == 2) {
			openCategoryInventory(player, categoryName, page, auswahl, 2 * count, tool);
			return;
		}

		// buy item ?
		if (event.getSlot() >= 18 && event.getSlot() < 54) {
			UUID uuid = Utils.getItemUUID(event.getCurrentItem());
			buyItem(uuid, inv, event.getClick());
			return;
		}

		// admin settings ?
		if (!player.hasPermission("eden.edenutils.shop.edit"))
			return;

		if (event.getSlot() == 1) {
			openCategoryInventory(player, categoryName, page, auswahl, count,
					tool == EnumTool.ADMIN_TOOL ? EnumTool.NONE : EnumTool.ADMIN_TOOL);
			return;
		}

		if (event.getSlot() == 10) {
			openCategoryInventory(player, categoryName, page, auswahl, count,
					tool == EnumTool.EXPIRE ? EnumTool.NONE : EnumTool.EXPIRE);
			return;
		}
	}

	private EnumTool getTool(Inventory inv) {
		ItemStack item1 = inv.getItem(1);
		ItemStack item2 = inv.getItem(10);
		if (item1 == null || item2 == null)
			return EnumTool.NO_PERM;
		if (item1.getItemMeta().hasEnchants())
			return EnumTool.ADMIN_TOOL;
		if (item2.getItemMeta().hasEnchants())
			return EnumTool.EXPIRE;
		return EnumTool.NONE;
	}

	private ShopItem[] getItems(ShopCategory category, EnumAuswahl auswahl, int page, EnumTool tool, String player) {
		ShopItem[] items = new ShopItem[36];
		ArrayList<ShopItem> filteredItems = new ArrayList<>();
		for (ShopItem shopItem : contents) {
			if (category == null || category.containsMaterial(shopItem.getItem().getType())) {
				switch (auswahl) {
				case ALL:
					if (!isExpired(shopItem) || tool == EnumTool.ADMIN_TOOL)
						filteredItems.add(shopItem);
					break;
				case EXPIRED:
					if (player.equalsIgnoreCase(shopItem.getPlayer()) && isExpired(shopItem))
						filteredItems.add(shopItem);
					break;
				case OTHERS:
					if (!player.equalsIgnoreCase(shopItem.getPlayer())
							&& (!isExpired(shopItem) || tool == EnumTool.ADMIN_TOOL))
						filteredItems.add(shopItem);
					break;
				case OWN:
					if (player.equalsIgnoreCase(shopItem.getPlayer())
							&& (!isExpired(shopItem) || tool == EnumTool.ADMIN_TOOL))
						filteredItems.add(shopItem);
					break;
				default:
					break;
				}
			}
		}

		int startIndex = (page - 1) * 36;
		for (int i = 0; i < 36; i++) {
			try {
				ShopItem item = filteredItems.get(i + startIndex);
				items[i] = item;
			} catch (IndexOutOfBoundsException e) {
				break;
			}
		}
		return items;
	}

	private boolean isExpired(ShopItem shopItem) {
		return System.currentTimeMillis() - shopItem.getTimeStamp() > expiring;
	}

	private EnumAuswahl getAuswahl(Inventory inv) {
		if (inv.getItem(3).getItemMeta().hasEnchants())
			return EnumAuswahl.ALL;
		if (inv.getItem(4).getItemMeta().hasEnchants())
			return EnumAuswahl.OWN;
		if (inv.getItem(5).getItemMeta().hasEnchants())
			return EnumAuswahl.EXPIRED;
		if (inv.getItem(6).getItemMeta().hasEnchants())
			return EnumAuswahl.OTHERS;

		return EnumAuswahl.ALL;
	}

	private void setAuswahl(Inventory inv, EnumAuswahl auswahl) {
		inv.setItem(3, Utils.getItem(Material.HOPPER, ChatColor.GREEN + "Alles",
				ChatColor.AQUA + "Zeige alle verfügbaren Angebote."));

		inv.setItem(4, Utils.getItem(Material.HOPPER, ChatColor.GREEN + "Eigene",
				ChatColor.AQUA + "Zeige nur deine eigenen Angebote."));

		inv.setItem(5, Utils.getItem(Material.HOPPER, ChatColor.GREEN + "Ausgelaufen",
				ChatColor.AQUA + "Zeige nur deine ausgelaufenen Angebote."));

		inv.setItem(6, Utils.getItem(Material.HOPPER, ChatColor.GREEN + "Andere",
				ChatColor.AQUA + "Zeige nur Angebote von anderen Spielern."));

		switch (auswahl) {
		case ALL:
			inv.setItem(3, Utils.addGlow(inv.getItem(3)));
			break;
		case OWN:
			inv.setItem(4, Utils.addGlow(inv.getItem(4)));
			break;
		case EXPIRED:
			inv.setItem(5, Utils.addGlow(inv.getItem(5)));
			break;
		case OTHERS:
			inv.setItem(6, Utils.addGlow(inv.getItem(6)));
			break;
		default:
			break;
		}
	}

	private void setPage(Inventory inv, int page) {
		inv.setItem(7, Utils.getItem(Material.PAPER, ChatColor.GREEN + "Vorherige Seite",
				ChatColor.GREEN + "Aktuell auf Seite: " + ChatColor.DARK_RED + page));
		inv.setItem(8, Utils.getItem(Material.PAPER, ChatColor.GREEN + "Nächste Seite",
				ChatColor.GREEN + "Aktuell auf Seite: " + ChatColor.DARK_RED + page));
	}

	public ShopCategory getShopCategory(String name) {
		for (ShopCategory shopCategory : categories) {
			if (shopCategory.getName().equalsIgnoreCase(name))
				return shopCategory;
		}
		return null;
	}

	private void buyItem(UUID itemUUID, InventoryView inv, ClickType click) {
		Player player = (Player) inv.getPlayer();
		ShopItem item = getItem(itemUUID);
		if (item == null)
			return;
		EnumTool tool = getTool(inv.getTopInventory());

		int count = Integer.parseInt(inv.getTopInventory().getItem(2).getItemMeta().getDisplayName()
				.replaceFirst(ChatColor.GREEN + "Anzahl: " + ChatColor.GOLD, ""));

		switch (tool) {
		case ADMIN_TOOL:
			contents.remove(item);
			break;
		case EXPIRE:
			item.setTimeStamp(System.currentTimeMillis() - 650000000);
			break;
		case NONE:
		case NO_PERM:
			if (click == ClickType.LEFT) {

				if (item.getPlayer().equalsIgnoreCase(player.getName())) {
					Utils.giveItem(player, item.getItem());
					contents.remove(item);
					updateOpenInventories();
					return;
				}

				try {
					if (!Economy.hasEnough(player.getName(), BigDecimal.valueOf(item.getPrice()))) {
						player.sendMessage(ChatColor.RED + "Du kannst dir diesen Gegenstand nicht leisten.");
						return;
					}
					ShopPlayerBuyItemEvent event = new ShopPlayerBuyItemEvent(player, item);
					Bukkit.getServer().getPluginManager().callEvent(event);
					if (event.isCancelled())
						return;

					ItemStack itemStack = item.getItem();
					if (itemStack.getAmount() < count)
						count = itemStack.getAmount();
					if (itemStack.getAmount() == count)
						contents.remove(item);

					Economy.substract(player.getName(), BigDecimal.valueOf(item.getPrice() * count));
					Economy.add(item.getPlayer(), BigDecimal.valueOf(item.getPrice() * (1 - fee) * count));
					itemStack.setAmount(itemStack.getAmount() - count);
					ItemStack give = new ItemStack(item.getItem());
					give.setAmount(count);
					Utils.giveItem(player, give);
					player.sendMessage(ChatColor.GOLD + "Gegenstand für " + ChatColor.GREEN + item.getPrice() * count
							+ "$" + ChatColor.GOLD + " gekauft. Du hast noch " + ChatColor.GREEN
							+ Economy.getMoneyExact(player.getName()) + "$" + ChatColor.GOLD + " übrig.");
					updateOpenInventories();
				} catch (ArithmeticException | UserDoesNotExistException | NoLoanPermittedException e) {
					e.printStackTrace();
				}
			} else if (click == ClickType.RIGHT) {
				if (!item.getPlayer().equalsIgnoreCase(player.getName()))
					return;
				item.setTimeStamp(System.currentTimeMillis());
			}
			break;
		default:
			break;
		}
		updateOpenInventories();
	}

	public void sellItem(ShopItem item, Player player, int i) {
		if (!player.hasPermission("eden.edenutils.shop.sell")) {
			player.sendMessage(ChatColor.RED + "Du hast keine ausreichenden Rechte um Items zu verkaufen.");
			return;
		}
		ShopPlayerSellItemEvent event = new ShopPlayerSellItemEvent(player, item);
		Bukkit.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return;
		player.getInventory().setItem(i, null);
		player.sendMessage(
				ChatColor.GOLD + "Du hast das Item für " + item.getPrice() + " pro stück in den Shop gestellt.");
		contents.add(item);
	}

	private ShopItem getItem(UUID itemUUID) {
		for (ShopItem item : contents) {
			if (item.getUuid().equals(itemUUID))
				return item;
		}
		return null;
	}

	private void updateOpenInventories() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			tryUpdateOpenInventory(player);
		}
	}

	private void tryUpdateOpenInventory(Player player) {
		InventoryView view = player.getOpenInventory();
		if (view.getTopInventory() == null)
			return;
		if (!view.getTitle().equals(categoryTitle))
			return;

		ShopCategory category = getShopCategory(Utils.getNbtString(view.getItem(0), "shopCategory"));
		int count = Integer.parseInt(view.getItem(2).getItemMeta().getDisplayName()
				.replaceFirst(ChatColor.GREEN + "Anzahl: " + ChatColor.GOLD, ""));
		EnumAuswahl auswahl = getAuswahl(view.getTopInventory());
		int page = Integer.parseInt(view.getItem(8).getItemMeta().getLore().get(0)
				.replaceFirst(ChatColor.GREEN + "Aktuell auf Seite: " + ChatColor.DARK_RED, ""));
		EnumTool tool = getTool(view.getTopInventory());

		openCategoryInventory(player, category == null ? null : category.getName(), page, auswahl, count, tool);
	}

	public void save() {
		YamlConfiguration cfg = new YamlConfiguration();
		for (ShopItem shopItem : contents) {
			if (shopItem != null)
				shopItem.save(cfg.createSection(shopItem.getUuid().toString()));
		}
		try {
			if (!file.exists())
				file.createNewFile();
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveAsynchronous() {
		Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, new Runnable() {
			@Override
			public void run() {
				save();
			}
		});
	}

	public List<ShopCategory> getCategories() {
		return categories;
	}

	public String getCategoryTitle() {
		return categoryTitle;
	}

	public ArrayList<ShopItem> getContents() {
		return contents;
	}

	public File getFile() {
		return file;
	}

	public String getMainTitle() {
		return mainTitle;
	}

	public long getExpiring() {
		return expiring;
	}
}
