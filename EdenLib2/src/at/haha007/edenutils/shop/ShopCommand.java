package at.haha007.edenutils.shop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.Utils;

public class ShopCommand implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player))
			return false;

		if (args.length == 0) {
			Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, new Runnable() {
				@Override
				public void run() {
					Shop.getShop().openMainInventory((Player) sender);
				}
			});
			return true;
		}

		if (args.length == 1 && args[0].equalsIgnoreCase("info")) {
			info((Player) sender);
			return true;
		}
		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("sell")) {
				ItemStack item = ((Player) sender).getInventory().getItemInMainHand();
				if (item == null)
					return true;
				if (item.getType() == Material.AIR)
					return true;
				double price = -1;
				try {
					price = Double.parseDouble(args[1]);
				} catch (NumberFormatException e) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.general.notdouble")
							.replaceAll("\\{NUMBER\\}", args[1]));
					return true;
				}
				if (price < 0) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.shop.negativeprice"));
					return true;
				}
				Shop.getShop().sellItem(new ShopItem(sender.getName(), item, price, System.currentTimeMillis()),
						(Player) sender, ((Player) sender).getInventory().getHeldItemSlot());
				return true;
			}
			if (args[0].equalsIgnoreCase("sellall")) {
				Player player = (Player) sender;
				PlayerInventory inv = player.getInventory();
				ItemStack item = inv.getItemInMainHand();
				if (item == null)
					return true;
				if (item.getType() == Material.AIR)
					return true;

				double price = -1;
				try {
					price = Double.parseDouble(args[1]);
				} catch (NumberFormatException e) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.general.notdouble")
							.replaceAll("\\{NUMBER\\}", args[1]));
					return true;
				}
				if (price < 0) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.shop.negativeprice"));
					return true;
				}

				Material material = item.getType();
				long time = System.currentTimeMillis();
				for (int i = 0; i < inv.getSize(); i++) {
					ItemStack itemStack = inv.getItem(i);
					if (itemStack != null && itemStack.getType() == material) {
						Shop.getShop().sellItem(new ShopItem(player.getName(), itemStack, price, time), player, i);
					}
				}
				return true;
			}
		}

		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		ArrayList<String> list = new ArrayList<>();
		if (args.length == 1) {
			if ("sell".startsWith(args[0].toLowerCase()))
				list.add("sell");
			if ("sellall".startsWith(args[0].toLowerCase()))
				list.add("sellall");
			if ("info".startsWith(args[0].toLowerCase()))
				list.add("info");
		}
		return list;
	}

	private void info(Player p) {
		p.sendMessage(ChatColor.YELLOW + "-" + ChatColor.GOLD + " Auktionshaus Info " + ChatColor.YELLOW + "-");
		p.sendMessage(ChatColor.YELLOW + "*" + ChatColor.AQUA
				+ "Im Auktionshaus kannst du beliebig viele Items zum Verkauf anbieten.");
		p.sendMessage(ChatColor.YELLOW + "*" + ChatColor.AQUA + "Benutze " + ChatColor.GREEN + "/shop" + ChatColor.AQUA
				+ " um das Auktionshaus zu öffnen.");
		p.sendMessage(ChatColor.YELLOW + "*" + ChatColor.AQUA + "Mit " + ChatColor.GREEN + "\"/shop sell <Preis>\" "
				+ ChatColor.AQUA + "bietest du die Items in deiner Hand zum Verkauf an. Zum Beispiel: "
				+ ChatColor.GREEN + "\"/shop sell 0.5\"");
		p.sendMessage(ChatColor.YELLOW + "*" + ChatColor.AQUA + "Mit " + ChatColor.GREEN + "\"/shop sellall <Preis>\" "
				+ ChatColor.AQUA
				+ "bietest du die Items in deinem Inventar vom selben typ des Items in deiner Hand zum Verkauf an. Zum Beispiel: "
				+ ChatColor.GREEN + "\"/shop sellall 0.5\"");
		p.sendMessage(ChatColor.YELLOW + "*" + ChatColor.AQUA + "Jedes deiner Items ist bis zu " + ChatColor.GREEN
				+ Utils.formatDateDiff(Shop.getShop().getExpiring()) + ChatColor.AQUA
				+ " im Auktionshaus verfügbar. Danach kann es nur noch von dir selbst gesehen werden. Klicke es an um es aus dem Shop rauszunehmen.");
		p.sendMessage(ChatColor.YELLOW + "*" + ChatColor.AQUA + "Klicke auf ein " + ChatColor.GREEN + "Item"
				+ ChatColor.AQUA + " um es zu kaufen. Klicke auf die " + ChatColor.GREEN + "Blätter" + ChatColor.AQUA
				+ " oben rechts um die angezeigte Seite im Shop zu ändern.");
		p.sendMessage(ChatColor.YELLOW + "*" + ChatColor.AQUA + "Mit den " + ChatColor.GREEN + "Trichtern"
				+ ChatColor.AQUA
				+ " kannst du einstellen welche Angebote du sehen willst - alle, deine eigenen, abgelaufene Items oder nur Items von anderen Spielern.");
		p.sendMessage(ChatColor.YELLOW + "*" + ChatColor.AQUA + "Klicke auf den " + ChatColor.GREEN + "Wollblock"
				+ ChatColor.AQUA + " um die Anzahl Items zu ändern welche auf einmal gekauft werden.");
		p.sendMessage(ChatColor.YELLOW + "*" + ChatColor.RED + "Achtung: " + ChatColor.AQUA
				+ "Von dem Gewinn den du erh�lst werden " + ChatColor.GREEN + "10 %" + ChatColor.AQUA
				+ " Verkaufsgeb�hr abgezogen.");

	}

}
