package at.haha007.edenutils.shop;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;

import at.haha007.edenlib.EdenLib;

public class ShopListener implements Listener {

	@EventHandler
	void onInventoryClick(InventoryClickEvent event) {
		InventoryView inv = event.getView();
		if (inv == null)
			return;
		Shop shop = Shop.getShop();
		String title = inv.getTitle();

		if (title.equals(shop.getMainTitle())) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, new Runnable() {
				@Override
				public void run() {
					shop.clickMainInventory(event);
				}
			});
			return;
		}

		if (title.equals(shop.getCategoryTitle())) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, new Runnable() {
				@Override
				public void run() {
					shop.clickCategoryInventory(event);
				}
			});
			return;
		}
	}

}
