package at.haha007.edenutils.listeners;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkPopulateEvent;

import at.haha007.edenlib.utils.Settings;

public class NoOreListener implements Listener {

	private List<String> worlds = new ArrayList<>();
	private Set<Material> filter = new HashSet<>();

	public NoOreListener() {

		filter.add(Material.COAL_ORE);
		filter.add(Material.DIAMOND_ORE);
		filter.add(Material.EMERALD_ORE);
		filter.add(Material.REDSTONE_ORE);
		filter.add(Material.GOLD_ORE);
		filter.add(Material.IRON_ORE);
		filter.add(Material.LAPIS_ORE);

		List<String> worldNames = Settings.noOreGenWorlds;
		for (String string : worldNames) {
			worlds.add(string.toLowerCase());
		}
	}

	@EventHandler
	void onChunkGenerate(ChunkPopulateEvent event) {

		if (!worlds.contains(event.getWorld().getName().toLowerCase()))
			return;

		Chunk chunk = event.getChunk();
		if (!chunk.isLoaded())
			chunk.load();

		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 256; y++) {
				for (int z = 0; z < 16; z++) {
					BlockState bs = chunk.getBlock(x, y, z).getState();
					if (filter.contains(bs.getType())) {
						bs.setType(Material.STONE);
						bs.update(true, false);
					}
				}
			}
		}
	}
}
