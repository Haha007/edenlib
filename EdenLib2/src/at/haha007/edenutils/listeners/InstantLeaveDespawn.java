package at.haha007.edenutils.listeners;

import java.util.HashSet;

import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class InstantLeaveDespawn implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	void onBlockBreak(BlockBreakEvent event) {
		Material material = event.getBlock().getType();

		if (Tag.LOGS.isTagged(material)) {
			if (event.getPlayer().hasPermission("eden.edenutils.leavedespawn.log"))
				destroyLeaves(event.getBlock());
		} else if (Tag.LEAVES.isTagged(material)) {
			if (event.getPlayer().getInventory().getItemInMainHand().getType() != Material.SHEARS
					&& event.getPlayer().hasPermission("eden.edenutils.leavedespawn.leaves"))
				destroyLeaves(event.getBlock());
		}
	}

	private void destroyLeaves(Block block) {
		HashSet<Block> leaveBlocks = new HashSet<>();
		HashSet<Block> newBlocks = getConnectedLeaveBlocks(block);
		HashSet<Block> nextBlocks = new HashSet<>();
		for (int i = 0; i < 6; i++) {
			for (Block b : newBlocks) {
				nextBlocks.addAll(getConnectedLeaveBlocks(b));
			}
			leaveBlocks.addAll(newBlocks);
			newBlocks = nextBlocks;
			nextBlocks = new HashSet<>();
		}

		for (Block b : leaveBlocks) {
			b.breakNaturally();
		}
	}

	private HashSet<Block> getConnectedLeaveBlocks(Block block) {
		HashSet<Block> blocks = new HashSet<>();

		if (Tag.LEAVES.isTagged(block.getRelative(BlockFace.UP).getType()))
			blocks.add(block.getRelative(BlockFace.UP));

		if (Tag.LEAVES.isTagged(block.getRelative(BlockFace.DOWN).getType()))
			blocks.add(block.getRelative(BlockFace.DOWN));

		if (Tag.LEAVES.isTagged(block.getRelative(BlockFace.EAST).getType()))
			blocks.add(block.getRelative(BlockFace.EAST));

		if (Tag.LEAVES.isTagged(block.getRelative(BlockFace.WEST).getType()))
			blocks.add(block.getRelative(BlockFace.WEST));

		if (Tag.LEAVES.isTagged(block.getRelative(BlockFace.NORTH).getType()))
			blocks.add(block.getRelative(BlockFace.NORTH));

		if (Tag.LEAVES.isTagged(block.getRelative(BlockFace.SOUTH).getType()))
			blocks.add(block.getRelative(BlockFace.SOUTH));

		return blocks;
	}
}
