package at.haha007.edenutils.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import at.haha007.edenlib.EdenLib;
import me.clip.placeholderapi.PlaceholderAPI;

public class NameHighlighter implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	void onChat(AsyncPlayerChatEvent event) {
		if (event.isCancelled())
			return;

		if (EdenLib.essentials.getUser(event.getPlayer()).isMuted())
			return;

		event.setCancelled(true);
		String prefix = PlaceholderAPI.setPlaceholders(event.getPlayer(),
				"%multiverse_world_alias%%player_displayname%§r ");

		for (Player pl : Bukkit.getOnlinePlayers()) {
			String message = prefix
					+ event.getMessage().replaceAll(pl.getName(), ChatColor.AQUA + pl.getName() + ChatColor.WHITE);
			pl.sendMessage(message);
		}
	}
}
