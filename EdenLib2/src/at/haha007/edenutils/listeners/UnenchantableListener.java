package at.haha007.edenutils.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.ItemStack;

import at.haha007.edenlib.utils.Utils;

public class UnenchantableListener implements Listener {

	@EventHandler
	void onAnvil(PrepareAnvilEvent event) {
		for (ItemStack item : event.getInventory()) {
			if (!Utils.isEnchantable(item)) {
				event.setResult(null);
				return;
			}
		}
	}

	@EventHandler
	void onEnchant(PrepareItemEnchantEvent event) {
		for (ItemStack item : event.getInventory()) {
			if (!Utils.isEnchantable(item)) {
				event.setCancelled(true);
				return;
			}
		}
	}
}
