package at.haha007.edenutils.highscore;

import at.haha007.edenlib.utils.Utils;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GameScoreContent {
    enum EntryFormat {
        POINTS,
        TIME
        //...
    }

    enum EntryOrder {
        NO_ORDER,
        POINTS_HIGHEST,
        POINTS_LOWEST
    }

    private String name;
    private ItemStack item;
    private UUID uuid = UUID.randomUUID();
    private EntryFormat entryFormatId = EntryFormat.POINTS;
    private EntryOrder entryOrderId = EntryOrder.NO_ORDER;
    private ArrayList<GameScoreEntry> entryList = new ArrayList<>();

    public GameScoreContent(String _name, ItemStack _item) {
        name = _name;
        item = _item;
    }

    public GameScoreContent(String _name, ItemStack _item, EntryFormat _entryFormatId, EntryOrder _entryOrderId) {
        name = _name;
        item = _item;
        entryFormatId = _entryFormatId;
        entryOrderId = _entryOrderId;
    }

    public GameScoreContent(ConfigurationSection cfg) {
        name = cfg.getString("name", "Default");
        item = cfg.getItemStack("item", new ItemStack(Material.GRASS_BLOCK));

        int entryCounter = 0;
        while (cfg.get("p" + entryCounter) != null) {
            entryList.add((GameScoreEntry) cfg.get("p" + entryCounter));
            entryCounter++;
        }
    }

    public ItemStack getDisplayItem() {
        ItemStack item = new ItemStack(this.item);
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();
        if (lore == null)
            lore = new ArrayList<>();

        lore.add(0, ChatColor.GOLD + name);

        meta.setLore(lore);
        item.setItemMeta(meta);
        return Utils.setItemUUID(item, uuid);
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String _name) {
        name = _name;
    }

    public ItemStack getItem() {
        return new ItemStack(item);
    }

    public void save(ConfigurationSection cfg) {
        cfg.set("item", item);
        cfg.set("name", name);
        for (int i = 0; i < entryList.size(); i++) {
            cfg.set("p" + i, entryList.get(i));
        }
    }

    public void updateContent(GameScoreContent content){
        item = content.item;
        entryFormatId = content.entryFormatId;
        entryOrderId = content.entryOrderId;
    }

    public void addEntry(GameScoreEntry entry) {
        if(entry == null){
            return;
        }

        int orderFactor = -1;
        switch (entryOrderId){
            case NO_ORDER:
                entryList.add(entry);
                return;
            case POINTS_HIGHEST:
                orderFactor = 1;
                break;
            case POINTS_LOWEST:
                //Nothing
        }

        boolean positionFound = false;
        int index = -1;
        for(int i = 0; i < entryList.size(); i++){
            if(positionFound){
                if(entryList.get(i).getPlayerName().equals(entry.getPlayerName())){
                    entryList.remove(i);
                    return;
                }
            } else {
                if(entryList.get(i).getPlayerName().equals(entry.getPlayerName())){
                    entryList.get(i).updateEntry(entry);
                    return;
                }

                if(entry.compareTo(entryList.get(i)) * orderFactor > 0){
                    positionFound = true;
                }
                index ++;
            }
        }

        if (index == -1) {
            entryList.add(entry);
        } else {
            entryList.add(index, entry);
        }
    }

    public boolean removeEntry(String name) {
        for (GameScoreEntry entry : entryList) {
            if (entry.getPlayerName().equals(name)) {
                return removeEntry(entry);
            }
        }
        return false;
    }

    public boolean removeEntry(GameScoreEntry entry) {
        return entryList.remove(entry);
    }

    public void printEntries(Player player, String categoryName) {
        printEntries(player, categoryName, 0, 9);
    }

    public void printEntries(Player player, String categoryName, int from, int to) {
        if (from < 0) {
            from = 0;
        }

        if (to > entryList.size()) {
            to = entryList.size();
        }

        StringBuilder sb = new StringBuilder();
        String head = ChatColor.RED + "xxx " + ChatColor.GREEN + categoryName + " - " + name + ChatColor.RED + " xxx\n";
        String ownEntry = "Deine Platzierung: ";

        //Option for larger playernumber (NOT implemented yet)
        //EdenPlayerManager manager = new EdenPlayerManager();
        //GameScoreEntry entry = manager.getEdenPlayer(player).getScoreEntry(name);
        //head += GameScoreEntry.formatEntryByFormatId(entry, entryFormatId);

        if (entryList.isEmpty()) {
            sb.append(ChatColor.RED);
            sb.append("Keine Einträge gefunden!\n");
        } else {
            for (int pos = from; pos < to; pos++) {
                sb.append(ChatColor.GREEN);
                sb.append(pos + 1);
                sb.append(": ");
                entryList.get(pos).appendTo(sb);
                sb.append("\n");

                if (player.getName().equals(entryList.get(pos).getPlayerName())) {
                    ownEntry += (pos + 1) + GameScoreEntry.formatEntryByFormatId(entryList.get(pos), entryFormatId);
                }
            }
        }
        player.sendMessage(head + ownEntry + sb.toString());

        if (entryList.size() > to) {
            TextComponent msg = new TextComponent("\t\t" + ChatColor.GOLD + ">>>" + ChatColor.WHITE + " Nächste Seite " + ChatColor.GOLD + ">>>");
            msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/edenhighscore printlist "
                    + categoryName + " " + name + " " + (to + 1) + " " + (to + 10)));
            player.sendMessage(msg);
        }
    }
}