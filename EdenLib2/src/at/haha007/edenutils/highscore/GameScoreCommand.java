package at.haha007.edenutils.highscore;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class GameScoreCommand implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Dieser Command kann nur von Spielern ausgeführt werden.");
            return true;
        }
        Player player = (Player) sender;
        if (args.length == 0 || !player.hasPermission("eden.edenutils.highscore.edit")) {
            GameScoreOverview.getInstance().openOverviewInventory(player);
            return true;
        }
        ItemStack item = new ItemStack(player.getInventory().getItemInMainHand());
        item.setAmount(1);

        switch (args[0].toLowerCase()) {
            case "reload":
                GameScoreOverview.getInstance().reload();
                player.sendMessage(ChatColor.GREEN + "Die Highscores wurden neu geladen.");
                return true;

            case "addcategory":
                return handleAddCategory(player, item, args);

            case "movecategory":
                return handleMoveCategory(player, args);

            case "removecategory":
                return handleRemoveCategory(player, args);

            case "renamecategory":
                return handleRenameCategory(player, args);

            case "listcategories":
                player.sendMessage(ChatColor.DARK_GREEN + "Highscore Kategorien:");
                for (GameScoreCategory category : GameScoreOverview.getInstance().getCategories()) {
                    player.sendMessage(ChatColor.GREEN + category.getName());
                }
                return true;

            case "addcontent":
                return handleAddContent(player, item, args);

            case "movecontent":
                return handleMoveContent(player, args);

            case "removecontent":
                return handleRemoveContent(player, args);

            case "renamecontent":
                return handleRenameContent(player, args);

            case "removeentry":
                return handleRemoveEntry(player, args);

            case "removeplayerentries":
                return removeEntriesFromPlayer(player, args);

            default:
                sendInfo(player);
                return true;
        }
    }

    private void sendInfo(Player player) {
        player.sendMessage("");
        player.sendMessage(ChatColor.GOLD + "Commands ohne Item in der Hand:");
        player.sendMessage(ChatColor.GOLD + "/highscore reload");
        player.sendMessage(ChatColor.GOLD + "/highscore removecategory <name>");
        player.sendMessage(ChatColor.GOLD + "/highscore renamecategory <oldName> <newName>");
        player.sendMessage(ChatColor.GOLD + "/highscore movecategory <name> <index>");
        player.sendMessage(ChatColor.GOLD + "/highscore listcategories");
        player.sendMessage(ChatColor.GOLD + "/highscore removeitem <name> <category>");
        player.sendMessage(ChatColor.GOLD + "/highscore removeentry <name> <category> <item>");
        player.sendMessage(ChatColor.GOLD + "/highscore addentry <name> <category> <item>");
        player.sendMessage(ChatColor.GOLD + "/highscore addentry <name> <category> <item> <index>");

        player.sendMessage("");
        player.sendMessage(ChatColor.GOLD + "Commands mit dem jeweiligen Item in der Hand:");
        player.sendMessage(ChatColor.GOLD + "/highscore addcategory <name>");
        player.sendMessage(ChatColor.GOLD + "/highscore addcategory <name> <index>");
        player.sendMessage(ChatColor.GOLD + "/highscore additem <name> <category>");
        player.sendMessage(ChatColor.GOLD + "/highscore additem <name> <category> <index>");
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        ArrayList<String> list = new ArrayList<>();
        if (!sender.hasPermission("eden.edenutils.highscore.edit"))
            return list;
        if (args.length == 1) {
            String arg = args[0].toLowerCase();
            if ("reload".startsWith(arg))
                list.add("reload");
            if ("addcategory".startsWith(arg))
                list.add("addcategory");
            if ("movecategory".startsWith(arg))
                list.add("movecategory");
            if ("removecategory".startsWith(arg))
                list.add("removecategory");
            if ("renamecategory".startsWith(arg))
                list.add("renamecategory");
            if ("listcategories".startsWith(arg))
                list.add("listcategories");
            if ("addcontent".startsWith(arg))
                list.add("addcontent");
            if ("movecontent".startsWith(arg))
                list.add("movecontent");
            if ("removecontent".startsWith(arg))
                list.add("removecontent");
            if ("renamecontent".startsWith(arg))
                list.add("renamecontent");
            if ("addentry".startsWith(arg))
                list.add("addentry");
            if ("removeentry".startsWith(arg))
                list.add("removeentry");
            if ("removeplayerentries".startsWith(arg))
                list.add("removeplayerentries");
        }
        return list;
    }

    private boolean handleAddCategory(Player player, ItemStack item, String[] args) {
        if (args.length < 2) {
            player.sendMessage(ChatColor.GOLD + "/highscore addcategory <name>");
            player.sendMessage(ChatColor.GOLD + "/highscore addcategory <name> <index>");
            return true;
        }

        if (item.getType() == Material.AIR) {
            player.sendMessage(ChatColor.RED + "Du benötigst ein Item in der Hand um diesen Command auszuführen.");
            return true;
        }

        GameScoreCategory category = GameScoreOverview.getInstance().getCategoryByName(args[1]);
        if (category != null) {
            category.replaceDisplayItem(item);
            player.sendMessage(ChatColor.RED + "Diese Kategorie existiert bereits. Ihr Display-Item wurde aktualisiert.");
            return true;
        }

        int index = GameScoreOverview.getInstance().getCategories().size();
        if (args.length == 3) {
            try {
                index = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                player.sendMessage(ChatColor.RED + args[2] + " muss eine Zahl sein!");
                return true;
            }
        }

        try {
            GameScoreOverview.getInstance().addCategory(new GameScoreCategory(item, args[1]), index);
            player.sendMessage(ChatColor.GREEN + "Die Kategorie wurde erstellt.");
            GameScoreOverview.getInstance().save();
        } catch (IndexOutOfBoundsException e) {
            player.sendMessage(ChatColor.RED + "Die Kategorie konnte nicht am angegebenen Index eingefügt werden.");
        }
        return true;
    }

    private boolean handleMoveCategory(Player player, String[] args) {
        if (args.length != 3) {
            player.sendMessage(ChatColor.GOLD + "/highscore movecategory <name> <index>");
            return true;
        }

        GameScoreCategory category = GameScoreOverview.getInstance().getCategoryByName(args[1]);
        List<GameScoreCategory> categories = GameScoreOverview.getInstance().getCategories();
        int index;

        try {
            index = Integer.parseInt(args[2]);
        } catch (Exception e) {
            player.sendMessage(ChatColor.RED + args[2] + " muss eine Zahl sein!");
            return true;
        }

        if (categories.size() <= index || index < 0) {
            player.sendMessage(ChatColor.RED + "Der Index ist in einem ungültigem Bereich.");
            return true;
        }

        categories.remove(category);
        categories.add(index, category);
        player.sendMessage(ChatColor.GREEN + "Die Kategorie wurde verschoben.");
        GameScoreOverview.getInstance().save();
        return true;
    }

    private boolean handleRemoveCategory(Player player, String[] args) {
        if (args.length != 2) {
            player.sendMessage(ChatColor.GOLD + "/highscore removecategory <name>");
            return true;
        }

        if (GameScoreOverview.getInstance().removeCategory(args[1])) {
            player.sendMessage(ChatColor.GREEN + "Die Kategorie wurde entfernt.");
            GameScoreOverview.getInstance().save();
        } else {
            player.sendMessage(ChatColor.RED + "Die Kategorie existiert nicht.");
        }
        return true;
    }

    private boolean handleRenameCategory(Player player, String[] args) {
        if (args.length != 3) {
            player.sendMessage(ChatColor.GOLD + "/highscore renamecategory <oldName> <newName>");
            return true;
        }

        GameScoreOverview.getInstance().getCategoryByName(args[1]).setName(args[2]);
        player.sendMessage(ChatColor.GREEN + "Die Kategorie wurde umbenannt.");
        GameScoreOverview.getInstance().save();
        return true;
    }

    private boolean handleAddContent(Player player, ItemStack item, String[] args) {
        if (args.length < 3) {
            player.sendMessage(ChatColor.GOLD + "/highscore addcontent <name> <category> (<index>) (<entryOrderId>) (<entryFormatId>)");
            return true;
        }

        if (item.getType() == Material.AIR) {
            player.sendMessage(ChatColor.RED + "Du benötigst ein Item in der Hand um diesen Command auszuführen.");
            return true;
        }

        GameScoreCategory category = GameScoreOverview.getInstance().getCategoryByName(args[2]);
        if (category == null) {
            player.sendMessage(ChatColor.RED + "Diese Kategorie existiert nicht.");
            return true;
        }

        int contentsSize = category.getContents().size();
        int index = contentsSize;
        GameScoreContent.EntryOrder order = GameScoreContent.EntryOrder.NO_ORDER;
        GameScoreContent.EntryFormat format = GameScoreContent.EntryFormat.POINTS;
        if (args.length > 3) {
            try {
                index = Integer.parseInt(args[3]);
            } catch (NumberFormatException e) {
                player.sendMessage(ChatColor.RED + args[3] + " muss eine Zahl sein!");
                return true;
            }

            if (index < 0 || index > contentsSize) {
                player.sendMessage(ChatColor.RED + "Der Index ist in einem ungültigem Bereich.");
                return true;
            }

            if (args.length > 4) {
                try {
                    order = GameScoreContent.EntryOrder.valueOf(args[4]);
                } catch (NumberFormatException e) {
                    player.sendMessage(ChatColor.RED + args[4] + " muss eine gültige Zahl sein!");
                    return true;
                }
            }

            if (args.length > 5) {
                try {
                    format = GameScoreContent.EntryFormat.valueOf(args[5]);
                } catch (NumberFormatException e) {
                    player.sendMessage(ChatColor.RED + args[5] + " muss eine gültige Zahl sein!");
                    return true;
                }
            }
        }

        if (category.addContent(new GameScoreContent(args[1], item, format, order), index)) {
            player.sendMessage(ChatColor.GREEN + "Der Inhalt wurde zu " + ChatColor.GOLD
                    + category.getName() + ChatColor.GREEN + " hinzugefügt.");
        } else {
            player.sendMessage(ChatColor.GREEN + "Der Inhalt " + ChatColor.GOLD
                    + args[1] + ChatColor.GREEN + " wurde aktualisiert.");
        }

        GameScoreOverview.getInstance().save();
        return true;
    }

    private boolean handleMoveContent(Player player, String[] args) {
        if (args.length != 4) {
            player.sendMessage(ChatColor.GOLD + "/highscore movecitem <category> <name> <index>");
            return true;
        }

        int index;
        try {
            index = Integer.parseInt(args[3]);
        } catch (Exception e) {
            player.sendMessage(ChatColor.RED + args[3] + " muss eine Zahl sein!");
            return true;
        }

        GameScoreCategory category = GameScoreOverview.getInstance().getCategoryByName(args[1]);
        List<GameScoreContent> contents = category.getContents();
        if (contents.size() <= index || index < 0) {
            player.sendMessage(ChatColor.RED + "Der Index ist in einem ungültigem Bereich.");
            return true;
        }
        GameScoreContent content = category.getContentByName(args[2]);
        contents.remove(content);
        contents.add(index, content);
        player.sendMessage(ChatColor.GREEN + "Die Kategorie wurde verschoben.");
        GameScoreOverview.getInstance().save();
        return true;
    }

    private boolean handleRemoveContent(Player player, String[] args) {
        if (args.length < 3) {
            player.sendMessage(ChatColor.GOLD + "/highscore removecontent <name> <category>");
            return true;
        }

        GameScoreCategory category = GameScoreOverview.getInstance().getCategoryByName(args[2]);
        GameScoreContent content = category.getContentByName(args[1]);

        if (content != null) {
            if (category.removeContent(content)) {
                player.sendMessage(ChatColor.GREEN + "Der Inhalt " + ChatColor.GOLD
                        + args[1] + ChatColor.GREEN + " wurde gelöscht.");
            }
        } else {
            player.sendMessage(ChatColor.RED + "Der Inhalt konnte nicht gefunden werden.");
        }

        GameScoreOverview.getInstance().save();
        return true;
    }

    private boolean handleRenameContent(Player player, String[] args) {
        if (args.length != 4) {
            player.sendMessage(ChatColor.GOLD + "/highscore renamecontent <category> <oldName> <newName>");
            return true;
        }

        GameScoreCategory category = GameScoreOverview.getInstance().getCategoryByName(args[1]);
        if (category == null) {
            player.sendMessage(ChatColor.RED + "Diese Kategorie existiert nicht.");
            return true;
        }


        if (category.renameContent(args[2], args[3])) {
            player.sendMessage(ChatColor.GREEN + "Der Inhalt wurde umbenannt.");
        } else {
            player.sendMessage(ChatColor.GREEN + "Der Inhalt konnte nicht umbenannt werden.");
        }
        GameScoreOverview.getInstance().save();
        return true;
    }

    public boolean addEntry(String categoryName, String subcategoryName, GameScoreEntry entry){
        return addEntry(categoryName, subcategoryName, entry, false, null);
    }

    public boolean addEntry(String categoryName, String subcategoryName, GameScoreEntry entry, boolean forced){
        return addEntry(categoryName, subcategoryName, entry, forced, Material.GRASS_BLOCK);
    }

    public boolean addEntry(String categoryName, String subcategoryName, GameScoreEntry entry, boolean forced, Material itemMat) {
        GameScoreCategory category = GameScoreOverview.getInstance().getCategoryByName(categoryName);
        GameScoreContent content;
        if (category == null) {
            if (forced) {
                category = new GameScoreCategory(new ItemStack(itemMat), categoryName);
                content = new GameScoreContent(subcategoryName, new ItemStack(itemMat));
                content.addEntry(entry);
                category.addContent(content, 0);
                GameScoreOverview.getInstance().addCategory(category);
            }
            Bukkit.broadcast(ChatColor.RED + "Die Kategorie " + categoryName + " existiert nicht.",
                    "eden.edenutils.highscore.edit");
            return true;
        }

        content = category.getContentByName(subcategoryName);

        if (content == null) {
            if (forced) {
                content = new GameScoreContent(subcategoryName, new ItemStack(itemMat));
                content.addEntry(entry);
                category.addContent(content, 0);
            }
            Bukkit.broadcast(ChatColor.RED + "Die Unterkategorie " + subcategoryName + " existiert nicht.",
                    "eden.edenutils.highscore.edit");
            return true;
        }

        content.addEntry(entry);

        GameScoreOverview.getInstance().save();
        return true;
    }

    private boolean handleRemoveEntry(Player player, String[] args) {
        if (args.length < 4) {
            player.sendMessage(ChatColor.GOLD + "/highscore removeentry <name> <category> <subcategory>");
            return true;
        }

        GameScoreCategory category = GameScoreOverview.getInstance().getCategoryByName(args[2]);
        if (category == null) {
            player.sendMessage(ChatColor.RED + "Die Kategorie " + args[2] + " existiert nicht.");
            return true;
        }

        GameScoreContent content = category.getContentByName(args[1]);
        if (content == null) {
            player.sendMessage(ChatColor.RED + "Die Unterkategorie " + args[3] + " existiert nicht.");
            return true;
        }

        if (content.removeEntry(args[1])) {
            player.sendMessage(ChatColor.GREEN + "Der Eintrag des Spielers " + ChatColor.GOLD
                    + args[1] + ChatColor.GREEN + " in " + args[2] + "/" + args[3] + " wurde gelöscht.");
        } else {
            player.sendMessage(ChatColor.GREEN + "Der Eintrag konnte nicht gefunden werden.");
        }

        GameScoreOverview.getInstance().save();
        return true;
    }

    private boolean removeEntriesFromPlayer(Player player, String[] args) {
        if (args.length < 2) {
            player.sendMessage(ChatColor.GOLD + "/highscore removeplayerentries <name>");
            return true;
        }

        int removeCount = 0;
        for (GameScoreCategory category : GameScoreOverview.getInstance().getCategories()) {
            for (GameScoreContent content : category.getContents()) {
                if (content.removeEntry(args[1])) {
                    removeCount++;
                }
            }
        }
        player.sendMessage(ChatColor.GREEN + "Es wurden " + removeCount + " Einträge von Spieler " + args[1] + " entfernt!");
        return true;
    }
}