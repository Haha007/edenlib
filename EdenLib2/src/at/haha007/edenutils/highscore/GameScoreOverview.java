package at.haha007.edenutils.highscore;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GameScoreOverview {
    public static GameScoreOverview instance;
    private File file = new File(EdenLib.instance.getDataFolder(), "highscores.yml");
    private String title = ChatColor.GREEN + "Highscores";
    private String categoryTitle = ChatColor.AQUA + "Highscores";
    private ArrayList<GameScoreCategory> categories = new ArrayList<>();

    private GameScoreOverview() {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        for (String key : cfg.getKeys(false)) {
            categories.add(new GameScoreCategory(cfg.getConfigurationSection(key), key));
        }
    }

    public static GameScoreOverview getInstance() {
        return instance == null ? instance = new GameScoreOverview() : instance;
    }

    public void reload() {
        instance = new GameScoreOverview();
    }

    public void save() {
        YamlConfiguration cfg = new YamlConfiguration();
        for (GameScoreCategory category : categories) {
            ConfigurationSection section = cfg.createSection(category.getName());
            category.save(section);
        }
        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openOverviewInventory(Player player) {
        openOverviewPage(player, 0);
    }

    private void openOverviewPage(Player player, int page) {
        if (!player.hasPermission("eden.edenutils.highscore.open")) {
            player.sendMessage(ChatColor.RED + "Du hast keine ausreichenden Rechte um die Highscores anzusehen.");
            return;
        }

        int size = ((categories.size() + 9) / 9) * 9;
        if (page > size / 36 && size % 36 > 0) {
            page -= 1;
        }

        boolean hasMorePages = false;
        if (size <= 36) {
            size += 18;
        } else {
            size = 54;
            hasMorePages = true;
        }

        if (page < 0) {
            page = 0;
        }

        Inventory inv = Bukkit.createInventory(null, size, title);
        boolean editPermission = player.hasPermission("eden.edenutils.highscore.edit");

        inv.setItem(0, Utils.getItem(Material.BARRIER, ChatColor.GREEN + "Schließen"));
        inv.setItem(1, Utils.getItem(Material.BOOK, ChatColor.GREEN + "- Info -",
                ChatColor.AQUA + "Klicke auf ein Item um dir \ndie jeweiligen Highscorekategorien anzeigen zu lassen.",
                hasMorePages ? ChatColor.AQUA + "Benutze das Papier um die Seiten zu wechseln." : ""));

        if (hasMorePages) {
            inv.setItem(7, Utils.getItem(Material.PAPER, ChatColor.GREEN + "Vorherige Seite",
                    ChatColor.GOLD + "Aktuelle Seite: " + ChatColor.AQUA + (page + 1)));
            inv.setItem(8, Utils.getItem(Material.PAPER, ChatColor.GREEN + "Nächste Seite",
                    ChatColor.GOLD + "Aktuelle Seite: " + ChatColor.AQUA + (page + 1)));
        }

        int offset = page * 36;
        for (int i = 0; (i < 36) && (offset + i < categories.size()); i++) {
            int index = offset + i;
            GameScoreCategory category = categories.get(index);
            ItemStack item = category.getItem();
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(category.getName());

            if (editPermission) {
                List<String> lore = item.getLore();
                if (lore == null) {
                    lore = new ArrayList<>();
                }
                lore.add(0, ChatColor.DARK_PURPLE + "Scores: " + ChatColor.LIGHT_PURPLE
                        + category.getName());
                meta.setLore(lore);
                item.setItemMeta(meta);
            }
            inv.setItem(i + 18, item);
        }
        player.openInventory(inv);
    }

    public void clickOverviewInventory(InventoryClickEvent event) {
        if (event.getClick() != ClickType.LEFT) {
            return;
        }

        InventoryView view = event.getView();
        if (event.getClickedInventory() != view.getTopInventory()) {
            return;
        }

        Player player = (Player) event.getWhoClicked();
        if (!player.hasPermission("eden.edenutils.highscore.open")) {
            return;
        }

        ItemStack item = event.getCurrentItem();
        if (item == null || item.getType() == Material.AIR) {
            return;
        }

        Inventory inv = event.getClickedInventory();
        int page = getPage(inv);
        switch (event.getSlot()) {
            case 0:
                player.closeInventory();
                break;
            case 7:
                openOverviewPage(player, page - 1);
                break;
            case 8:
                openOverviewPage(player, page + 1);
                break;
        }

        UUID uuid = Utils.getItemUUID(item);
        if (uuid == null) {
            return;
        }

        GameScoreCategory category = getCategoryByID(uuid);
        if (category == null) {
            return;
        }

        openCategoryInventory(player, category);
    }

    private void openCategoryInventory(Player player, GameScoreCategory category) {
        if (!player.hasPermission("eden.edenutils.highscore.open")) {
            player.sendMessage(ChatColor.RED + "Du hast keine ausreichenden Rechte um die Highscores anzusehen.");
            return;
        }

        int size = ((categories.size() + 9) / 9) * 9 + 18;
        categoryTitle = ChatColor.AQUA + "Highscores - " + category.getName();
        Inventory inv = Bukkit.createInventory(null, size, categoryTitle);
        boolean editPermission = player.hasPermission("eden.edenutils.highscore.edit");

        inv.setItem(0, Utils.getItem(Material.BARRIER, ChatColor.GREEN + "Zurück"));
        inv.setItem(1, Utils.setItemUUID(Utils.getItem(Material.BOOK, ChatColor.GREEN + "- Info -",
                ChatColor.AQUA + "Klicke auf ein Item um dir \ndie jeweiligen Highscores anzeigen zu lassen.",
                ChatColor.AQUA + "Benutze das Papier um die Seiten zu wechseln."), category.getUuid()));

        ArrayList<GameScoreContent> items = category.getContents();
        for (int i = 0; i < items.size(); i++) {
            GameScoreContent contentItem = items.get(i);
            ItemStack item = contentItem.getDisplayItem();
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(contentItem.getName());

            if (editPermission) {
                List<String> lore = item.getLore();
                if (lore == null) {
                    lore = new ArrayList<>();
                }

                lore.add(0, ChatColor.DARK_PURPLE + "Item Index: " + ChatColor.LIGHT_PURPLE + i);
                meta.setLore(lore);
                item.setItemMeta(meta);
            }
            inv.setItem(i + 18, item);
        }
        player.openInventory(inv);
    }

    public void clickCategoryInventory(InventoryClickEvent event) {
        if (event.getClick() != ClickType.LEFT) {
            return;
        }

        InventoryView view = event.getView();
        if (event.getClickedInventory() != view.getTopInventory()) {
            return;
        }

        Player player = (Player) event.getWhoClicked();
        if (!player.hasPermission("eden.edenutils.highscore.open")) {
            return;
        }

        ItemStack item = event.getCurrentItem();
        if (item == null || item.getType() == Material.AIR) {
            return;
        }

        Inventory inv = event.getClickedInventory();
        GameScoreCategory category = getCategoryByID(Utils.getItemUUID(inv.getItem(1)));

        if (event.getSlot() == 0) {
            openOverviewInventory(player);
        } else if (event.getSlot() == 2) {
            openCategoryInventory(player, category);
        }

        if (event.getSlot() < 18) {
            return;
        }

        if (player.getGameMode() == GameMode.SPECTATOR) {
            return;
        }

        UUID uuid = Utils.getItemUUID(item);
        if (uuid == null) {
            return;
        }

        GameScoreContent contentItem = category.getItem(uuid);
        if (contentItem == null) {
            return;
        }

        contentItem.printEntries(player, category.getName());
    }

    private GameScoreCategory getCategoryByID(UUID uuid) {
        for (GameScoreCategory category : categories) {
            if (category.getUuid().equals(uuid))
                return category;
        }
        return null;
    }

    public GameScoreCategory getCategoryByName(String name) {
        name = name.toLowerCase();
        for (GameScoreCategory category : categories) {
            if (category.getName().toLowerCase().equals(name))
                return category;
        }
        return null;
    }

    public void addCategory(GameScoreCategory category){
        categories.add(category);
    }

    public void addCategory(GameScoreCategory category, int index) {
        categories.add(index, category);
    }

    public boolean removeCategory(String identifier) {
        GameScoreCategory category = getCategoryByName(identifier);
        if (category != null) {
            categories.remove(category);
            return true;
        }
        return false;
    }

    private int getPage(Inventory inv) {
        ItemStack item = inv.getItem(8);
        if (item == null || item.getType() == Material.AIR)
            return 1;

        ItemMeta meta = item.getItemMeta();
        if (meta == null)
            return 1;

        List<String> lore = meta.getLore();
        if (lore == null || lore.isEmpty())
            return 1;

        String pageString = lore.get(0);

        try {
            return Integer.parseInt(pageString.replaceFirst(ChatColor.GOLD + "Aktuelle Seite: " + ChatColor.AQUA, ""));
        } catch (NumberFormatException e) {
            return 1;
        }
    }

    public ArrayList<GameScoreCategory> getCategories() {
        return categories;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public String getTitle() {
        return title;
    }
}