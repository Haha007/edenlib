package at.haha007.edenutils.highscore;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class GameScoreEntry {
    protected String playerName;
    protected int points;
    protected int count = 0;

    public GameScoreEntry(String _playerName, int _points) {
        playerName = _playerName;
        points = _points;
    }

    public static void listEntryFormats(Player player) {
        int count = 0;
        player.sendMessage(ChatColor.GREEN + (count++ + " Default:\t " + ChatColor.YELLOW + "Deine Platzierung: X Deine Spiele: X Dein Score: X"));
        player.sendMessage(ChatColor.GREEN + (count++ + " Survival:\t " + ChatColor.YELLOW + "Deine Platzierung: X Deine Zeit: XX:XX:XXX Deine Spiele: X Dein Score: X"));
        player.sendMessage(ChatColor.GREEN + (count++ + " Time:\t " + ChatColor.YELLOW + "Deine Platzierung: X Deine Zeit: XX:XX:XXX Deine Spiele: X Dein Score: X"));
        player.sendMessage(ChatColor.GOLD + (count + " Neues Format hinzufügen"));
    }

    public static String formatEntryByFormatId(GameScoreEntry entry, GameScoreContent.EntryFormat formatId) {
        switch (formatId) {
            case TIME:
                if(entry instanceof GameTimeScoreEntry){
                    GameTimeScoreEntry timeEntry = (GameTimeScoreEntry) entry;
                    StringBuilder sb = new StringBuilder();
                    sb.append(ChatColor.GOLD);
                    sb.append("Deine Zeit: ");
                    sb.append(ChatColor.YELLOW);
                    sb.append(timeEntry.getTimeString());
                    sb.append(ChatColor.GOLD);
                    sb.append("Deine Versuche: ");
                    sb.append(ChatColor.YELLOW);
                    sb.append(timeEntry.getCount());
                    sb.append(ChatColor.GOLD);
                    sb.append("Dein Score: ");
                    sb.append(ChatColor.YELLOW);
                    sb.append(timeEntry.getSecondaryValue());
                    return sb.toString();
                }
                break;
            case POINTS:
                StringBuilder sb = new StringBuilder();
                sb.append(ChatColor.GOLD);
                sb.append("Deine Punkte: ");
                sb.append(ChatColor.YELLOW);
                sb.append(entry.getMainValue());
                sb.append(ChatColor.GOLD);
                sb.append("Deine Versuche: ");
                sb.append(ChatColor.YELLOW);
                sb.append(entry.getCount());
                return sb.toString();
        }
        return ChatColor.RED + "\nDein Score kann leider nicht angezeigt werden. Wir arbeiten daran.";
    }

    public void updateEntry(GameScoreEntry entry) {
        if (playerName.equals(entry.getPlayerName())) {
            updateEntry(entry.points);
        }
    }

    protected void updateEntry(int _points) {
        if (points < _points) {
            points = _points;
        }
        count++;
    }

    public void updateEntry(String name) {
        playerName = name;
    }

    public String getPlayerName() {
        return playerName;
    }

    public long getMainValue() {
        return points;
    }

    public long getSecondaryValue() {
        return count;
    }

    public int getCount(){
        return count;
    }

    public int compareTo(Object object) {
        if (object instanceof GameScoreEntry) {
            GameScoreEntry entry = (GameScoreEntry) object;
            if (getMainValue() < entry.getMainValue()
                    || (getMainValue() == entry.getMainValue() && getSecondaryValue() <= entry.getSecondaryValue())) {

                return -1;
            }
        }
        return 1;
    }

    public String toString() {
        return ChatColor.YELLOW + playerName + ChatColor.GOLD + "\t Punkte:\t " + ChatColor.YELLOW + points
                + ChatColor.GOLD + "\t Spiele:\t " + ChatColor.YELLOW + count + "\n";
    }

    public StringBuilder appendTo(StringBuilder builder) {
        builder.append(ChatColor.YELLOW);
        builder.append(playerName);
        builder.append(ChatColor.GOLD);
        builder.append("\t Punkte:\t ");
        builder.append(ChatColor.YELLOW);
        builder.append(points);
        builder.append(ChatColor.GOLD);
        builder.append("\t Spiele:\t ");
        builder.append(ChatColor.YELLOW);
        builder.append(count);
        builder.append("\n");

        return builder;
    }
}