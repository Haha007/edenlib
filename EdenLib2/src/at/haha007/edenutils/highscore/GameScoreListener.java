package at.haha007.edenutils.highscore;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;

public class GameScoreListener implements Listener {
    @EventHandler
    void onInventoryClick(InventoryClickEvent event) {
        InventoryView view = event.getView();
        GameScoreOverview highscores = GameScoreOverview.getInstance();
        String title = view.getTitle();

        if (title.equals(highscores.getTitle())) {
            event.setCancelled(true);
            highscores.clickOverviewInventory(event);
            return;
        }

        if (title.equals(highscores.getCategoryTitle())) {
            event.setCancelled(true);
            highscores.clickCategoryInventory(event);
            return;
        }
    }
}