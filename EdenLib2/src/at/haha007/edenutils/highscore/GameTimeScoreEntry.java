package at.haha007.edenutils.highscore;

import org.bukkit.ChatColor;

public class GameTimeScoreEntry extends GameScoreEntry {

    private long time;

    public GameTimeScoreEntry(String _playerName, long _time, int _points) {
        super(_playerName, _points);
        time = _time;
    }

    public String getTimeString(){
        long tempTime = time;
        int millis = (int)(tempTime % 1000);
        tempTime /= 1000;
        int secs = (int)(tempTime % 60);
        tempTime /= 60;
        int mins = (int)(tempTime % 60);
        tempTime /= 60;
        int hours = (int)(tempTime);
        return hours + ":" + mins + ":" + secs + "." + millis;
    }

    @Override
    public void updateEntry(GameScoreEntry entry) {
        if (entry instanceof GameTimeScoreEntry && playerName.equals(entry.getPlayerName())) {
            updateTime(((GameTimeScoreEntry) entry).time);
            updateEntry(entry.points);
        }
    }

    private void updateTime(long _time){
        time = _time;
    }

    @Override
    public long getMainValue() {
        return time;
    }

    @Override
    public long getSecondaryValue() {
        return points;
    }

    public StringBuilder appendTo(StringBuilder builder) {
        builder.append(ChatColor.YELLOW);
        builder.append(playerName);
        builder.append(ChatColor.GOLD);
        builder.append("\t Zeit:\t ");
        builder.append(ChatColor.YELLOW);
        builder.append(getTimeString());
        builder.append(ChatColor.GOLD);
        builder.append("\t Punkte:\t ");
        builder.append(ChatColor.YELLOW);
        builder.append(points);
        builder.append(ChatColor.GOLD);
        builder.append("\t Spiele:\t ");
        builder.append(ChatColor.YELLOW);
        builder.append(count);
        builder.append("\n");

        return builder;
    }
}