package at.haha007.edenutils.highscore;

import at.haha007.edenlib.utils.Utils;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.UUID;

public class GameScoreCategory {
    private String name;
    private ItemStack item;
    private ArrayList<GameScoreContent> contents = new ArrayList<>();
    private UUID uuid = UUID.randomUUID();

    public GameScoreCategory(ConfigurationSection cfg, String name) {
        ConfigurationSection items = cfg.getConfigurationSection("items");
        for (String key : items.getKeys(false)) {
            contents.add(new GameScoreContent(items.getConfigurationSection(key)));
        }
        item = cfg.getItemStack("item", new ItemStack(Material.GRASS_BLOCK));
        this.name = name;
    }

    public GameScoreCategory(ItemStack item, String name) {
        this.name = name;
        this.item = item;
    }

    ArrayList<GameScoreContent> getContents() {
        return contents;
    }

    GameScoreContent getContentByName(String name) {
        for (GameScoreContent content : contents) {
            if (content.getName().equals(name)) {
                return content;
            }
        }
        return null;
    }

    public boolean addContent(GameScoreContent _content, int index) {
        GameScoreContent content = getContentByName(_content.getName());
        if (content == null) {
            contents.add(index, _content);
            return true;
        } else {
            content.updateContent(_content);
            return false;
        }
    }

    public boolean removeContent(GameScoreContent content) {
        return contents.remove(content);
    }

    public boolean renameContent(String oldName, String newName) {
        GameScoreContent content = getContentByName(oldName);
        if (content != null) {
            content.setName(newName);
            return true;
        }
        return false;
    }

    public void replaceDisplayItem(ItemStack _item) {
        item = _item;
    }

    public ItemStack getItem() {
        return Utils.setItemUUID(item, uuid);
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void save(ConfigurationSection cfg) {
        cfg.set("item", item);
        ConfigurationSection items = cfg.createSection("items");
        for (int i = 0; i < contents.size(); i++) {
            contents.get(i).save(items.createSection(Integer.toString(i)));
        }
    }

    public GameScoreContent getItem(UUID uuid) {
        for (GameScoreContent contentItem : contents) {
            if (contentItem.getUuid().equals(uuid)) {
                return contentItem;
            }
        }
        return null;
    }
}