package at.haha007.edenutils.adminshop;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Utils;

public class Adminshop {
	private static Adminshop instance;

	public static Adminshop getAdminshop() {
		return instance == null ? instance = new Adminshop() : instance;
	}

	private File file = new File(EdenLib.instance.getDataFolder(), "adminshop.yml");
	private ArrayList<AdminshopCategory> categories = new ArrayList<>();
	private String mainName = ChatColor.GREEN + "Adminshop";
	private String categoryName = ChatColor.AQUA + "Adminshop";

	private Adminshop() {
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		for (String key : cfg.getKeys(false)) {
			categories.add(new AdminshopCategory(cfg.getConfigurationSection(key), key));
		}
	}

	public void reload() {
		instance = new Adminshop();
	}

	void save() {
		YamlConfiguration cfg = new YamlConfiguration();
		for (AdminshopCategory adminShopCategory : categories) {
			ConfigurationSection section = cfg.createSection(adminShopCategory.getName());
			adminShopCategory.save(section);
		}
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void openMainInventory(Player player) {
		if (!player.hasPermission("eden.edenutils.adminshop.open")) {
			player.sendMessage(ChatColor.RED + "Du hast keine ausreichenden Rechte um den Adminshop zu öffnen.");
			return;
		}
		boolean editPermission = player.hasPermission("eden.edenutils.adminshop.edit");
		int size = ((categories.size() + 9) / 9) * 9;
		Inventory inv = Bukkit.createInventory(null, size, mainName);
		for (AdminshopCategory adminshopCategory : categories) {
			ItemStack item = adminshopCategory.getItem();
			if (editPermission) {
				ItemMeta meta = item.getItemMeta();
				List<String> lore = item.getLore();
				if (lore == null)
					lore = new ArrayList<String>();
				lore.add(0, ChatColor.DARK_PURPLE + "Category Name: " + ChatColor.LIGHT_PURPLE
						+ adminshopCategory.getName());
				meta.setLore(lore);
				item.setItemMeta(meta);
			}
			inv.addItem(item);
		}
		player.openInventory(inv);
	}

	public void clickMainInventory(InventoryClickEvent event) {
		if (event.getClick() != ClickType.LEFT)
			return;
		InventoryView view = event.getView();
		if (event.getClickedInventory() != view.getTopInventory())
			return;
		Player player = (Player) event.getWhoClicked();
		if (!player.hasPermission("eden.edenutils.adminshop.open"))
			return;

		ItemStack item = event.getCurrentItem();
		if (item == null || item.getType() == Material.AIR)
			return;

		UUID uuid = Utils.getItemUUID(item);
		if (uuid == null)
			return;

		AdminshopCategory category = getCategory(uuid);
		if (category == null)
			return;

		openCategoryInventory(player, category, 1, 1);
	}

	public void openCategoryInventory(Player player, AdminshopCategory category, int page, int count) {
		if (!player.hasPermission("eden.edenutils.adminshop.open")) {
			player.sendMessage(ChatColor.RED + "Du hast keine ausreichenden Rechte um den Adminshop zu öffnen.");
			return;
		}
		if (page < 1)
			page = 1;
		Inventory inv = Bukkit.createInventory(null, 54, categoryName);
		boolean editPermission = player.hasPermission("eden.edenutils.adminshop.edit");

		// Info
		inv.setItem(1,
				Utils.setItemUUID(Utils.getItem(Material.BOOK, ChatColor.GREEN + "- Info -",
						ChatColor.AQUA + "Klicke auf ein Item um es zu kaufen.",
						ChatColor.AQUA + "Benutze das Papier um die Seiten zu wechseln."), category.getUuid()));

		// Anzahl
		ItemStack anzahl = Utils.getItem(Material.WHITE_WOOL, ChatColor.GREEN + "Anzahl: " + ChatColor.GOLD + count,
				ChatColor.AQUA + "Anzahl der Items die auf einmal gekauft werden.",
				ChatColor.AQUA + "Klicken um es zu ändern.");
		anzahl.setAmount(count);
		inv.setItem(2, anzahl);

		// Umblättern
		inv.setItem(7, Utils.getItem(Material.PAPER, ChatColor.GREEN + "Vorherige Seite",
				ChatColor.GOLD + "Aktuelle Seite: " + ChatColor.AQUA + page));
		inv.setItem(8, Utils.getItem(Material.PAPER, ChatColor.GREEN + "Nächste Seite",
				ChatColor.GOLD + "Aktuelle Seite: " + ChatColor.AQUA + page));

		// Zurück
		inv.setItem(0, Utils.getItem(Material.BARRIER, ChatColor.GREEN + "Zurück"));

		// Items
		ArrayList<AdminshopItem> items = category.getContents();
		int offset = (page - 1) * 36;
		try {
			for (int i = 0; i < 36; i++) {
				int index = offset + i;
				AdminshopItem adminshopItem = items.get(index);
				ItemStack item = adminshopItem.getDisplayItem();
				if (editPermission) {
					ItemMeta meta = item.getItemMeta();
					List<String> lore = item.getLore();
					if (lore == null)
						lore = new ArrayList<String>();
					lore.add(0, ChatColor.DARK_PURPLE + "Item Index: " + ChatColor.LIGHT_PURPLE + index);
					meta.setLore(lore);
					item.setItemMeta(meta);
				}
				inv.setItem(i + 18, item);
			}
		} catch (IndexOutOfBoundsException e) {
		}

		player.openInventory(inv);
	}

	public void clickCategoryInventory(InventoryClickEvent event) {
		if (event.getClick() != ClickType.LEFT)
			return;
		InventoryView view = event.getView();
		if (event.getClickedInventory() != view.getTopInventory())
			return;
		Player player = (Player) event.getWhoClicked();
		if (!player.hasPermission("eden.edenutils.adminshop.open"))
			return;

		ItemStack item = event.getCurrentItem();
		if (item == null || item.getType() == Material.AIR)
			return;

		Inventory inv = event.getClickedInventory();
		int page = getPage(inv);
		AdminshopCategory category = getCategory(Utils.getItemUUID(inv.getItem(1)));
		int count = getCount(inv);

		switch (event.getSlot()) {
		case 0:
			openMainInventory(player);
			break;
		case 2:
			count *= 2;
			if (count > 64)
				count = 1;
			openCategoryInventory(player, category, page, count);
			break;
		case 7:
			openCategoryInventory(player, category, page - 1, count);
			break;
		case 8:
			openCategoryInventory(player, category, page + 1, count);
			break;

		default:
			break;
		}

		if (event.getSlot() < 18)
			return;
		if (player.getGameMode() == GameMode.SPECTATOR)
			return;

		UUID uuid = Utils.getItemUUID(item);
		if (uuid == null)
			return;
		AdminshopItem adminshopItem = category.getItem(uuid);
		if (adminshopItem == null)
			return;
		double price = player.getGameMode() == GameMode.CREATIVE ? 0 : adminshopItem.getPrice() * count;

		Essentials ess = EdenLib.essentials;
		User user = ess.getUser(player);

		if (user.getMoney().doubleValue() < price && player.getGameMode() != GameMode.CREATIVE)
			return;

		user.takeMoney(BigDecimal.valueOf(price));
		ItemStack buyItem = adminshopItem.getItem();
		buyItem.setAmount(count);
		Utils.giveItem(player, buyItem);
	}

	private int getCount(Inventory inv) {
		ItemStack item = inv.getItem(2);

		if (item == null || item.getType() == Material.AIR)
			return 1;

		return item.getAmount();
	}

	private int getPage(Inventory inv) {
		ItemStack item = inv.getItem(8);
		if (item == null || item.getType() == Material.AIR)
			return 1;

		ItemMeta meta = item.getItemMeta();
		if (meta == null)
			return 1;

		List<String> lore = meta.getLore();
		if (lore == null || lore.isEmpty())
			return 1;

		String pageString = lore.get(0);

		try {
			return Integer.parseInt(pageString.replaceFirst(ChatColor.GOLD + "Aktuelle Seite: " + ChatColor.AQUA, ""));
		} catch (NumberFormatException e) {
			return 1;
		}
	}

	private AdminshopCategory getCategory(UUID uuid) {
		for (AdminshopCategory category : categories) {
			if (category.getUuid().equals(uuid))
				return category;
		}
		return null;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public String getMainName() {
		return mainName;
	}

	public ArrayList<AdminshopCategory> getCategories() {
		return categories;
	}

	public File getFile() {
		return file;
	}

	public AdminshopCategory getCategory(String name) {
		name = name.toLowerCase();
		for (AdminshopCategory adminshopCategory : categories) {
			if (adminshopCategory.getName().toLowerCase().equals(name))
				return adminshopCategory;
		}
		return null;
	}

	public void addCategory(AdminshopCategory adminshopCategory) {
		categories.add(adminshopCategory);
	}

	public void addCategory(AdminshopCategory adminshopCategory, int index) {
		categories.add(index, adminshopCategory);
	}

	public boolean removeCategory(String name) {
		for (int i = 0; i < categories.size(); i++) {
			AdminshopCategory category = categories.get(i);
			if (category.getName().equalsIgnoreCase(name)) {
				categories.remove(i);
				return true;
			}
		}
		return false;
	}
}
