package at.haha007.edenutils.adminshop;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import at.haha007.edenlib.utils.Utils;

public class AdminshopCategory {
	private String name;
	private ItemStack item;
	private ArrayList<AdminshopItem> contents = new ArrayList<>();
	private UUID uuid = UUID.randomUUID();

	public AdminshopCategory(ConfigurationSection cfg, String name) {
		ConfigurationSection items = cfg.getConfigurationSection("items");
		for (String key : items.getKeys(false)) {
			contents.add(new AdminshopItem(items.getConfigurationSection(key)));
		}
		item = cfg.getItemStack("item", new ItemStack(Material.GRASS_BLOCK));
		this.name = name;
	}

	public AdminshopCategory(ItemStack item, String name) {
		this.name = name;
		this.item = item;
	}

	public ArrayList<AdminshopItem> getContents() {
		return contents;
	}

	public void setItem(ItemStack item) {
		this.item = item;
	}

	public ItemStack getItem() {
		return Utils.setItemUUID(item, uuid);
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getName() {
		return name;
	}

	public void save(ConfigurationSection cfg) {
		cfg.set("item", item);
		ConfigurationSection items = cfg.createSection("items");
		for (int i = 0; i < contents.size(); i++) {
			contents.get(i).save(items.createSection(Integer.toString(i)));
		}
	}

	public AdminshopItem getItem(UUID uuid) {
		for (AdminshopItem adminshopItem : contents) {
			if (adminshopItem.getUuid().equals(uuid))
				return adminshopItem;
		}
		return null;
	}

	public void setName(String name) {
		this.name = name;
	}
}
