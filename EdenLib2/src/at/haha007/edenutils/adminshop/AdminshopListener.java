package at.haha007.edenutils.adminshop;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;

public class AdminshopListener implements Listener {
	@EventHandler
	void onInventoryClick(InventoryClickEvent event) {
		InventoryView view = event.getView();
		Adminshop adminshop = Adminshop.getAdminshop();
		String title = view.getTitle();

		if (title.equals(adminshop.getMainName())) {
			event.setCancelled(true);
			adminshop.clickMainInventory(event);
			return;
		}

		if (title.equals(adminshop.getCategoryName())) {
			event.setCancelled(true);
			adminshop.clickCategoryInventory(event);
			return;
		}

	}
}
