package at.haha007.edenutils.adminshop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import at.haha007.edenlib.utils.Messages;

public class AdminshopCommand implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.notplayer"));
			return true;
		}
		Player player = (Player) sender;
		if (args.length == 0 || !player.hasPermission("eden.edenutils.adminshop.edit")) {
			Adminshop.getAdminshop().openMainInventory(player);
			return true;
		}
		ItemStack item = new ItemStack(player.getInventory().getItemInMainHand());
		item.setAmount(1);

		switch (args[0].toLowerCase()) {
		case "reload":
			Adminshop.getAdminshop().reload();
			player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.reload"));
			return true;
		case "addcategory":
			if (args.length < 2) {
				player.sendMessage(ChatColor.GOLD + "/adminshop addcategory name");
				player.sendMessage(ChatColor.GOLD + "/adminshop addcategory name index");
				return true;
			}
			if (Adminshop.getAdminshop().getCategory(args[1]) != null) {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.exists"));
				return true;
			}
			if (item == null || item.getType() == Material.AIR) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noitem"));
				return true;
			}
			int index = Adminshop.getAdminshop().getCategories().size();
			if (args.length == 3) {
				try {
					index = Integer.parseInt(args[2]);
				} catch (NumberFormatException e) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
							.replaceAll("\\{NUMBER\\}", args[2]));
					return true;
				}
			}
			try {
				Adminshop.getAdminshop().addCategory(new AdminshopCategory(item, args[1]), index);
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.create")
						.replaceAll("\\{CATEGORY\\}", args[1]));
				Adminshop.getAdminshop().save();
			} catch (IndexOutOfBoundsException e) {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.create.outofbounds"));
			}
			return true;

		case "removecategory":
			if (args.length != 2) {
				player.sendMessage(ChatColor.GOLD + "/adminshop removecategory name");
				return true;
			}
			if (Adminshop.getAdminshop().removeCategory(args[1])) {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.delete")
						.replaceAll("\\{CATEGORY\\}", args[1]));
				Adminshop.getAdminshop().save();
			} else {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.notexists")
						.replaceAll("\\{CATEGORY\\}", args[1]));
			}
			return true;

		case "renamecategory":
			if (args.length != 3) {
				player.sendMessage(ChatColor.GOLD + "/adminshop renamecategory oldName newName");
				return true;
			}
			AdminshopCategory category = Adminshop.getAdminshop().getCategory(args[1]);
			if (category == null) {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.notexists")
						.replaceAll("\\{CATEGORY\\}", args[1]));
				return true;
			}
			category.setName(args[2]);
			player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.rename")
					.replaceAll("\\{OLD\\}", args[1]).replaceAll("\\{NEW\\}", args[2]));
			Adminshop.getAdminshop().save();
			return true;

		case "setcategoryitem":
			if (args.length != 3) {
				player.sendMessage(ChatColor.GOLD + "/adminshop setcategoryitem category");
				return true;
			}
			if (item == null || item.getType() == Material.AIR) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noitem"));
				return true;
			}
			category = Adminshop.getAdminshop().getCategory(args[1]);
			if (category == null) {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.notexists")
						.replaceAll("\\{CATEGORY\\}", args[1]));
				return true;
			}
			category.setItem(item);
			player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.changeitem")
					.replaceAll("\\{CATEGORY\\}", args[1]));
			Adminshop.getAdminshop().save();
			return true;

		case "movecategory":
			if (args.length != 3) {
				player.sendMessage(ChatColor.GOLD + "/adminshop movecategory name index");
				return true;
			}
			category = Adminshop.getAdminshop().getCategory(args[1]);
			if (category == null) {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.notexists")
						.replaceAll("\\{CATEGORY\\}", args[1]));
				return true;
			}
			List<AdminshopCategory> categories = Adminshop.getAdminshop().getCategories();
			try {
				index = Integer.parseInt(args[2]);
			} catch (Exception e) {
				player.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
						.replaceAll("\\{NUMBER\\}", args[2]));
				return true;
			}
			if (categories.size() <= index || index < 0) {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.outofbounds"));
				return true;
			}
			categories.remove(category);
			categories.add(index, category);
			player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.move")
					.replaceAll("\\{CATEGORY\\}", category.getName())
					.replaceAll("\\{NUMBER\\}", Integer.toString(index)));
			Adminshop.getAdminshop().save();
			return true;

		case "listcategories":
			player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.listcategories"));
			for (AdminshopCategory adminshopCategory : Adminshop.getAdminshop().getCategories()) {
				player.sendMessage(ChatColor.GREEN + adminshopCategory.getName());
			}
			return true;

		case "additem":
			if (args.length < 3) {
				player.sendMessage(ChatColor.GOLD + "/adminshop additem category stückpreis");
				player.sendMessage(ChatColor.GOLD + "/adminshop additem category stückpreis position");
				return true;
			}
			if (item == null || item.getType() == Material.AIR) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noitem"));
				return true;
			}
			category = Adminshop.getAdminshop().getCategory(args[1]);
			if (category == null) {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.notexists")
						.replaceAll("\\{CATEGORY\\}", args[1]));
				return true;
			}
			ArrayList<AdminshopItem> items = category.getContents();
			index = items.size();
			double price;
			try {
				price = Double.parseDouble(args[2]);
			} catch (NumberFormatException e) {
				player.sendMessage(Messages.getMessages().getMessage("commands.general.notdouble")
						.replaceAll("\\{NUMBER\\}", args[2]));
				return true;
			}
			if (args.length > 3) {
				try {
					index = Integer.parseInt(args[3]);
				} catch (NumberFormatException e) {
					player.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
							.replaceAll("\\{NUMBER\\}", args[3]));
					return true;
				}
			}
			if (index < 0 || index > items.size()) {
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.outofbounds"));
				return true;
			}
			for (int i = 0; i < items.size(); i++) {
				AdminshopItem adminshopItem = items.get(i);
				if (adminshopItem.getItem().equals(item)) {
					items.remove(i);
					items.add(index, new AdminshopItem(price, item));
					player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.changeprice")
							.replaceAll("\\{CATEGORY\\}", category.getName())
							.replaceAll("\\{NUMBER\\}", Integer.toString(index)));
					Adminshop.getAdminshop().save();
					return true;
				}
			}
			items.add(index, new AdminshopItem(price, item));
			player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.additem")
					.replaceAll("\\{NUMBER\\}", Double.toString(price)));
			Adminshop.getAdminshop().save();
			return true;

		case "removeitem":
			if (item == null || item.getType() == Material.AIR) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noitem"));
				return true;
			}
			if (args.length == 1) {
				int found = 0;
				for (AdminshopCategory adminshopCategory : Adminshop.getAdminshop().getCategories()) {
					items = adminshopCategory.getContents();
					for (int i = items.size() - 1; i >= 0; i--) {
						if (items.get(i).getItem().equals(item)) {
							items.remove(i);
							found++;
						}
					}
				}
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.removeitem")
						.replaceAll("\\{NUMBER\\}", Integer.toString(found)));
				Adminshop.getAdminshop().save();
			} else if (args.length == 2) {
				category = Adminshop.getAdminshop().getCategory(args[1]);
				if (category == null) {
					player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.category.notexists")
							.replaceAll("\\{CATEGORY\\}", args[1]));
					return true;
				}
				int found = 0;
				items = category.getContents();
				for (int i = items.size() - 1; i >= 0; i++) {
					if (items.get(i).getItem().equals(item)) {
						items.remove(i);
						found++;
					}

				}
				player.sendMessage(Messages.getMessages().getMessage("commands.adminshop.removeitem")
						.replaceAll("\\{NUMBER\\}", Integer.toString(found)));
				Adminshop.getAdminshop().save();
			} else {
				player.sendMessage(ChatColor.GOLD + "/adminshop removeitem");
				player.sendMessage(ChatColor.GOLD + "/adminshop removeitem category");
			}
			return true;

		default:
			sendInfo(player);
			return true;
		}
	}

	private void sendInfo(Player player) {

		player.sendMessage("");
		player.sendMessage(ChatColor.GOLD + "Commands ohne Item in der Hand:");
		player.sendMessage(ChatColor.GOLD + "/adminshop reload");
		player.sendMessage(ChatColor.GOLD + "/adminshop addcategory name");
		player.sendMessage(ChatColor.GOLD + "/adminshop addcategory name index");
		player.sendMessage(ChatColor.GOLD + "/adminshop removecategory name");
		player.sendMessage(ChatColor.GOLD + "/adminshop renamecategory oldName newName");
		player.sendMessage(ChatColor.GOLD + "/adminshop movecategory name index");
		player.sendMessage(ChatColor.GOLD + "/adminshop listcategories");

		player.sendMessage("");
		player.sendMessage(ChatColor.GOLD + "Commands mit dem jeweiligen Item in der Hand:");
		player.sendMessage(ChatColor.GOLD + "/adminshop additem category stückpreis");
		player.sendMessage(ChatColor.GOLD + "/adminshop additem category stückpreis position");
		player.sendMessage(ChatColor.GOLD + "/adminshop removeitem");
		player.sendMessage(ChatColor.GOLD + "/adminshop removeitem category");
		player.sendMessage(ChatColor.GOLD + "/adminshop setcategoryitem category");

	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		ArrayList<String> list = new ArrayList<>();
		if (!sender.hasPermission("eden.edenutils.adminshop.edit"))
			return list;
		if (args.length == 1) {
			String arg = args[0].toLowerCase();
			if ("reload".startsWith(arg))
				list.add("reload");
			if ("addcategory".startsWith(arg))
				list.add("addcategory");
			if ("removecategory".startsWith(arg))
				list.add("removecategory");
			if ("renamecategory".startsWith(arg))
				list.add("renamecategory");
			if ("movecategory".startsWith(arg))
				list.add("movecategory");
			if ("listcategories".startsWith(arg))
				list.add("listcategories");
			if ("additem".startsWith(arg))
				list.add("additem");
			if ("removeitem".startsWith(arg))
				list.add("removeitem");
			if ("setcategoryitem".startsWith(arg))
				list.add("setcategoryitem");
		}
		return list;
	}

}
