package at.haha007.edenutils.adminshop;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import at.haha007.edenlib.utils.Utils;

public class AdminshopItem {
	private double price;
	private ItemStack item;
	private UUID uuid = UUID.randomUUID();

	public AdminshopItem(double _price, ItemStack _item) {
		item = _item;
		price = _price;
	}

	public AdminshopItem(ConfigurationSection cfg) {
		price = cfg.getDouble("price", 1000);
		item = cfg.getItemStack("item", new ItemStack(Material.GRASS_BLOCK));
	}

	public ItemStack getDisplayItem() {
		ItemStack item = new ItemStack(this.item);
		ItemMeta meta = item.getItemMeta();
		List<String> lore = meta.getLore();
		if (lore == null)
			lore = new ArrayList<>();

		lore.add(0, ChatColor.GOLD + "Preis pro Stück: " + ChatColor.AQUA + price);

		meta.setLore(lore);
		item.setItemMeta(meta);
		return Utils.setItemUUID(item, uuid);
	}

	public UUID getUuid() {
		return uuid;
	}

	public ItemStack getItem() {
		return new ItemStack(item);
	}

	public double getPrice() {
		return price;
	}

	public void save(ConfigurationSection cfg) {
		cfg.set("item", item);
		cfg.set("price", price);
	}
}
