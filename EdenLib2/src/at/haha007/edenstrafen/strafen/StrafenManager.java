package at.haha007.edenstrafen.strafen;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import at.haha007.edenlib.EdenLib;
import net.md_5.bungee.api.ChatColor;

public class StrafenManager {

	public static Inventory getStrafen(String player, int page) {
		Inventory inventory = Bukkit.createInventory(null, 54,
				ChatColor.GOLD + "Strafen von " + player + ". Seite " + page);

		YamlConfiguration cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig();
		ConfigurationSection section = cfg.getConfigurationSection("strafen");
		if (section == null)
			return inventory;
		Set<String> keySet = section.getKeys(false);
		ArrayList<Long> timeStamps = new ArrayList<>();
		for (String string : keySet) {
			try {
				timeStamps.add(Long.parseLong(string));
			} catch (NumberFormatException e) {
				System.err.println("Strafe " + string + " von Spieler " + player + " konnte nicht geladen werden.");
			}
		}
		timeStamps.sort(null);

		int index = page * 54;
		try {
			for (int i = 0; i < 54; i++) {
				InterfaceStrafe strafe = getStrafe(player, timeStamps.get(index));

				if (strafe != null) {
					ItemStack item = strafe.toItem();
					ItemMeta meta = item.getItemMeta();
					List<String> lore = meta.getLore();
					lore.add(ChatColor.DARK_GREEN + "ID: " + ChatColor.GREEN + index);
					meta.setLore(lore);
					item.setItemMeta(meta);
					inventory.setItem(i, item);
				}
				index++;
			}
		} catch (IndexOutOfBoundsException e) {
		}

		return inventory;
	}

	public static boolean deleteStrafe(String player, int id) {
		// returns false when id didn't exist

		YamlConfiguration cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig();
		Set<String> keySet = cfg.getConfigurationSection("strafen").getKeys(false);
		ArrayList<Long> timeStamps = new ArrayList<>();
		for (String string : keySet) {
			try {
				timeStamps.add(Long.parseLong(string));
			} catch (NumberFormatException e) {
				System.err.println("Strafe " + string + " von Spieler " + player + " konnte nicht geladen werden.");
			}
		}
		timeStamps.sort(null);

		try {
			long timeStamp = timeStamps.get(id);
			InterfaceStrafe strafe = getStrafe(player, timeStamp);
			if (strafe == null)
				return false;
			cfg.set("strafen." + timeStamp, null);

		} catch (IndexOutOfBoundsException e) {
			return false;
		}

		return true;
	}

	public static void addStrafe(String player, InterfaceStrafe strafe) {
		YamlConfiguration cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig();
		String key = "strafen." + strafe.getTimeStamp();
		cfg.set(key, strafe.saveToSection(cfg.createSection(key)));
	}

	public static ArrayList<InterfaceStrafe> getStrafen(String player) {
		YamlConfiguration cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig();
		Set<String> keySet = cfg.getConfigurationSection("strafen").getKeys(false);
		ArrayList<InterfaceStrafe> strafen = new ArrayList<>();
		for (String string : keySet) {
			try {
				strafen.add(getStrafe(player, Long.parseLong(string)));
			} catch (NumberFormatException e) {
				System.err.println("Strafe " + string + " von Spieler " + player + " konnte nicht geladen werden.");
			}
		}
		return strafen;
	}

	public static InterfaceStrafe getStrafe(String player, int id) {
		YamlConfiguration cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig();
		Set<String> keySet = cfg.getConfigurationSection("strafen").getKeys(false);
		ArrayList<Long> timeStamps = new ArrayList<>();
		for (String string : keySet) {
			try {
				timeStamps.add(Long.parseLong(string));
			} catch (NumberFormatException e) {
				System.err.println("Strafe " + string + " von Spieler " + player + " konnte nicht geladen werden.");
			}
		}
		timeStamps.sort(null);

		return getStrafe(player, timeStamps.get(id));
	}

	public static InterfaceStrafe getStrafe(String player, long timeStamp) {
		ConfigurationSection cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig()
				.getConfigurationSection("strafen." + Long.toString(timeStamp));
		String type = cfg.getString("type");
		InterfaceStrafe strafe;
		switch (type.toUpperCase()) {

		case "JAIL":
			strafe = new StrafeJail(timeStamp, cfg.getString("reason"), cfg.getString("from"), cfg.getInt("blocks"));
			break;

		case "MUTE":
			strafe = new StrafeMute(timeStamp, cfg.getString("reason"), cfg.getString("from"), cfg.getLong("time"));
			break;

		case "CUSTOM":
			strafe = new StrafeCustom(timeStamp, cfg.getString("reason"), cfg.getString("from"));
			break;

		case "KICK":
			strafe = new StrafeKick(timeStamp, cfg.getString("reason"), cfg.getString("from"));
			break;

		case "BAN":
			strafe = new StrafeBan(timeStamp, cfg.getString("reason"), cfg.getString("from"));
			break;

		case "TEMPBAN":
			strafe = new StrafeTempban(timeStamp, cfg.getString("reason"), cfg.getString("from"), cfg.getLong("time"));
			break;

		default:
			strafe = null;
			break;
		}

		return strafe;
	}
}
