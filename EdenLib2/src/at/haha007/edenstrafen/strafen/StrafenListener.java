package at.haha007.edenstrafen.strafen;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import at.haha007.edenlib.utils.Utils;
import net.md_5.bungee.api.ChatColor;

public class StrafenListener implements Listener {

	@EventHandler
	void onInventoryClickEvent(InventoryClickEvent event) {
		if (event.getClickedInventory() == null)
			return;

		if (event.getClickedInventory().getType() != InventoryType.CHEST)
			return;

		if (!event.getView().getTitle().startsWith(ChatColor.GOLD + "Strafen von "))
			return;

		event.setCancelled(true);
	}

	@EventHandler(ignoreCancelled = false)
	void onCommand(PlayerCommandPreprocessEvent event) {

		checkMute(event);

		if (event.isCancelled())
			return;

		checkKick(event);

		if (event.isCancelled())
			return;

		checkBan(event);

		if (event.isCancelled())
			return;

		checkTempan(event);
	}

	private void checkTempan(PlayerCommandPreprocessEvent event) {
		String cmd = event.getMessage();
		Player sender = event.getPlayer();
		String[] args = cmd.split(" ");

		if (!(args[0].equalsIgnoreCase("/tempban") || args[0].equalsIgnoreCase("/etempban")))
			return;

		if (!(sender.hasPermission("essentials.tempban.offline") || sender.hasPermission("essentials.tempban.exempt")))
			return;

		if (args.length < 4) {
			event.getPlayer().sendMessage(ChatColor.RED + "/tempban <player> <time> <reason>");
			event.setCancelled(true);
			return;
		}
		try {
			// arg 0 = command
			// arg 1 = player
			// arg 2 = time
			// arg 3+ = reason
			long time = Utils.parseDateDiff(args[2], true) - System.currentTimeMillis();
			String reason = "";
			for (int i = 3; i < args.length; i++) {
				reason += " " + args[i];
			}
			reason.replaceFirst(" ", "");

			StrafeTempban strafe = new StrafeTempban(System.currentTimeMillis(), reason, sender.getName(), time);

			StrafenManager.addStrafe(args[1], strafe);

			return;
		} catch (Exception e) {
			event.getPlayer().sendMessage(ChatColor.RED + "/tempban <player> <time> <reason>");
			event.setCancelled(true);
			return;
		}

	}

	private void checkBan(PlayerCommandPreprocessEvent event) {
		String cmd = event.getMessage();
		Player sender = event.getPlayer();
		String[] args = cmd.split(" ");

		if (!(args[0].equalsIgnoreCase("/ban") || args[0].equalsIgnoreCase("/eban")))
			return;

		if (!(sender.hasPermission("essentials.ban.offline") || sender.hasPermission("essentials.ban.exempt")))
			return;

		if (args.length < 3) {
			event.getPlayer().sendMessage(ChatColor.RED + "/ban <player> <reason>");
			event.setCancelled(true);
			return;
		}

		String reason = "";
		for (int i = 2; i < args.length; i++) {
			reason += " " + args[i];
		}
		reason.replaceFirst(" ", "");

		StrafeBan strafe = new StrafeBan(System.currentTimeMillis(), reason, sender.getName());

		StrafenManager.addStrafe(args[1], strafe);

	}

	private void checkKick(PlayerCommandPreprocessEvent event) {
		String cmd = event.getMessage();
		Player sender = event.getPlayer();
		String[] args = cmd.split(" ");

		if (!(args[0].equalsIgnoreCase("/kick") || args[0].equalsIgnoreCase("/ekick")))
			return;

		if (!sender.hasPermission("essentials.kick.exempt"))
			return;

		if (args.length < 3) {
			event.getPlayer().sendMessage(ChatColor.RED + "/kick <player> <reason>");
			event.setCancelled(true);
			return;
		}

		Player player = Bukkit.getPlayerExact(args[1]);

		if (player == null || !player.isOnline()) {
			event.getPlayer().sendMessage(ChatColor.RED + "Player not found.");
			event.setCancelled(true);
			return;
		}

		String reason = "";
		for (int i = 2; i < args.length; i++) {
			reason += " " + args[i];
		}
		reason.replaceFirst(" ", "");

		StrafeKick strafe = new StrafeKick(System.currentTimeMillis(), reason, sender.getName());

		StrafenManager.addStrafe(args[1], strafe);

	}

	private void checkMute(PlayerCommandPreprocessEvent event) {
		String cmd = event.getMessage();
		Player sender = event.getPlayer();
		String[] args = cmd.split(" ");

		if (!(args[0].equalsIgnoreCase("/mute") || args[0].equalsIgnoreCase("/emute")
				|| args[0].equalsIgnoreCase("/silence") || args[0].equalsIgnoreCase("/esilence")))
			return;

		if (!(sender.hasPermission("essentials.mute.offline") || sender.hasPermission("essentials.mute.exempt")))
			return;

		if (args.length < 4) {
			event.getPlayer().sendMessage(ChatColor.RED + "/mute <player> <time> <reason>");
			event.setCancelled(true);
			return;
		}
		try {
			// arg 0 = command
			// arg 1 = player
			// arg 2 = time
			// arg 3+ = reason
			long time = Utils.parseDateDiff(args[2], true) - System.currentTimeMillis();
			String reason = "";
			for (int i = 3; i < args.length; i++) {
				reason += " " + args[i];
			}
			reason.replaceFirst(" ", "");

			StrafeMute strafe = new StrafeMute(System.currentTimeMillis(), reason, sender.getName(), time);

			StrafenManager.addStrafe(args[1], strafe);

			event.setMessage(args[0] + " " + args[1] + " " + args[2]);
			return;
		} catch (Exception e) {
			event.getPlayer().sendMessage(ChatColor.RED + "/mute <player> <time> <reason>");
			event.setCancelled(true);
			return;
		}
	}
}
