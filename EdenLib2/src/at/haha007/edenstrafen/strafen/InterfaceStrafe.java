package at.haha007.edenstrafen.strafen;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

public interface InterfaceStrafe {
	public ItemStack toItem();
	
	public String getKey();

	public long getTimeStamp();

	public String getReason();

	public ConfigurationSection saveToSection(ConfigurationSection cfg);

}
