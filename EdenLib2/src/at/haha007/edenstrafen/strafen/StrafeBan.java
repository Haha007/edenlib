package at.haha007.edenstrafen.strafen;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import at.haha007.edenlib.utils.Utils;

public class StrafeBan implements InterfaceStrafe {

	private long timeStamp;
	private String from;
	private String reason;

	public StrafeBan(long timeStamp, String reason, String from) {
		this.timeStamp = timeStamp;
		this.reason = reason;
		this.from = from;
	}

	@Override
	public ItemStack toItem() {
		ItemStack itemStack = new ItemStack(Material.BARRIER);
		ItemMeta itemMeta = itemStack.getItemMeta();

		String date = new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date(timeStamp));
		itemMeta.setDisplayName(ChatColor.GREEN + date);

		List<String> reason = Utils.toLines(this.reason, 15);
		reason.set(0, ChatColor.YELLOW + "Grund: " + ChatColor.AQUA + reason.get(0));
		for (int i = 1; i < reason.size(); i++) {
			reason.set(i, "       " + ChatColor.AQUA + reason.get(i));
		}

		ArrayList<String> lore = new ArrayList<>();
		lore.add(ChatColor.YELLOW + "Art: " + ChatColor.GOLD + " Ban");
		lore.addAll(reason);
		lore.add(ChatColor.YELLOW + "Gesetzt von: " + ChatColor.AQUA + from);
		itemMeta.setLore(lore);
		itemStack.setItemMeta(itemMeta);

		return itemStack;
	}

	@Override
	public long getTimeStamp() {
		return timeStamp;
	}

	@Override
	public String getReason() {
		return reason;
	}

	@Override
	public ConfigurationSection saveToSection(ConfigurationSection cfg) {
		cfg.set("type", getKey());
		cfg.set("from", from);
		cfg.set("reason", reason);
		return cfg;
	}

	@Override
	public String getKey() {
		return "BAN";
	}
}
