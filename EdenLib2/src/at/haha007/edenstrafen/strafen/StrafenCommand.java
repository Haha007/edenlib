package at.haha007.edenstrafen.strafen;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Messages;

public class StrafenCommand implements CommandExecutor, TabCompleter {
	// strafen add player reason
	// strafen remove player id
	// strafen player
	// strafen show player page

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.notplayer"));
			return true;
		}
		Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, new Runnable() {

			@Override
			public void run() {
				Player player = (Player) sender;
//				if (args.length == 1) {
//					if (!sender.hasPermission("eden.edenstrafen.strafen.show")) {
//						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
//						return;
//					}
//					showInfo(sender);
//					return;
//				}
				if (args.length == 1) {
					if (!sender.hasPermission("eden.edenstrafen.strafen.show")) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
						return;
					}

					Inventory inv = StrafenManager.getStrafen(args[0], 0);
					player.openInventory(inv);
					return;
				}

				if (args[0].toLowerCase().equals("show")) {
					if (!sender.hasPermission("eden.edenstrafen.strafen.show")) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
						return;
					}
					int page;
					if (args.length == 2) {
						page = 1;
					} else {
						try {
							page = Integer.parseInt(args[2]);
						} catch (NumberFormatException e) {
							player.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
									.replaceAll("\\{NUMBER\\}", args[2]));
							return;
						}
					}
					page--;
					if (page < 0)
						page = 0;

					Inventory inv = StrafenManager.getStrafen(args[1], page);
					player.openInventory(inv);

					return;
				}

				if (args[0].toLowerCase().equals("remove")) {
					if (!sender.hasPermission("eden.edenstrafen.strafen.remove")) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
						return;
					}
					int id;
					if (args.length == 3) {
						id = 1;
					} else {
						try {
							id = Integer.parseInt(args[1]);
						} catch (NumberFormatException e) {
							player.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
									.replaceAll("\\{NUMBER\\}", args[2]));
							return;
						}
					}
					id--;

					if (StrafenManager.deleteStrafe(args[1], id)) {
						player.sendMessage(Messages.getMessages().getMessage("commands.strafen.remove")
								.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{NUMBER\\}", Integer.toString(id)));
					} else {
						player.sendMessage(Messages.getMessages().getMessage("commands.strafen.notfound")
								.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{NUMBER\\}", Integer.toString(id)));
					}
					return;
				}

				if (args[0].toLowerCase().equals("add")) {
					if (!sender.hasPermission("eden.edenstrafen.strafen.add")) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
						return;
					}
					if (args.length < 3) {
						sender.sendMessage(ChatColor.RED + "/strafen add <player> <reason>");
						return;
					}

					String reason = "";
					for (int i = 2; i < args.length; i++) {
						reason += " " + args[i];
					}
					reason.replaceFirst(" ", "");

					StrafenManager.addStrafe(args[1],
							new StrafeCustom(System.currentTimeMillis(), reason, player.getName()));

					player.sendMessage(Messages.getMessages().getMessage("commands.strafen.remove")
							.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{STRAFE\\}", reason));
					return;
				}

				showInfo(sender);
			}
		});
		return true;
	}

	private void showInfo(CommandSender sender) {
		sender.sendMessage(ChatColor.GOLD + "/strafen <player>");
		sender.sendMessage(ChatColor.GOLD + "/strafen show <player> <page>");
		sender.sendMessage(ChatColor.GOLD + "/strafen remove <player> <id>");
		sender.sendMessage(ChatColor.GOLD + "/strafen add <player> <reason>");
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> returns = new ArrayList<>();
		for (int i = 0; i < args.length; i++) {
			args[i] = args[i].toLowerCase();
		}
		if (args.length == 1) {
			String arg = args[0].toLowerCase();
			if ("add".startsWith(arg))
				returns.add("add");
			if ("remove".startsWith(arg))
				returns.add("remove");
			if ("show".startsWith(arg))
				returns.add("show");

			for (Player player : Bukkit.getOnlinePlayers()) {
				String name = player.getName();
				if (name.toLowerCase().startsWith(arg))
					returns.add(name);
			}
		} else if (args.length == 2) {
			String arg = args[0].toLowerCase();
			if (!(arg.equals("add") || arg.equals("remove") || arg.equals("show")))
				return returns;

			for (Player player : Bukkit.getOnlinePlayers()) {
				String name = player.getName();
				if (name.toLowerCase().startsWith(args[1]))
					returns.add(name);
			}
		}
		return returns;
	}

}
