package at.haha007.edenstrafen.jail;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Utils;
import net.md_5.bungee.api.ChatColor;

public class JailListener implements Listener {

	private JailManager jailManager = EdenLib.instance.getJailManager();

	@EventHandler(priority = EventPriority.MONITOR)
	void onBlockBreak(BlockBreakEvent event) {
		if (!jailManager.isPlayerInJail(event.getPlayer().getName())) {
			if (event.getPlayer().hasPermission("eden.EdenLib.jail.edit"))
				return;
			if (jailManager.getJailArea().isPointInArea(event.getBlock().getLocation()))
				event.setCancelled(true);
			return;
		}
		if (event.isCancelled()) {
			String name = event.getPlayer().getName();
			jailManager.setBlocksInJail(name, jailManager.getBlocksInJail(name) + 1);
			return;
		}
		if (!(event.getBlock().getType() == Material.COBBLESTONE)) {
			event.setCancelled(true);
			return;
		}
		if (!jailManager.getJailArea().isPointInArea(event.getBlock().getLocation())) {
			event.setCancelled(true);
			return;
		}

		jailManager.blockMined(event.getPlayer(), event.getBlock());
		event.setDropItems(false);
	}

	@EventHandler
	private void onInventoryClick(InventoryClickEvent event) {
		if (jailManager.isPlayerInJail(event.getWhoClicked().getName()))
			event.setCancelled(true);

		if (event.getView().getTopInventory() == event.getClickedInventory()
				&& event.getView().getTitle().startsWith(jailManager.getJailInventoryPrefix())) {
			event.setCancelled(true);
			String player = event.getView().getTitle().replaceFirst(jailManager.getJailInventoryPrefix(), "");
			int amount;
			try {
				amount = jailManager.getBlocks()[event.getSlot()];
			} catch (IndexOutOfBoundsException e) {
				return;
			}

			String command = "edenjail set " + player + " " + amount + " Griefing "
					+ event.getWhoClicked().getWorld().getName();

			Bukkit.dispatchCommand(event.getWhoClicked(), command);
		}
	}

	@EventHandler
	private void onInventoryInteract(InventoryInteractEvent event) {
		if (jailManager.isPlayerInJail(event.getWhoClicked().getName()))
			event.setCancelled(true);

	}

	@EventHandler(priority = EventPriority.MONITOR)
	void onBlockPlace(BlockPlaceEvent event) {
		if (event.getPlayer().hasPermission("eden.EdenLib.jail.edit"))
			return;
		if (!jailManager.getJailArea().isPointInArea(event.getBlock().getLocation()))
			return;
		event.setCancelled(true);
	}

	@EventHandler
	void onPlayerMove(PlayerMoveEvent event) {
		if (!jailManager.isPlayerInJail(event.getPlayer().getName()))
			return;
		if (jailManager.getJailArea().isPointInArea(event.getTo()))
			return;
		Player player = event.getPlayer();
		player.teleport(jailManager.getJailSpawn());
		player.setGameMode(GameMode.SURVIVAL);
		player.getInventory().clear();
		player.getInventory()
				.setItemInMainHand(Utils.getItem(Material.STONE_PICKAXE, ChatColor.GOLD + "Jail Spitzhacke"));
	}

	@EventHandler
	void onPlayerTeleport(PlayerTeleportEvent event) {
		if (!jailManager.isPlayerInJail(event.getPlayer().getName()))
			return;
		if (jailManager.getJailArea().isPointInArea(event.getTo()))
			return;
		event.setCancelled(true);
		Player player = event.getPlayer();
		player.teleport(jailManager.getJailSpawn());
		player.setGameMode(GameMode.SURVIVAL);
		player.getInventory().clear();
		player.getInventory()
				.setItemInMainHand(Utils.getItem(Material.STONE_PICKAXE, ChatColor.GOLD + "Jail Spitzhacke"));
	}

	@EventHandler
	void onPlayerLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, () -> {
			if (jailManager.isPlayerInJail(player.getName())) {
				player.teleport(jailManager.getJailSpawn());
				player.setGameMode(GameMode.SURVIVAL);
				player.getInventory().clear();
				player.getInventory()
						.setItemInMainHand(Utils.getItem(Material.STONE_PICKAXE, ChatColor.GOLD + "Jail Spitzhacke"));
			}
		});
	}

	@EventHandler
	void onGameModeChange(PlayerGameModeChangeEvent event) {
		Player player = event.getPlayer();
		if (event.getNewGameMode() == GameMode.SURVIVAL)
			return;
		if (jailManager.isPlayerInJail(player.getName())) {
			player.teleport(jailManager.getJailSpawn());
			player.setGameMode(GameMode.SURVIVAL);
			player.getInventory().clear();
			player.getInventory()
					.setItemInMainHand(Utils.getItem(Material.STONE_PICKAXE, ChatColor.GOLD + "Jail Spitzhacke"));
		}
	}

}
