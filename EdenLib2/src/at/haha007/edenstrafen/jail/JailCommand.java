package at.haha007.edenstrafen.jail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.boydti.fawe.FaweAPI;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.commandexecutos.CE_EdenLib;
import at.haha007.edenlib.utils.EdenArea;
import at.haha007.edenlib.utils.Messages;
import at.haha007.edenstrafen.strafen.StrafeJail;
import at.haha007.edenstrafen.strafen.StrafenManager;

public class JailCommand implements CommandExecutor, TabCompleter {
	// set spawn
	// set area
	// (check if spawn is in area)

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0)
			return false;
		if (!(sender instanceof Player)) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.notplayer"));
			return true;
		}
		switch (args[0].toLowerCase()) {

		case "reload":
			if (sender.hasPermission("eden.EdenLib.jail.reload")) {
				EdenLib.instance.getJailManager().reload();
				sender.sendMessage(Messages.getMessages().getMessage("commands.jail.reload"));
			} else {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			}
			break;

		case "add":
			if (!sender.hasPermission("eden.EdenLib.jail.add")) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
				return true;
			}
			if (args.length < 4) {
				sender.sendMessage(ChatColor.GOLD + "/" + label + " add <player> <amount> <reason>");
				return true;
			}
			try {
				int amount = Integer.parseInt(args[2]);
				if (amount <= 0) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.general.numbertoosmall")
							.replaceAll("\\{NUMBER\\}", Integer.toString(amount)).replaceAll("\\{MINIMUM\\}", "0"));
					return true;
				}
				String reason = "";
				for (int i = 3; i < args.length; i++) {
					reason += " " + args[i];
				}
				reason.replaceFirst(" ", "");
				StrafenManager.addStrafe(args[1],
						new StrafeJail(System.currentTimeMillis(), reason, sender.getName(), amount));
				JailManager manager = EdenLib.instance.getJailManager();
				amount += manager.getBlocksInJail(args[1]);
				manager.setBlocksInJail(args[1], amount);
				Bukkit.broadcast(Messages.getMessages().getMessage("commands.jail.set")
						.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{NUMBER\\}", args[1]),
						"eden.EdenLib.jail.info");
			} catch (NumberFormatException e) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
						.replaceAll("\\{NUMBER\\}", args[2]));
			}
			break;

		case "remove":
			if (!sender.hasPermission("eden.EdenLib.jail.remove")) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
				return true;
			}
			if (args.length != 3) {
				sender.sendMessage(ChatColor.GOLD + "/" + label + " remove <player> <amount>");
				return true;
			}
			try {
				int amount = Integer.parseInt(args[2]);
				JailManager manager = EdenLib.instance.getJailManager();
				amount = manager.getBlocksInJail(args[1]) - amount;
				manager.setBlocksInJail(args[1], amount);
				Bukkit.broadcast(Messages.getMessages().getMessage("commands.jail.set")
						.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{NUMBER\\}", args[1]),
						"eden.EdenLib.jail.info");
			} catch (NumberFormatException e) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
						.replaceAll("\\{NUMBER\\}", args[2]));
			}
			break;

		case "release":
			if (!sender.hasPermission("eden.EdenLib.jail.release")) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
				return true;
			}
			if (args.length != 2) {
				sender.sendMessage(ChatColor.GOLD + "/" + label + " release");
				break;
			} else {
				JailManager manager = EdenLib.instance.getJailManager();
				manager.setBlocksInJail(args[1], 0);
			}
			break;

		case "set":
			if (!sender.hasPermission("eden.EdenLib.jail.set")) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
				return true;
			}
			if (args.length < 4) {
				sender.sendMessage(ChatColor.GOLD + "/" + label + " set <player> <amount> <reason>");
				return true;
			}
			try {
				int amount = Integer.parseInt(args[2]);
				JailManager manager = EdenLib.instance.getJailManager();
				String reason = "";
				for (int i = 3; i < args.length; i++) {
					reason += " " + args[i];
				}
				reason.replaceFirst(" ", "");
				StrafenManager.addStrafe(args[1],
						new StrafeJail(System.currentTimeMillis(), reason, sender.getName(), amount));
				manager.setBlocksInJail(args[1], amount);
				Bukkit.broadcast(Messages.getMessages().getMessage("commands.jail.set")
						.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{NUMBER\\}", args[2]),
						"eden.EdenLib.jail.info");
			} catch (NumberFormatException e) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
						.replaceAll("\\{NUMBER\\}", args[2]));
			}
			break;

		case "inventory":
			if (!sender.hasPermission("eden.EdenLib.jail.set")) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
				return true;
			}
			if (args.length < 2) {
				sender.sendMessage(ChatColor.GOLD + "/" + label + " inventory <player>");
				return true;
			}
			JailManager manager = EdenLib.instance.getJailManager();
			manager.openJailInventory((Player) sender, args[1]);
			break;

		case "redefine":
			if (!sender.hasPermission("eden.EdenLib.jail.redefine")) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
				return true;
			}
			if (args.length != 1) {
				sender.sendMessage(ChatColor.GOLD + "/" + label + " redefine");
				return true;
			}
			Location loc = ((Player) sender).getLocation();
			Region selection = FaweAPI.wrapPlayer(sender).getSelection();
			if (selection == null) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noselection"));
				return true;
			}
			BlockVector3 min = selection.getMinimumPoint();
			BlockVector3 max = selection.getMaximumPoint();
			EdenArea area = new EdenArea(new Vector(min.getX(), min.getY(), min.getZ()),
					new Vector(max.getX(), max.getY(), max.getZ()), loc.getWorld());
			if (!area.isPointInArea(loc)) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.jail.redefine.outside"));
				return true;
			}

			CE_EdenLib cmdEdenLib = EdenLib.instance.getCE_EdenLib();
			YamlConfiguration cfg = cmdEdenLib.getConfig();
			cfg.set("jail.spawn.x", loc.getX());
			cfg.set("jail.spawn.y", loc.getY());
			cfg.set("jail.spawn.z", loc.getZ());
			cfg.set("jail.spawn.yaw", loc.getYaw());
			cfg.set("jail.spawn.pitch", loc.getPitch());

			cfg.set("jail.aabb.pos1.x", area.getMinPoint().getX());
			cfg.set("jail.aabb.pos1.y", area.getMinPoint().getY());
			cfg.set("jail.aabb.pos1.z", area.getMinPoint().getZ());
			cfg.set("jail.aabb.pos2.x", area.getMaxPoint().getX());
			cfg.set("jail.aabb.pos2.y", area.getMaxPoint().getY());
			cfg.set("jail.aabb.pos2.z", area.getMaxPoint().getZ());

			try {
				cfg.save(cmdEdenLib.getFile());
			} catch (IOException e) {
				e.printStackTrace();
			}
			EdenLib.instance.getJailManager().reload();
			sender.sendMessage(Messages.getMessages().getMessage("commands.jail.redefine"));
			break;

		case "setspawn":
			if (!sender.hasPermission("eden.EdenLib.jail.setspawn")) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
				return true;
			}
			if (args.length != 1) {
				sender.sendMessage(ChatColor.GOLD + "/" + label + " setspawn");
				return true;
			}
			loc = ((Player) sender).getLocation();
			cmdEdenLib = EdenLib.instance.getCE_EdenLib();
			cfg = cmdEdenLib.getConfig();

			cfg.set("jail.release.world", loc.getWorld().getName());
			cfg.set("jail.release.x", loc.getX());
			cfg.set("jail.release.y", loc.getY());
			cfg.set("jail.release.z", loc.getZ());
			cfg.set("jail.release.yaw", loc.getYaw());
			cfg.set("jail.release.pitch", loc.getPitch());

			try {
				cfg.save(cmdEdenLib.getFile());
			} catch (IOException e) {
				e.printStackTrace();
			}
			EdenLib.instance.getJailManager().reload();
			sender.sendMessage(Messages.getMessages().getMessage("commands.jail.setspawn"));
			break;

		case "info":
		case "i":
			if (!sender.hasPermission("eden.EdenLib.jail.info")) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
				return true;
			}
			if (args.length != 2) {
				sender.sendMessage(ChatColor.GOLD + "/" + label + " info <player>");
				break;
			}

			sender.sendMessage(Messages.getMessages().getMessage("commands.jail.info")
					.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{NUMBER\\}",
							Integer.toString(EdenLib.instance.getJailManager().getBlocksInJail(args[1]))));
			break;

		default:
			return false;
		}

		return true;

	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> returns = new ArrayList<>();
		if (args.length == 1) {
			String arg = args[0].toLowerCase();
			if ("add".startsWith(arg))
				returns.add("add");
			if ("remove".startsWith(arg))
				returns.add("remove");
			if ("reload".startsWith(arg))
				returns.add("reload");
			if ("release".startsWith(arg))
				returns.add("release");
			if ("set".startsWith(arg))
				returns.add("set");
			if ("info".startsWith(arg))
				returns.add("info");
			if ("inventory".startsWith(arg))
				returns.add("inventory");
			if ("redefine".startsWith(arg))
				returns.add("redefine");
			if ("setspawn".startsWith(arg))
				returns.add("setspawn");
		} else if (args.length == 2) {
			String arg = args[1].toLowerCase();
			if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("release")
					|| args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("set")
					|| args[0].equalsIgnoreCase("info")) {
				for (Player player : Bukkit.getOnlinePlayers()) {
					String name = player.getName();
					if (name.startsWith(arg))
						returns.add(name);
				}
			}

		}
		return returns;
	}

}
