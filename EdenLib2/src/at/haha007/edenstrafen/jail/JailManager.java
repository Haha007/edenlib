package at.haha007.edenstrafen.jail;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.edenplayer.EdenPlayerManager;
import at.haha007.edenlib.utils.EdenArea;
import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.Utils;
import at.haha007.edenstrafen.strafen.InterfaceStrafe;
import at.haha007.edenstrafen.strafen.StrafeJail;
import at.haha007.edenstrafen.strafen.StrafenManager;
import net.md_5.bungee.api.ChatColor;

public class JailManager {
	private EdenArea jailArea;
	private Location jailSpawn;
	private Location releaseLocation;
	private EdenPlayerManager playerManager;
	private int cobbleRespawn;
	private final String jailInventoryPrefix = ChatColor.GREEN + "Jail Player " + ChatColor.GOLD;
	private final int[] blocks = new int[] { 0, 100, 150, 200, 300, 400, 500, 750, 1000, 1500, 2000, 2500, 3000, 3500,
			4000, 5000, 7500, 10000 };
	private HashSet<Block> blockSet = new HashSet<>();

	public JailManager() {
		reload();
	}

	public void jailInventory(Player player) {
		PlayerInventory inv = player.getInventory();

		inv.clear();
		ItemStack jailPickaxe = new ItemStack(Material.STONE_PICKAXE);
		ItemMeta meta = jailPickaxe.getItemMeta();
		meta.setDisplayName("");
		jailPickaxe.setItemMeta(meta);

	}

	public void reload() {
		File folder = EdenLib.instance.getDataFolder();
		if (!folder.exists())
			folder.mkdirs();

		YamlConfiguration cfg = EdenLib.instance.getCE_EdenLib().getConfig();
		playerManager = EdenLib.instance.getEdenPlayerManager();

		cobbleRespawn = cfg.getInt("jail.cobble_respawn");
		if (cobbleRespawn < 1)
			cobbleRespawn = 1;

		String jailWorld = cfg.getString("jail.world");
		if (jailWorld == null || jailWorld == "")
			jailWorld = Bukkit.getWorlds().get(0).getName();

		World respawnWorld = Bukkit.getWorld(cfg.getString("jail.release.world"));
		if (respawnWorld == null)
			respawnWorld = Bukkit.getWorlds().get(0);
		releaseLocation = new Location(respawnWorld, cfg.getDouble("jail.release.x"), cfg.getDouble("jail.release.y"),
				cfg.getDouble("jail.release.z"), (float) cfg.getDouble("jail.release.yaw"),
				(float) cfg.getDouble("jail.release.pitch"));

		jailArea = new EdenArea(
				new Vector(cfg.getDouble("jail.aabb.pos1.x"), cfg.getDouble("jail.aabb.pos1.y"),
						cfg.getDouble("jail.aabb.pos1.z")),
				new Vector(cfg.getDouble("jail.aabb.pos2.x"), cfg.getDouble("jail.aabb.pos2.y"),
						cfg.getDouble("jail.aabb.pos2.z")),
				Bukkit.getWorld(jailWorld));

		jailSpawn = new Location(jailArea.getWorld(), cfg.getDouble("jail.spawn.x"), cfg.getDouble("jail.spawn.y"),
				cfg.getDouble("jail.spawn.z"), (float) cfg.getDouble("jail.spawn.yaw"),
				(float) cfg.getDouble("jail.spawn.pitch"));

		if (!jailArea.isPointInArea(jailSpawn)) {
			Vector vec = jailArea.getMinPoint().clone().add(jailArea.getMaxPoint()).multiply(.5);
			jailSpawn.set(vec.getX(), vec.getY(), vec.getZ());
		}
	}

	public void onDisable() {
		for (Block block : blockSet) {
			block.setType(Material.COBBLESTONE);
		}
		blockSet.clear();
	}

	public boolean isPlayerInJail(String player) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		return cfg.getInt("jail") > 0;
	}

	public int getBlocksInJail(String player) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		return cfg.getInt("jail");
	}

	public void setBlocksInJail(String player, int blocks) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		cfg.set("jail", blocks > 0 ? blocks : 0);
		if (blocks == 0) {
			Player pl = Bukkit.getPlayerExact(player);
			if (pl != null)
				pl.teleport(releaseLocation);
			return;
		}

		for (Player _player : Bukkit.getOnlinePlayers()) {
			if (_player.getName().equalsIgnoreCase(player)) {
				_player.teleport(jailSpawn);
				_player.setGameMode(GameMode.SURVIVAL);
				_player.getInventory().clear();
				_player.getInventory()
						.setItemInMainHand(Utils.getItem(Material.STONE_PICKAXE, ChatColor.GOLD + "Jail Spitzhacke"));
			}
		}
	}

	public void blockMined(Player player, Block block) {
		YamlConfiguration cfg = playerManager.getEdenPlayer(player).getConfig();
		int blocks = cfg.getInt("jail") - 1;
		cfg.set("jail", blocks);
		if (blocks <= 0) {
			player.teleport(releaseLocation);
			return;
		}

		player.sendTitle("", Messages.getMessages().getMessage("jail.mine.subtitle").replaceAll("\\{NUMBER\\}",
				Integer.toString(blocks)), 10, 50, 10);
		player.getInventory().clear();
		player.getInventory()
				.setItemInMainHand(Utils.getItem(Material.STONE_PICKAXE, ChatColor.GOLD + "Jail Spitzhacke"));

		ArrayList<InterfaceStrafe> strafen = StrafenManager.getStrafen(player.getName());
		String reason = null;
		for (int i = strafen.size() - 1; i >= 0; i++) {
			if (strafen.get(i) instanceof StrafeJail) {
				reason = strafen.get(i).getReason();
				break;
			}
		}

		if (reason != null)
			player.sendActionBar(
					Messages.getMessages().getMessage("jail.mine.actionbar").replaceAll("\\{REASON\\}", reason));

		blockSet.add(block);
		Bukkit.getScheduler().scheduleSyncDelayedTask(EdenLib.instance, () -> {
			block.setType(Material.COBBLESTONE);
			blockSet.remove(block);
		}, cobbleRespawn);
	}

	public EdenArea getJailArea() {
		return jailArea;
	}

	public Location getJailSpawn() {
		return jailSpawn;
	}

	public String getJailInventoryPrefix() {
		return jailInventoryPrefix;
	}

	public void openJailInventory(Player player, String jail) {
		Inventory inv = Bukkit.createInventory(null, 18, getJailInventoryPrefix() + jail);
		for (int block : blocks) {
			ItemStack item = Utils.getItem(Material.COBBLESTONE, ChatColor.GOLD.toString() + block);
			inv.addItem(item);
		}
		player.openInventory(inv);
	}

	public int[] getBlocks() {
		return blocks;
	}
}
