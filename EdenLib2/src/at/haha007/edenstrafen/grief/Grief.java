package at.haha007.edenstrafen.grief;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import at.haha007.edenlib.utils.Utils;

public class Grief {

	private UUID uuid;
	private String reason;
	private String from;
	private Location location;
	private long created;

	public Grief(String reason, String from, Location location) {
		this.location = location;
		this.reason = reason;
		this.from = from;
		this.created = System.currentTimeMillis();
		this.uuid = UUID.randomUUID();
	}

	public Grief(ConfigurationSection cfg) {
		uuid = UUID.randomUUID();
		reason = cfg.getString("reason");
		from = cfg.getString("from");
		created = cfg.getLong("time");
		location = new Location(Bukkit.getWorld(cfg.getString("world")), cfg.getDouble("x"), cfg.getDouble("y"),
				cfg.getDouble("z"), (float) cfg.getDouble("yaw"), (float) cfg.getDouble("pitch"));
	}

	public ConfigurationSection save(ConfigurationSection cfg) {
		cfg.set("reason", reason);
		cfg.set("from", from);
		cfg.set("time", created);
		cfg.set("x", location.getX());
		cfg.set("y", location.getY());
		cfg.set("z", location.getZ());
		cfg.set("pitch", location.getPitch());
		cfg.set("yaw", location.getYaw());
		cfg.set("world", location.getWorld().getName());
		return cfg;
	}

	public ItemStack getItem() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemMeta meta = item.getItemMeta();

		meta.setDisplayName(ChatColor.GREEN + Utils.formatDate(created));

		ArrayList<String> lore = new ArrayList<>();
		lore.add(ChatColor.GOLD + "Von Spieler: " + ChatColor.AQUA + from);
		lore.add(ChatColor.GOLD + "Position: " + ChatColor.AQUA + location.getWorld().getName() + " "
				+ location.getBlockX() + " " + location.getBlockY() + " " + location.getBlockZ());
		lore.add(ChatColor.GOLD + "Angegebener Grund:");
		List<String> reason = Utils.toLines(this.reason, 20);
		for (String string : reason) {
			lore.add(ChatColor.AQUA + "   " + string);
		}
		meta.setLore(lore);
		item.setItemMeta(meta);

		return Utils.setItemUUID(item, uuid);
	}

	public String getFrom() {
		return from;
	}

	public Location getLocation() {
		return location;
	}

	public String getReason() {
		return reason;
	}

	public UUID getUuid() {
		return uuid;
	}

	public long getCreated() {
		return created;
	}
}
