package at.haha007.edenstrafen.grief;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.Utils;

public class GriefCommand implements CommandExecutor, TabCompleter, Listener {
	private File file;
	private ArrayList<Grief> griefs = new ArrayList<>();
	private String inventoryName = ChatColor.GREEN + "- Griefs -";
	private double dist2 = 1200;

	public GriefCommand() {
		file = new File(EdenLib.instance.getDataFolder(), "griefs.yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// load griefs
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		for (String key : cfg.getKeys(false)) {
			griefs.add(new Grief(cfg.getConfigurationSection(key)));
		}
	}

	private void save() {
		Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, new Runnable() {
			@Override
			public void run() {
				YamlConfiguration cfg = new YamlConfiguration();
				for (Grief grief : griefs) {
					grief.save(cfg.createSection(grief.getUuid().toString()));
				}
				try {
					cfg.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.notplayer"));
			return true;
		}
		Player player = (Player) sender;
		if (args.length == 0) {
			if (player.hasPermission("eden.edenstrafen.grief.show")) {
				showGriefs(player, 1);
			} else {
				player.sendMessage(ChatColor.RED + "/grief <grund>");
			}
			return true;
		}

		if (!sender.hasPermission("eden.edenstrafen.grief.show")) {
			addGrief(player, args, 0);
			return true;
		} else if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("list")) {
				int page = 1;
				if (args.length > 1) {
					try {
						page = Integer.parseInt(args[1]);
					} catch (NumberFormatException e) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
								.replaceAll("\\{NUMBER\\}", args[1]));
						return true;
					}
				}
				showGriefs(player, page);
				return true;
			} else if (args.length > 1 && args[0].equalsIgnoreCase("create")) {
				addGrief(player, args, 1);
				return true;
			}
		}

		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> returns = new ArrayList<>();
		if (args.length == 1) {
			String arg = args[0].toLowerCase();
			if (sender.hasPermission("eden.edenstrafen.grief.show")) {
				if ("create".startsWith(arg))
					returns.add("create");
				if ("list".startsWith(arg))
					returns.add("list");
			}
		}
		return returns;
	}

	@EventHandler
	void onInventoryClick(InventoryClickEvent event) {
		if (!event.getView().getTitle().equals(inventoryName))
			return;
		event.setCancelled(true);
		if (event.getClickedInventory() != event.getView().getTopInventory())
			return;
		Grief grief = getGrief(Utils.getItemUUID(event.getCurrentItem()));
		if (grief == null)
			return;
		if (event.getClick() == ClickType.RIGHT && event.getWhoClicked().hasPermission("eden.edenstrafen.grief.remove"))
			removeGrief(grief);
		else
			event.getWhoClicked().teleport(grief.getLocation());
	}

	@EventHandler
	void onPlayerJoin(PlayerJoinEvent event) {
		if (!event.getPlayer().hasPermission("eden.edenstrafen.grief.message"))
			return;
		event.getPlayer().sendMessage(
				ChatColor.RED + "Es sind " + ChatColor.AQUA + griefs.size() + ChatColor.RED + " Griefs gelistet.");
	}

	private void showGriefs(Player player, int page) {
		if (page < 1) {
			player.sendMessage(Messages.getMessages().getMessage("commands.grief.outofbounds"));
			return;
		}
		Inventory inv = Bukkit.createInventory(null, 54, inventoryName);
		inv.setItem(0, Utils.getItem(Material.BOOK, ChatColor.GREEN + "- INFO -", ChatColor.GOLD + "Seite: " + page,
				ChatColor.GOLD + "Linksklick: Teleportieren", ChatColor.GOLD + "Rechtsklick: Löschen"));
		try {
			for (int i = 1; i < 54; i++) {
				Grief grief = griefs.get(i + (page - 1) * 53 - 1);
				ItemStack item = grief.getItem();
				if (grief.getLocation().getWorld() == player.getWorld()
						&& grief.getLocation().distanceSquared(player.getLocation()) < dist2)
					item = Utils.addGlow(item);
				inv.setItem(i, item);
			}
		} catch (IndexOutOfBoundsException e) {
		}
		player.openInventory(inv);
	}

	private void removeGrief(Grief grief) {
		griefs.remove(grief);
		save();
		updateOpenInventories();
	}

	private void addGrief(Player player, String[] args, int startIndex) {
		if (!player.hasPermission("eden.edenstrafen.grief.add")) {
			player.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			return;
		}

		Location location = player.getLocation();
		for (Grief grief : griefs) {
			if (location.getWorld() == player.getWorld() && grief.getLocation().distanceSquared(location) < dist2) {
				player.sendMessage(Messages.getMessages().getMessage("commands.grief.tooclose"));
				return;
			}
		}
		String reason = Utils.combineStrings(startIndex, args.length - 1, args);
		Grief grief = new Grief(reason, player.getName(), location);
		griefs.add(grief);

		Bukkit.broadcast(
				Messages.getMessages().getMessage("commands.grief.broadcast")
						.replaceAll("\\{PLAYER\\}", player.getDisplayName()).replaceAll("\\{REASON\\}", reason),
				"eden.edenstrafen.grief.message");

		updateOpenInventories();
		save();
	}

	private void updateOpenInventories() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			try {
				InventoryView view = player.getOpenInventory();
				if (view.getTitle().equals(inventoryName)) {
					int page = Integer.parseInt(view.getTopInventory().getItem(0).getItemMeta().getLore().get(0)
							.replaceFirst(ChatColor.GOLD + "Seite: ", ""));
					showGriefs(player, page);
				}
			} catch (NullPointerException | NumberFormatException e) {
			}
		}
	}

	private Grief getGrief(UUID uuid) {
		for (Grief grief : griefs) {
			if (grief.getUuid().equals(uuid))
				return grief;
		}
		return null;
	}
}
