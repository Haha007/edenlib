package at.haha007.edenstrafen.grief;

import java.util.Hashtable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.Settings;
import me.clip.placeholderapi.PlaceholderAPI;

public class GriefAlert implements Listener {

	private Hashtable<Player, Integer[]> counter = new Hashtable<>();
	private Hashtable<Player, Long> lastBroadcasts = new Hashtable<>();
	// counter[0] = placed
	// counter[1] = broken

	@EventHandler(ignoreCancelled = false, priority = EventPriority.MONITOR)
	void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (event.isCancelled())
			return;
		if (player.hasPermission("eden.edenstrafen.griefalert.ignore"))
			return;
		if (Settings.griefalertIgnoredMaterials.contains(event.getBlock().getType()))
			return;
		if (Settings.griefalertIgnoredWorlds.contains(player.getWorld().getName().toLowerCase()))
			return;
		if (!counter.containsKey(player))
			counter.put(player, new Integer[] { 0, 0 });

		Integer[] data = counter.get(player);
		data[1]++;

		if (data[1] * 2 < data[0])
			return;

		if (data[1] < 10)
			return;

		if (!lastBroadcasts.containsKey(player))
			lastBroadcasts.put(player, 0l);

		if (System.currentTimeMillis() - lastBroadcasts.get(player) < 20000)
			return;

		lastBroadcasts.replace(player, System.currentTimeMillis());

		String message1 = Messages.getMessages().getMessage("strafen.griefalert.message");
		message1 = PlaceholderAPI.setPlaceholders(player, message1);

		Bukkit.broadcast(message1, "eden.edenstrafen.griefalert.alert");

		String message2 = Messages.getMessages().getMessage("strafen.griefalert.blocks");
		message2 = message2.replaceAll("\\{PLACED\\}", data[0].toString());
		message2 = message2.replaceAll("\\{DESTROYED\\}", data[1].toString());

		Bukkit.broadcast(message2, "eden.edenstrafen.griefalert.alert");

	}

	@EventHandler(ignoreCancelled = false, priority = EventPriority.MONITOR)
	void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		if (event.isCancelled())
			return;
		if (player.hasPermission("eden.edenstrafen.griefalert.ignore"))
			return;
		if (Settings.griefalertIgnoredMaterials.contains(event.getBlock().getType()))
			return;
		if (!counter.containsKey(player))
			counter.put(player, new Integer[] { 0, 0 });

		Integer[] data = counter.get(player);
		data[0]++;
	}

	@EventHandler
	void onPlayerQuit(PlayerQuitEvent event) {
		counter.remove(event.getPlayer());
		lastBroadcasts.remove(event.getPlayer());
	}

	@EventHandler
	void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
		counter.remove(event.getPlayer());
		lastBroadcasts.remove(event.getPlayer());
	}
}
