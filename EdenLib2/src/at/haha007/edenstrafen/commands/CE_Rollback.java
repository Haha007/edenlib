package at.haha007.edenstrafen.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Messages;
import net.minecraft.server.v1_13_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_13_R2.PacketPlayOutChat;

public class CE_Rollback implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.notplayer"));
			return true;
		}
		if (!sender.hasPermission("eden.edenstrafen.rollback.rb")) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			return true;
		}
		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "/rb <name> <area>");
			return true;
		}

		int radius = 30;
		if (args.length == 2) {
			try {
				radius = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.general.notinteger")
						.replaceAll("\\{NUMBER\\}", args[1]));
				return true;
			}
		}

		Bukkit.dispatchCommand(sender, "lb rb player " + args[0] + " since 10000d area " + radius
				+ " block !beacon !chest !furnace !dispenser !sign !trapped_chest !player_head");

		Player player = (Player) sender;

		Bukkit.getScheduler().scheduleSyncDelayedTask(EdenLib.instance, () -> {
			sendClickMessage(player,
					Messages.getMessages().getMessage("commands.rollback.jail").replaceAll("\\{PLAYER\\}", args[0]),
					"/edenjail inventory " + args[0]);
			sendClickMessage(player,
					Messages.getMessages().getMessage("commands.rollback.strafen").replaceAll("\\{PLAYER\\}", args[0]),
					"/edenstrafen show " + args[0]);
		}, 5);

		return true;
	}

	private void sendClickMessage(Player player, String message, String cmd) {
		((CraftPlayer) player).getHandle().playerConnection
				.sendPacket(new PacketPlayOutChat(ChatSerializer.a("{\"text\":\"" + "" + "\",\"extra\":[{\"text\":\""
						+ message + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"" + cmd + "\"}}]}")));
	}
}