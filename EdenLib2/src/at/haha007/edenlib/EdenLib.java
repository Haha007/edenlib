package at.haha007.edenlib;

import at.haha007.edenutils.highscore.GameScoreCommand;
import at.haha007.edenutils.highscore.GameScoreListener;
import org.bukkit.plugin.java.JavaPlugin;

import com.earth2me.essentials.Essentials;

import at.haha007.edenbugfixes.commandexecotors.CE_Freeze;
import at.haha007.edenbugfixes.listeners.AntiDupeListener;
import at.haha007.edengames.jnr.JumpAndRunGame;
import at.haha007.edengames.utils.EdengameCommand;
import at.haha007.edengames.utils.InviteCommand;
import at.haha007.edenlib.commandexecutos.CE_EdenLib;
import at.haha007.edenlib.commandexecutos.CE_Edenplayer;
import at.haha007.edenlib.commandexecutos.CE_Reward;
import at.haha007.edenlib.edenplayer.EdenPlayerManager;
import at.haha007.edenlib.utils.PacketListener;
import at.haha007.edenstrafen.commands.CE_Rollback;
import at.haha007.edenstrafen.grief.GriefAlert;
import at.haha007.edenstrafen.grief.GriefCommand;
import at.haha007.edenstrafen.jail.JailCommand;
import at.haha007.edenstrafen.jail.JailListener;
import at.haha007.edenstrafen.jail.JailManager;
import at.haha007.edenstrafen.strafen.StrafenCommand;
import at.haha007.edenstrafen.strafen.StrafenListener;
import at.haha007.edenutils.ChatFilter;
import at.haha007.edenutils.adminshop.AdminshopCommand;
import at.haha007.edenutils.adminshop.AdminshopListener;
import at.haha007.edenutils.commandexecutors.CE_ItemTool;
import at.haha007.edenutils.commandexecutors.CE_RandomTeleport;
import at.haha007.edenutils.commandexecutors.CE_Team;
import at.haha007.edenutils.listeners.InstantLeaveDespawn;
import at.haha007.edenutils.listeners.NameHighlighter;
import at.haha007.edenutils.listeners.NoOreListener;
import at.haha007.edenutils.listeners.UnenchantableListener;
import at.haha007.edenutils.quests.QuestCommand;
import at.haha007.edenutils.quests.QuestListener;
import at.haha007.edenutils.shop.Shop;
import at.haha007.edenutils.shop.ShopCommand;
import at.haha007.edenutils.shop.ShopListener;

public class EdenLib extends JavaPlugin {
    public static EdenLib instance;
    public static Essentials essentials;

	private EdenPlayerManager ePlayerManager;
	private PacketListener packetListener;
	private CE_EdenLib ce_EdenLib;
	private CE_Edenplayer ce_Edenplayer;
	private CE_Reward ce_Reward;
	private JailCommand jailCommand;
	private JailManager jailManager;
	private StrafenCommand strafenCommand;
	private ShopCommand shopCommand;
	private CE_Freeze freezeCommand;
	private GriefCommand griefCommand;
	private CE_ItemTool itemToolCommand;
	private CE_RandomTeleport randomTpCommand;
	private AdminshopCommand adminshopCommand;
    private EdengameCommand edengameCommand;
    private InviteCommand inviteCommand;
    private GameScoreCommand highscoreCommand;
	private ChatFilter chatFilter;
	private QuestCommand questCommand;

	@Override
	public void onEnable() {
	    instance = this;

		essentials = (Essentials) Essentials.getProvidingPlugin(Essentials.class);
		packetListener = new PacketListener();
		ePlayerManager = EdenPlayerManager.getEdenplayerManager();
		ce_EdenLib = new CE_EdenLib();
		ce_Edenplayer = new CE_Edenplayer();
		ce_Reward = new CE_Reward();
		jailCommand = new JailCommand();
		strafenCommand = new StrafenCommand();
		jailManager = new JailManager();
		shopCommand = new ShopCommand();
		griefCommand = new GriefCommand();
		freezeCommand = new CE_Freeze();
		itemToolCommand = new CE_ItemTool();
		randomTpCommand = new CE_RandomTeleport();
		adminshopCommand = new AdminshopCommand();
        edengameCommand = new EdengameCommand();
        inviteCommand = new InviteCommand();
        highscoreCommand = new GameScoreCommand();
		chatFilter = new ChatFilter(this);
		questCommand = new QuestCommand();

        Shop.getShop();

        // register commandexecutors
        getCommand("edenlib").setExecutor(ce_EdenLib);

        getCommand("edenreward").setExecutor(ce_Reward);
        getCommand("edenreward").setTabCompleter(ce_Reward);

        getCommand("edenplayer").setExecutor(ce_Edenplayer);
        getCommand("edenplayer").setTabCompleter(ce_Edenplayer);

        getCommand("edenjail").setExecutor(jailCommand);
        getCommand("edenjail").setTabCompleter(jailCommand);

        getCommand("edenstrafen").setExecutor(strafenCommand);
        getCommand("edenstrafen").setTabCompleter(strafenCommand);

        getCommand("edenshop").setExecutor(shopCommand);
        getCommand("edenshop").setTabCompleter(shopCommand);

        getCommand("edenadminshop").setExecutor(adminshopCommand);
        getCommand("edenadminshop").setTabCompleter(adminshopCommand);

        getCommand("edengrief").setExecutor(griefCommand);
        getCommand("edengrief").setTabCompleter(griefCommand);

        getCommand("edenfreeze").setExecutor(freezeCommand);

        getCommand("edenitemtool").setExecutor(itemToolCommand);
        getCommand("edenitemtool").setTabCompleter(itemToolCommand);

        getCommand("edenrandomtp").setExecutor(randomTpCommand);

		getCommand("edengame").setExecutor(edengameCommand);
		getCommand("edengame").setTabCompleter(edengameCommand);

		getCommand("edenrollback").setExecutor(new CE_Rollback());

		getCommand("edeninvite").setExecutor(inviteCommand);
		getCommand("edeninvite").setTabCompleter(inviteCommand);

        getCommand("edenhighscore").setExecutor(highscoreCommand);
        getCommand("edenhighscore").setTabCompleter(highscoreCommand);

		getCommand("edenchatfilter").setExecutor(chatFilter);
		getCommand("edenchatfilter").setTabCompleter(chatFilter);

		getCommand("edenquest").setExecutor(questCommand);
		getCommand("edenquest").setTabCompleter(questCommand);

		getCommand("edenteam").setExecutor(new CE_Team());

		// register Listeners
		getServer().getPluginManager().registerEvents(packetListener, this);
		getServer().getPluginManager().registerEvents(new ShopListener(), this);
		getServer().getPluginManager().registerEvents(new JailListener(), this);
		getServer().getPluginManager().registerEvents(new StrafenListener(), this);
		getServer().getPluginManager().registerEvents(new AntiDupeListener(), this);
		getServer().getPluginManager().registerEvents(griefCommand, this);
		getServer().getPluginManager().registerEvents(new GriefAlert(), this);
		getServer().getPluginManager().registerEvents(freezeCommand, this);
		getServer().getPluginManager().registerEvents(new AdminshopListener(), this);
        getServer().getPluginManager().registerEvents(new NoOreListener(), this);
        getServer().getPluginManager().registerEvents(new GameScoreListener(), this);
		getServer().getPluginManager().registerEvents(chatFilter, this);
		getServer().getPluginManager().registerEvents(new NameHighlighter(), this);
		getServer().getPluginManager().registerEvents(new QuestListener(), this);
		getServer().getPluginManager().registerEvents(new InstantLeaveDespawn(), this);
		getServer().getPluginManager().registerEvents(new UnenchantableListener(), this);

        // register games
        edengameCommand.getManager().registerGame("jnr", JumpAndRunGame.class);
	}

	@Override
	public void onDisable() {
		ePlayerManager.saveAll();
		edengameCommand.getManager().stopAll();
		jailManager.onDisable();
		Shop.getShop().save();
	}

	public EdengameCommand getEdengameCommand() {
		return edengameCommand;
	}

	public JailManager getJailManager() {
		return jailManager;
	}

    public PacketListener getPacketListener() {
        return packetListener;
    }

	public EdenPlayerManager getEdenPlayerManager() {
		return ePlayerManager;
	}

	public CE_EdenLib getCE_EdenLib() {
		return ce_EdenLib;
	}
}