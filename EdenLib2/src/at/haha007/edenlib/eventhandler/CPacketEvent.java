package at.haha007.edenlib.eventhandler;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import net.minecraft.server.v1_13_R2.Packet;

public class CPacketEvent extends PlayerEvent implements Cancellable {
	// gets called when the Client sends a Packet to the Server

	private static final HandlerList HANDLERS_LIST = new HandlerList();
	private boolean isCanceled = false;
	private Packet<?> packet;

	public CPacketEvent(Player player, Packet<?> packet) {
		super(player);
		this.packet = packet;
	}

	@Override
	public HandlerList getHandlers() {
		return HANDLERS_LIST;
	}

	public static HandlerList getHandlerList() {
		return HANDLERS_LIST;
	}

	@Override
	public boolean isCancelled() {
		return isCanceled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		isCanceled = cancel;
	}

	public Packet<?> getPacket() {
		return packet;
	}

	public void setPacket(Packet<?> packet) {
		this.packet = packet;
	}
}
