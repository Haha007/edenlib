package at.haha007.edenlib.edenplayer;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Settings;

public class EdenPlayerManager {
	private HashSet<EdenPlayer> loadetPlayers = new HashSet<>();
	private int taskIDUnload, taskIDAutosave;
	private static EdenPlayerManager instance;

	public static EdenPlayerManager getEdenplayerManager() {
		return instance == null ? instance = new EdenPlayerManager() : instance;
	}

	private EdenPlayerManager() {
		startTasks();
	}

	private void startTasks() {
		taskIDUnload = Bukkit.getScheduler().scheduleSyncRepeatingTask(EdenLib.instance, () -> {
			unloadUnused();
		}, 0, 100);
		taskIDAutosave = Bukkit.getScheduler().scheduleSyncRepeatingTask(EdenLib.instance, () -> {
			saveAll();
		}, 0, Settings.autosaveInterval);
	}

	public void reload() {
		Bukkit.getScheduler().cancelTask(taskIDAutosave);
		Bukkit.getScheduler().cancelTask(taskIDUnload);
		startTasks();
	}

	private void unloadUnused() {
		HashSet<EdenPlayer> unload = new HashSet<>();
		for (EdenPlayer edenPlayer : loadetPlayers) {
			if (edenPlayer.unused++ > 10) {
				unload.add(edenPlayer);
			}
		}

		for (EdenPlayer edenPlayer : unload) {
			unload(edenPlayer.getName(), edenPlayer.isChanged());
		}
	}

	public void unload(String name, boolean save) {
		if (Bukkit.getPlayerExact(name).isOnline()) {
			getEdenPlayer(name).unused = 0;
			return;
		}

		Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, () -> {
			if (save) {
				getEdenPlayer(name).save();
			}
			loadetPlayers.remove(getEdenPlayer(name));
		});
	}

	public EdenPlayer getEdenPlayer(Player player) {
		return getEdenPlayer(player.getName());
	}

	public EdenPlayer getEdenPlayer(String name) {

		for (EdenPlayer edenPlayer : loadetPlayers) {
			if (edenPlayer.getName().equals(name.toLowerCase())) {
				edenPlayer.unused = 0;
				return edenPlayer;
			}
		}
		EdenPlayer eplayer = new EdenPlayer(name);
		loadetPlayers.add(eplayer);
		return eplayer;
	}

	public void saveAll() {
		for (EdenPlayer edenPlayer : loadetPlayers) {
			edenPlayer.save();
		}
	}

	public void renameEdenPlayer(String oldName, String newName) {
		EdenPlayer oep = getEdenPlayer(oldName);
		EdenPlayer nep = oep.rename(newName);
		loadetPlayers.add(nep);
	}
}
