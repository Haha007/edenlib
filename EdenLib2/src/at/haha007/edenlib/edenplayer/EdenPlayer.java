package at.haha007.edenlib.edenplayer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bukkit.configuration.file.YamlConfiguration;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Settings;
import at.haha007.edenlib.utils.Settings.SQL;

public class EdenPlayer {
	private static File folder = new File(EdenLib.instance.getDataFolder(), "EdenPlayer");
	private YamlConfiguration cfg;
	private YamlConfiguration unchangedReference;
	private String name;
	int unused = 0;

	public static void createTable() {
		if (Settings.useDatabase) {
			try {
				PreparedStatement ps = SQL.getConnection().prepareStatement(
						"CREATE TABLE IF NOT EXISTS `EdenPlayer` (player VARCHAR(32), data MEDIUMBLOB, PRIMARY KEY(player));");
				ps.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private EdenPlayer() {
	}

	EdenPlayer(String name) {
		name = name.toLowerCase();

		if (!folder.exists()) {
			folder.mkdirs();
		}

		this.name = name;

		if (Settings.useDatabase) {
			try {
				PreparedStatement pst;
				pst = SQL.getConnection().prepareStatement("SELECT * FROM `EdenPlayer` WHERE player = ?");
				pst.setString(1, getName());
				ResultSet rs = pst.executeQuery();
				while (rs.next()) {
					GZIPInputStream gzStream = new GZIPInputStream(rs.getBlob("data").getBinaryStream());
					InputStreamReader reader = new InputStreamReader(gzStream);
					StringBuilder builder = new StringBuilder();

					BufferedReader input = new BufferedReader(reader);
					String line;
					try {
						while ((line = input.readLine()) != null) {
							builder.append(line);
							builder.append('\n');
						}
					} catch (Exception e) {
					}

					cfg = YamlConfiguration.loadConfiguration(new StringReader(builder.toString()));
					return;
				}
				cfg = new YamlConfiguration();
			} catch (SQLException | IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				File file = new File(folder, getName());
				if (file.exists())
					cfg = YamlConfiguration
							.loadConfiguration(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
				else
					cfg = new YamlConfiguration();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		unchangedReference = YamlConfiguration.loadConfiguration(new StringReader(cfg.saveToString()));

	}

	boolean isChanged() {
		return !cfg.equals(unchangedReference);
	}

	public YamlConfiguration getConfig() {
		unused = 0;
		return cfg;
	}

	public String getName() {
		return name;
	}

	void save() {
		if (Settings.useDatabase) {
			try {
				byte[] compressedData = null;
				byte[] dataToCompress = getConfig().saveToString().getBytes();
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
				gzipOutputStream.write(dataToCompress);
				gzipOutputStream.close();
				compressedData = byteArrayOutputStream.toByteArray();

				Connection connection = SQL.getConnection();
				PreparedStatement ps = connection.prepareStatement("REPLACE INTO `EdenPlayer` VALUES(?, ?);");

				ps.setBlob(2, new ByteArrayInputStream(compressedData));
				ps.setString(1, getName());

				ps.execute();
			} catch (SQLException | IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				File file = new File(folder, name);
				OutputStreamWriter writer = new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(file)));
				writer.write(getConfig().saveToString());
				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	EdenPlayer rename(String name) {
		EdenPlayer ep = new EdenPlayer();
		ep.cfg = new YamlConfiguration();
		ep.unchangedReference = new YamlConfiguration();
		for (String key : cfg.getKeys(true)) {
			ep.cfg.set(key, cfg.get(key));
			ep.unchangedReference.set(key, cfg.get(key));
		}
		ep.name = name;
		ep.save();
		return ep;
	}

	public void reset() {
		cfg = new YamlConfiguration();
	}
}
