package at.haha007.edenlib.utils;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import at.haha007.edenlib.eventhandler.CPacketEvent;
import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;
import net.minecraft.server.v1_13_R2.Packet;

public class PacketListener implements Listener {
	// Calling a custom Event when a packet is sent or arrived.
	// CPacketEvent when its a packet from the Client to the Server
	// SPacketEvent when its a packet from the Server to the Client

	// All players who's packets get handeled
	// Nominally all online players
	HashSet<Player> injectedPlayers = new HashSet<>();

	// EventHandler for PlayerJoinEvent
	@EventHandler
	void onPlayerJoinEvent(PlayerJoinEvent event) {
		// injects Player into Pipeline
		inject(event.getPlayer());
	}

	// EventHandler for PlayerQuitEvent
	@EventHandler
	void onPlayerQuitEvent(PlayerQuitEvent event) {
		// disInjects Player from Pipeline
		disInject(event.getPlayer());
	}

	void inject(Player player) {
		// return if the player allready is injected
		if (injectedPlayers.contains(player))
			return;

		// add the player to the list of injected Players
		injectedPlayers.add(player);

		// Create Handler
		ChannelDuplexHandler channelDuplexHandler = new ChannelDuplexHandler() {

			// gets called when a packet is read
			@Override
			public void channelRead(ChannelHandlerContext channelHandlerContext, Object packet) throws Exception {

				// call the event and return if canceled
				if (packet instanceof Packet) {
					CPacketEvent event = new CPacketEvent(player, (Packet<?>) packet);
					Bukkit.getServer().getPluginManager().callEvent(event);
					if (event.isCancelled()) {
						return;
					}
				}

				// Continue normal actions
				super.channelRead(channelHandlerContext, packet);
			}

			// gets called when a packet is written
			@Override
			public void write(ChannelHandlerContext channelHandlerContext, Object packet, ChannelPromise channelPromise)
					throws Exception {

				// call the event and return if canceled
				if (packet instanceof Packet) {
					CPacketEvent event = new CPacketEvent(player, (Packet<?>) packet);
					Bukkit.getServer().getPluginManager().callEvent(event);
					if (event.isCancelled()) {
						return;
					}
				}

				// Continue normal actions
				super.write(channelHandlerContext, packet, channelPromise);
			}

		};

		// Get the Player's Pipeline
		ChannelPipeline pipeline = ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel
				.pipeline();

		// Inject Handler into Pipeline
		try {
			pipeline.addBefore("packet_handler", "Eden_" + player.getName(), channelDuplexHandler);
		} catch (IllegalArgumentException e) {
			return;
		}
	}

	void disInject(Player player) {
		// remove Player from list and check if player isn't injected
		if (!injectedPlayers.remove(player))
			return;

		Channel channel = ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel;

		// Get the Player's Pipeline
		ChannelPipeline pipeline = channel.pipeline();

		// Remove Player from Pipeline
		channel.eventLoop().submit(() -> {
			pipeline.remove("Eden_" + player.getName());
			return null;

		});
	}
}
