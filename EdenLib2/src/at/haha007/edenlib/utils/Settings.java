package at.haha007.edenlib.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenutils.OnlinetimeReward;

public class Settings {
	public static boolean useDatabase = false;
	public static int autosaveInterval = 1000;
	public static HashSet<Material> griefalertIgnoredMaterials = new HashSet<>();
	public static Location jumpAndRunLobby;
	public static List<String> noOreGenWorlds;
	public static OnlinetimeReward onlinetimeReward = null;
	public static List<String> griefalertIgnoredWorlds;

	public static class SQL {
		public static String sqlHost;
		public static String sqlUsername;
		public static String sqlPassword;
		public static String sqlDatabase;
		public static boolean sqlSSL;
		public static int sqlCheckConnectionInterval = 1000;

		private static int taskID = -1;
		private static Connection connection;

		public static void startTask() {
			if (taskID != -1) {
				Bukkit.getScheduler().cancelTask(taskID);
			}

			if (!useDatabase) {
				taskID = -1;
				return;
			}

			taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(EdenLib.instance, () -> {
				Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, () -> {
					if (!isConnected())
						connect();
				});
			}, sqlCheckConnectionInterval, sqlCheckConnectionInterval);
		}

		public static boolean connect() {

			try {
				MysqlDataSource dataSource = new MysqlDataSource();
				dataSource.setUseSSL(sqlSSL);
				dataSource.setAutoReconnect(true);
				dataSource.setUser(sqlUsername);
				dataSource.setPassword(sqlPassword);
				dataSource.setServerName(sqlHost);
				dataSource.setDatabaseName(sqlDatabase);
				connection = dataSource.getConnection();
			} catch (SQLException e) {
				System.err.println("[EdenLib] Couldn't connect to the database.");
				return false;
			}

			return true;
		}

		public static boolean isConnected() {
			try {
				if (connection == null || connection.isClosed())
					return false;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		}

		public static void disconnect() {
			if (isConnected())
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

		public static Connection getConnection() {
			if (!isConnected())
				connect();
			return connection;
		}
	}
}
