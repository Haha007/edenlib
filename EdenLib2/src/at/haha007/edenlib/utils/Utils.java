package at.haha007.edenlib.utils;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Utils {
	private static Pattern timePattern = Pattern.compile(
			"(?:([0-9]+)\\s*y[a-z]*[,\\s]*)?(?:([0-9]+)\\s*mo[a-z]*[,\\s]*)?(?:([0-9]+)\\s*w[a-z]*[,\\s]*)?(?:([0-9]+)\\s*d[a-z]*[,\\s]*)?(?:([0-9]+)\\s*h[a-z]*[,\\s]*)?(?:([0-9]+)\\s*m[a-z]*[,\\s]*)?(?:([0-9]+)\\s*(?:s[a-z]*)?)?",
			2);

	public static String formatDateDiff(long time) {
		long seconds = time / 1000;
		int minutes = (int) (seconds / 60);
		int hours = minutes / 60;
		int days = hours / 24;
		int months = days / 30;
		int years = days / 365;

		seconds %= 60;
		minutes %= 60;
		hours %= 24;
		days %= 30;
		months %= 12;

		String string = "";
		if (years > 0)
			string += years + "Y ";
		if (months > 0)
			string += months + "M ";
		if (days > 0)
			string += days + "D ";
		if (hours > 0)
			string += hours + "h ";
		if (minutes > 0)
			string += minutes + "m ";
		if (seconds > 0)
			string += seconds + "s";
		return string;
	}

	public static long parseDateDiff(String time, boolean future) throws Exception {
		Matcher m = timePattern.matcher(time);
		int years = 0;
		int months = 0;
		int weeks = 0;
		int days = 0;
		int hours = 0;
		int minutes = 0;
		int seconds = 0;
		boolean found = false;

		while (m.find()) {
			if (m.group() != null && !m.group().isEmpty()) {
				for (int c = 0; c < m.groupCount(); ++c) {
					if (m.group(c) != null && !m.group(c).isEmpty()) {
						found = true;
						break;
					}
				}

				if (found) {
					if (m.group(1) != null && !m.group(1).isEmpty()) {
						years = Integer.parseInt(m.group(1));
					}

					if (m.group(2) != null && !m.group(2).isEmpty()) {
						months = Integer.parseInt(m.group(2));
					}

					if (m.group(3) != null && !m.group(3).isEmpty()) {
						weeks = Integer.parseInt(m.group(3));
					}

					if (m.group(4) != null && !m.group(4).isEmpty()) {
						days = Integer.parseInt(m.group(4));
					}

					if (m.group(5) != null && !m.group(5).isEmpty()) {
						hours = Integer.parseInt(m.group(5));
					}

					if (m.group(6) != null && !m.group(6).isEmpty()) {
						minutes = Integer.parseInt(m.group(6));
					}

					if (m.group(7) != null && !m.group(7).isEmpty()) {
						seconds = Integer.parseInt(m.group(7));
					}
					break;
				}
			}
		}

		if (!found) {
			throw new Exception("nope");
		} else {
			GregorianCalendar arg12 = new GregorianCalendar();
			if (years > 0) {
				arg12.add(1, years * (future ? 1 : -1));
			}

			if (months > 0) {
				arg12.add(2, months * (future ? 1 : -1));
			}

			if (weeks > 0) {
				arg12.add(3, weeks * (future ? 1 : -1));
			}

			if (days > 0) {
				arg12.add(5, days * (future ? 1 : -1));
			}

			if (hours > 0) {
				arg12.add(11, hours * (future ? 1 : -1));
			}

			if (minutes > 0) {
				arg12.add(12, minutes * (future ? 1 : -1));
			}

			if (seconds > 0) {
				arg12.add(13, seconds * (future ? 1 : -1));
			}

			GregorianCalendar max = new GregorianCalendar();
			max.add(1, 10);
			return arg12.after(max) ? max.getTimeInMillis() : arg12.getTimeInMillis();
		}
	}

	public static List<String> toLines(String string, int lineLength) {
		ArrayList<String> lines = new ArrayList<>();
		String[] words = string.split(" ");
		String line = "";
		for (String word : words) {
			if (line.length() >= lineLength) {
				lines.add(line.replaceFirst(" ", ""));
				line = "";
			}
			line += " " + word;
		}
		lines.add(line.replaceFirst(" ", ""));

		return lines;
	}

	public static String formatDate(long timeStamp) {
		return new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date(timeStamp));
	}

	public static UUID getItemUUID(ItemStack item) {
		UUID uuid;
		try {
			uuid = UUID.fromString(CraftItemStack.asNMSCopy(item).getOrCreateTag().getString("uuid"));
		} catch (NullPointerException | IllegalArgumentException e) {
			uuid = null;
		}
		return uuid;
	}

	public static ItemStack setItemUUID(ItemStack item, UUID uuid) {
		net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
		nmsItem.getOrCreateTag().setString("uuid", uuid.toString());
		return CraftItemStack.asCraftMirror(nmsItem);
	}

	public static ItemStack addNbtString(ItemStack item, String name, String value) {
		net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
		nmsItem.getOrCreateTag().setString(name, value);
		return CraftItemStack.asCraftMirror(nmsItem);
	}

	public static String getNbtString(ItemStack item, String key) {
		return CraftItemStack.asNMSCopy(item).getOrCreateTag().getString(key);
	}

	public static ItemStack setEnchantable(ItemStack item, boolean enchantable) {
		net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
		nmsItem.getOrCreateTag().setBoolean("enchantable", enchantable);
		return CraftItemStack.asCraftMirror(nmsItem);
	}

	public static boolean isEnchantable(ItemStack item) {
		try {
			return CraftItemStack.asNMSCopy(item).getTag().getBoolean("enchantable");
		} catch (NullPointerException e) {
			return true;
		}
	}

	public static ItemStack getItem(Material material, String name, List<String> lore) {
		ItemStack item = new ItemStack(material);
		ItemMeta meta = item.getItemMeta();
		meta.setLore(lore);
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}

	public static ItemStack getItem(Material material, String name, String... lore) {
		return getItem(material, name, Arrays.asList(lore));
	}

	public static ItemStack addGlow(ItemStack item) {
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_ENCHANTS);
		meta.addEnchant(Enchantment.VANISHING_CURSE, 1, true);
		item.setItemMeta(meta);
		return item;
	}

	public static void giveItem(Player player, ItemStack item) {
		HashMap<Integer, ItemStack> remaining = player.getInventory().addItem(item);
		for (Entry<Integer, ItemStack> entry : remaining.entrySet()) {
			player.getWorld().dropItem(player.getLocation(), entry.getValue());
		}
	}

	public static String combineStrings(int startIndex, int endIndex, String... strings) {
		String string = "";
		try {
			for (int i = startIndex; i <= endIndex; i++) {
				string += " " + strings[i];
			}
		} catch (IndexOutOfBoundsException e) {
		}

		return string.replaceFirst(" ", "");
	}
}