package at.haha007.edenlib.utils;

import java.math.BigDecimal;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import at.haha007.edenlib.EdenLib;
import net.ess3.api.MaxMoneyException;

public class RewardManager {
	public static boolean hasReward(String player, String reward) {
		YamlConfiguration cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig();
		List<String> rewards = cfg.getStringList("rewards");
		return rewards.contains(reward);
	}

	public static List<String> getRewards(String player) {
		YamlConfiguration cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig();
		List<String> rewards = cfg.getStringList("rewards");
		return rewards;
	}

	public static boolean addReward(String player, String reward, boolean announce, double value) {
		// returns false if the player allready contained the reward
		YamlConfiguration cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig();
		List<String> rewards = cfg.getStringList("rewards");
		if (hasReward(player, reward))
			return false;
		
		if (announce) {
			Player pl = Bukkit.getPlayerExact(player);
			if (pl != null)
				pl.sendMessage(Messages.getMessages().getMessage("commands.reward.add.announcement")
						.replaceAll("\\{REWARD\\}", reward));
		}
		try {
			EdenLib.essentials.getUser(player).giveMoney(BigDecimal.valueOf(value));
		} catch (ArithmeticException | MaxMoneyException e) {
			e.printStackTrace();
		}
		
		rewards.add(reward);
		cfg.set("rewards", rewards);
		return true;
	}

	public static boolean removeReward(String player, String reward) {
		// returns false if the player didn't contain the reward
		YamlConfiguration cfg = EdenLib.instance.getEdenPlayerManager().getEdenPlayer(player).getConfig();
		List<String> rewards = cfg.getStringList("rewards");
		if (!rewards.remove(reward))
			return false;
		cfg.set("rewards", rewards);
		return true;
	}
}
