package at.haha007.edenlib.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Map.Entry;
import java.util.Properties;

import com.google.common.base.Charsets;

import at.haha007.edenlib.EdenLib;
import net.md_5.bungee.api.ChatColor;

public class Messages {
	private static Messages instance;
	private File file;
	private Properties props = new Properties();

	private Messages() {
		file = new File(EdenLib.instance.getDataFolder(), "lang.properties");
		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			props.load(new InputStreamReader(new FileInputStream(file), Charsets.UTF_8));

			Properties defaultProperties = new Properties();
			defaultProperties.load(
					new InputStreamReader(EdenLib.instance.getResource("assets/lang.properties"), Charsets.UTF_8));

			boolean changed = false;
			for (Entry<Object, Object> property : defaultProperties.entrySet()) {
				if (!props.containsKey(property.getKey())) {
					props.put(property.getKey(), property.getValue());
					changed = true;
				}
			}

			if (changed)
				props.store(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"), null);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Messages getMessages() {
		return instance == null ? instance = new Messages() : instance;
	}

	public static void reload() {
		instance = null;
	}

	public String getMessage(String key) {
		return ChatColor.translateAlternateColorCodes('&', props.getProperty(key));
	}
}
