package at.haha007.edenlib.utils;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_13_R2.AxisAlignedBB;
import net.minecraft.server.v1_13_R2.BlockPosition;

public class EdenArea {
	private AxisAlignedBB aabb;
	private World world;

	public EdenArea(Vector pos1, Vector pos2, World world) {
		aabb = new AxisAlignedBB(new BlockPosition(pos1.getX(), pos1.getY(), pos1.getZ()),
				new BlockPosition(pos2.getX(), pos2.getY(), pos2.getZ()));
		this.world = world;
	}

	public World getWorld() {
		return world;
	}

	public Vector getMinPoint() {
		return new Vector(aabb.minX, aabb.minY, aabb.minZ);
	}

	public Vector getMaxPoint() {
		return new Vector(aabb.maxX, aabb.maxY, aabb.maxZ);
	}

	public boolean isPointInArea(Location loc) {
		return loc.getWorld() == world && aabb.e(loc.getX(), loc.getY(), loc.getZ());
	}
}
