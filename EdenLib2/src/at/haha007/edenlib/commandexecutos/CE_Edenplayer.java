package at.haha007.edenlib.commandexecutos;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.edenplayer.EdenPlayer;
import at.haha007.edenlib.edenplayer.EdenPlayerManager;
import at.haha007.edenlib.utils.Messages;

public class CE_Edenplayer implements CommandExecutor, TabCompleter {
	// reset <from>
	// set <from> <path> <value>
	// get <from> <path>
	// transfer <from> <to>
	private EdenPlayerManager edenPlayerManager;

	public CE_Edenplayer() {
		edenPlayerManager = EdenLib.instance.getEdenPlayerManager();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0) {
            return false;
        }

		for (int i = 0; i < args.length; i++) {
			args[i] = args[i].toLowerCase();
		}

		Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, new Runnable() {
			@Override
			public void run() {
				String arg0 = args[0].toLowerCase();

				switch (arg0) {
				case "set":
					if (!sender.hasPermission("eden.edenlib.edenplayer.set")) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
						return;
					}

					if (args.length != 4) {
						sender.sendMessage(ChatColor.GOLD + "/edenplayer set <player> <path> <value>");
						return;
					}

					EdenPlayer edenPlayer = edenPlayerManager.getEdenPlayer(args[1]);
					String path = args[2];
					String value = args[3];

					if (StringUtils.isNumeric(value)) {
						// int
						int iValue = Integer.parseInt(value);
						edenPlayer.getConfig().set(path, iValue);

					} else if (value.equals("null")) {
						edenPlayer.getConfig().set(path, null);
					} else {

						try {
							// double
							double dValue = Double.parseDouble(value);
							edenPlayer.getConfig().set(path, dValue);

						} catch (NumberFormatException e) {
							// string
							edenPlayer.getConfig().set(path, value);

						}
					}

					sender.sendMessage(Messages.getMessages().getMessage("commands.edenplayer.setvalue")
							.replaceAll("\\{VALUE\\}", value).replaceAll("\\{PATH\\}", path));

					break;
				case "transfer":
					if (!sender.hasPermission("eden.edenlib.edenplayer.transfer")) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
						return;
					}

					if (args.length != 3) {
						sender.sendMessage(ChatColor.GOLD + "/edenplayer transfer <from> <to>");
						return;
					}

					edenPlayerManager.renameEdenPlayer(args[1], args[2]);

					sender.sendMessage(Messages.getMessages().getMessage("commands.edenplayer.transfer")
							.replaceAll("\\{FROM\\}", args[1]).replaceAll("\\{TO\\}", args[2]));

					break;
				case "reset":
					if (!sender.hasPermission("eden.edenlib.edenplayer.reset")) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
						return;
					}

					if (args.length != 2) {
						sender.sendMessage(ChatColor.GOLD + "/edenplayer reset <player>");
						return;
					}

					edenPlayerManager.getEdenPlayer(args[1]).reset();
					sender.sendMessage(Messages.getMessages().getMessage("commands.edenplayer.reset")
							.replaceAll("\\{PLAYER\\}", args[1]));
					break;
				case "get":
					if (!sender.hasPermission("eden.edenlib.edenplayer.get")) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
						return;
					}

					if (args.length != 3) {
						sender.sendMessage(ChatColor.GOLD + "/edenplayer get <player> <path>");
						return;
					}

					edenPlayer = edenPlayerManager.getEdenPlayer(args[1]);
					path = args[2];
					ConfigurationSection cfg = edenPlayer.getConfig();
					if (!cfg.contains(path)) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.edenplayer.get.notexists")
								.replaceAll("\\{PLAYER\\}", edenPlayer.getName()).replaceAll("\\{PATH\\}", path));
						return;
					}

					value = cfg.get(path).toString();

					sender.sendMessage(Messages.getMessages().getMessage("commands.edenplayer.get")
							.replaceAll("\\{PLAYER\\}", edenPlayer.getName()).replaceAll("\\{PATH\\}", path)
							.replaceAll("\\{VALUE\\}", value));
					break;
				case "dump":
					if (!sender.hasPermission("eden.edenlib.edenplayer.get")) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
						return;
					}

					if (args.length != 3 && args.length != 2) {
						sender.sendMessage(ChatColor.GOLD + "/edenplayer dump <player> [-f/-c]");
						return;
					}

					edenPlayer = edenPlayerManager.getEdenPlayer(args[1]);
					cfg = edenPlayer.getConfig();
					if (args.length == 2 || args[2].equals("-c")) {
						for (String key : cfg.getKeys(true)) {
							Object val = cfg.get(key);
							if (!(val instanceof ConfigurationSection))
								sender.sendMessage(ChatColor.GOLD + key + ": " + ChatColor.GREEN + val);
						}
					} else if (args[2].equals("-f")) {
						YamlConfiguration dump = new YamlConfiguration();
						for (String key : cfg.getKeys(false)) {
							dump.set(key, cfg.get(key));
						}
						File dumpFolder = new File(EdenLib.instance.getDataFolder(), "dump");
						if (!dumpFolder.exists())
							dumpFolder.mkdirs();
						File dumpFile = new File(dumpFolder, args[1] + "_dump.yml");
						try {
							dump.save(dumpFile);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					break;

				default:
					sender.sendMessage(ChatColor.GOLD + "/edenplayer set <player> <path> <value>");
					sender.sendMessage(ChatColor.GOLD + "/edenplayer reset <player>");
					sender.sendMessage(ChatColor.GOLD + "/edenplayer transfer <from> <to>");
					sender.sendMessage(ChatColor.GOLD + "/edenplayer get <player> <path>");
					sender.sendMessage(ChatColor.GOLD + "/edenplayer dump <player> [-f/-c]");
					break;
				}
			}
		});

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		if (args.length == 1) {
			String arg0 = args[0].toLowerCase();
			if ("reset".startsWith(arg0))
				list.add("reset");
			if ("set".startsWith(arg0))
				list.add("set");
			if ("get".startsWith(arg0))
				list.add("get");
			if ("transfer".startsWith(arg0))
				list.add("transfer");
			if ("dump".startsWith(arg0))
				list.add("dump");
		}
		return list;
	}
}