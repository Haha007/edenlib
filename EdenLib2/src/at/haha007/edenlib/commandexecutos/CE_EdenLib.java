package at.haha007.edenlib.commandexecutos;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.util.Vector;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.edenplayer.EdenPlayer;
import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.Settings;
import at.haha007.edenlib.utils.Settings.SQL;
import at.haha007.edenutils.OnlinetimeReward;

public class CE_EdenLib implements CommandExecutor {

	private YamlConfiguration config;
	private final File file;

	public CE_EdenLib() {
		file = new File(EdenLib.instance.getDataFolder(), "config.yml");
		reload();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 1 && args[0].toLowerCase().equals("reload")) {
			if (sender.hasPermission("eden.edenlib.reload")) {
				sender.sendMessage(Messages.getMessages().getMessage("commands.edenlib.reload"));
				reload();
				return true;
			}
		}
		return false;
	}

	public void reload() {
		if (SQL.isConnected())
			SQL.disconnect();

		try {
			if (!file.exists())
				file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		config = YamlConfiguration.loadConfiguration(file);
		Messages.reload();
		generateConfig();
		loadData();

		if (Settings.useDatabase) {
			SQL.connect();
			EdenPlayer.createTable();
		}
		EdenLib.instance.getEdenPlayerManager().reload();
		SQL.startTask();
	}

	private void loadData() {
		SQL.sqlHost = config.getString("sql.host");
		SQL.sqlPassword = config.getString("sql.password");
		SQL.sqlUsername = config.getString("sql.username");
		SQL.sqlSSL = config.getBoolean("sql.useSSL");
		SQL.sqlDatabase = config.getString("sql.database");
		SQL.sqlCheckConnectionInterval = config.getInt("sql.checkConnection");

		Settings.noOreGenWorlds = config.getStringList("noOreGenWorlds");
		Settings.autosaveInterval = config.getInt("edenplayer.autosaveInterval");
		Settings.useDatabase = config.getBoolean("use_sql");
		Location jumpAndRunSpawn = new Vector(config.getDouble("jumpandrunspawn.x"),
				config.getDouble("jumpandrunspawn.y"), config.getDouble("jumpandrunspawn.z"))
						.toLocation(Bukkit.getWorld(config.getString("jumpandrunspawn.world")));
		jumpAndRunSpawn.setPitch((float) config.getDouble("jumpandrunspawn.pitch"));
		jumpAndRunSpawn.setYaw((float) config.getDouble("jumpandrunspawn.yaw"));
		Settings.jumpAndRunLobby = jumpAndRunSpawn;
		Settings.griefalertIgnoredWorlds = new ArrayList<>();
		for (String str : config.getStringList("griefIgnoredWorlds")) {
			Settings.griefalertIgnoredWorlds.add(str.toLowerCase());
		}

		if (Settings.onlinetimeReward != null)
			Settings.onlinetimeReward.unregister();

		Settings.onlinetimeReward = new OnlinetimeReward(config.getInt("onlinetimeRewards.interval"),
				config.getDouble("onlinetimeRewards.value"));

		HashSet<Material> griefIgnoredMaterials = new HashSet<>();
		for (String string : config.getStringList("griefIgnoredMaterials")) {
			Material mat = Material.matchMaterial(string);
			if (mat != null)
				griefIgnoredMaterials.add(mat);
		}
		Settings.griefalertIgnoredMaterials = griefIgnoredMaterials;
	}

	public void generateConfig() {
		YamlConfiguration cfg = YamlConfiguration
				.loadConfiguration(new InputStreamReader(EdenLib.instance.getResource("assets/defaultConfig.yml")));

		for (String key : cfg.getKeys(true)) {
			if (!config.contains(key)) {
				config.set(key, cfg.get(key));
			}
		}

		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public File getFile() {
		return file;
	}

	public YamlConfiguration getConfig() {
		return config;
	}

}
