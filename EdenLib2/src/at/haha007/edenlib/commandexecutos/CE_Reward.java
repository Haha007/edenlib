package at.haha007.edenlib.commandexecutos;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import at.haha007.edenlib.EdenLib;
import at.haha007.edenlib.utils.Messages;
import at.haha007.edenlib.utils.RewardManager;
import net.md_5.bungee.api.ChatColor;

public class CE_Reward implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0)
			return false;
		Bukkit.getScheduler().runTaskAsynchronously(EdenLib.instance, () -> {
			switch (args[0].toLowerCase()) {
			case "add":
				if (!sender.hasPermission("eden.edenlib.reward.add")) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
					return;
				}
				if (args.length < 3 || args.length > 5) {
					sender.sendMessage(ChatColor.RED + "/reward add <player> <reward>");
					sender.sendMessage(ChatColor.RED + "/reward add <player> <reward> <value>");
					sender.sendMessage(ChatColor.RED + "/reward add <player> <reward> <value> <announce>");
					break;
				}

				double value = 0;

				if (args.length >= 4) {
					try {
						value = Double.parseDouble(args[3]);
					} catch (NumberFormatException e) {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.notdouble")
								.replaceAll("\\{NUMBER\\}", args[3]));
						return;
					}
				}

				boolean announce = false;

				if (args.length == 5) {

					if (args[4].toLowerCase().equals("true")) {
						announce = true;
					} else if (args[4].toLowerCase().equals("false")) {
						announce = false;
					} else {
						sender.sendMessage(Messages.getMessages().getMessage("commands.general.notboolean")
								.replaceAll("\\{NUMBER\\}", args[4]));
						return;
					}
				}

				if (RewardManager.addReward(args[1], args[2], announce, value)) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.reward.add")
							.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{REWARD\\}", args[2]));
				} else {
					sender.sendMessage(Messages.getMessages().getMessage("commands.reward.add.exists")
							.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{REWARD\\}", args[2]));
				}
				break;
			case "remove":
				if (!sender.hasPermission("eden.edenlib.reward.reward")) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
					return;
				}
				if (args.length != 3) {
					sender.sendMessage(ChatColor.RED + "/reward remove <player> <reward>");
					break;
				}
				if (RewardManager.removeReward(args[1], args[2])) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.reward.remove")
							.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{REWARD\\}", args[2]));
				} else {
					sender.sendMessage(Messages.getMessages().getMessage("commands.reward.remove.notexists")
							.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{REWARD\\}", args[2]));
				}
				break;
			case "check":
				if (!sender.hasPermission("eden.edenlib.reward.check")) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
					return;
				}
				if (args.length != 3) {
					sender.sendMessage(ChatColor.RED + "/reward check <player> <reward>");
					break;
				}
				if (RewardManager.hasReward(args[1], args[2])) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.reward.check.exists")
							.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{REWARD\\}", args[2]));
				} else {
					sender.sendMessage(Messages.getMessages().getMessage("commands.reward.check.notexists")
							.replaceAll("\\{PLAYER\\}", args[1]).replaceAll("\\{REWARD\\}", args[2]));
				}
				break;
			case "list":
				if (!sender.hasPermission("eden.edenlib.reward.list")) {
					sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
					return;
				}
				if (args.length != 2) {
					sender.sendMessage(ChatColor.RED + "/reward rewards <player>");
					break;
				}
				sender.sendMessage(
						Messages.getMessages().getMessage("commands.reward.list").replaceAll("\\{PLAYER\\}", args[1])
								.replaceAll("\\{REWARDS\\}", RewardManager.getRewards(args[1]).toString()));
				break;

			default:
				break;
			}
		});
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> returns = new ArrayList<>();
		if (args.length == 1) {
			String arg = args[0].toLowerCase();
			if ("add".startsWith(arg))
				returns.add("add");
			if ("remove".startsWith(arg))
				returns.add("remove");
			if ("check".startsWith(arg))
				returns.add("check");
			if ("list".startsWith(arg))
				returns.add("list");
		} else if (args.length == 2) {
			String arg = args[1].toLowerCase();
			for (Player player : Bukkit.getOnlinePlayers()) {
				String name = player.getName();
				if (name.startsWith(arg))
					returns.add(name);
			}
		}
		return returns;
	}

}
