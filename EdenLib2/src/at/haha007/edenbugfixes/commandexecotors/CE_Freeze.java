package at.haha007.edenbugfixes.commandexecotors;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import at.haha007.edenlib.utils.Messages;

public class CE_Freeze implements CommandExecutor, Listener {
	private boolean active = false;

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("eden.edenbugfixes.freeze.freeze")) {
			sender.sendMessage(Messages.getMessages().getMessage("commands.general.noperm"));
			return true;
		}
		active = !active;

		if (active) {
			Bukkit.broadcastMessage(Messages.getMessages().getMessage("commands.freeze.enable"));
		} else {
			Bukkit.broadcastMessage(Messages.getMessages().getMessage("commands.freeze.disable"));
		}
		return true;
	}

	@EventHandler
	void onBlockBreak(BlockBreakEvent event) {
		if (!active)
			return;
		if (event.getPlayer().hasPermission("eden.edenbugfixes.freeze.bypass"))
			return;
		event.setCancelled(true);
	}

	@EventHandler
	void onBlockPlace(BlockPlaceEvent event) {
		if (!active)
			return;
		if (event.getPlayer().hasPermission("eden.edenbugfixes.freeze.bypass"))
			return;
		event.setCancelled(true);
	}
}
