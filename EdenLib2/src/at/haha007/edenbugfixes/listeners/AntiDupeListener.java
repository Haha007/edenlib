package at.haha007.edenbugfixes.listeners;

import java.util.Hashtable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import at.haha007.edenlib.EdenLib;

public class AntiDupeListener implements Listener {
	Hashtable<Player, Integer> blocked = new Hashtable<>();

	@EventHandler
	void onPlayerWorldChangedEvent(PlayerChangedWorldEvent event) {

		Player player = event.getPlayer();
		blocked.remove(player);

		int taskID = Bukkit.getScheduler().scheduleSyncDelayedTask(EdenLib.instance, () -> {
			blocked.remove(player);
		}, 60);
		blocked.put(player, taskID);
	}

	@EventHandler
	void onPlayerInventoryEvent(InventoryClickEvent event) {
		if (blocked.containsKey(event.getWhoClicked())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	void onPlayerInventoryEvent(InventoryInteractEvent event) {
		if (blocked.containsKey(event.getWhoClicked())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	void onPlayerDropItemEvent(PlayerDropItemEvent event) {
		if (blocked.containsKey(event.getPlayer())) {
			event.setCancelled(true);
		}
	}
}
